FROM dyalog/dyalog:dotnet-18.2
USER root
ARG TO=sac
ENV TO=$TO 
# Args don't save in environment, so have to declare twice?
WORKDIR /app
COPY . /app

ENTRYPOINT ["sh","/app/docker/entrypoint.sh"]

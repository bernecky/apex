% SAC Code fragments for dyadic upgrade, downgrade
% Robert Bernecky 2005-11-17

%Fragment ugrd 1** c c i .
inline $ZTYPE[.] $FNAME($XTYPE[256] x, $YTYPE[+] y, int QUADio)
{ // Quadav-upgrade on character array
z = QUADio + UpgradeHeap(y);
 return( z);
}
%Generate , UpgradeHeap, X$YTI, X**, ., $CT

%Fragment dgrd 1** c c i .
inline $ZTYPE[.] $FNAME($XTYPE[256] x, $YTYPE[+] y, int QUADio)
{ // Quadav-downgrade on character 
z = QUADio + DowngradeHeap(y);
 return( z);
}
%Generate , DowngradeHeap, X$YTI, X**, ., $CT


%Fragment ugrd 1** c c i .
inline $ZTYPE[.] $FNAME($XTYPE[256] x, $YTYPE[+] y, int QUADio)
{ // Generic dyadic upgrade on character array
 fixme 
}

%Fragment dgrd 1** c c i .
inline $ZTYPE[.] $FNAME($XTYPE[256] x, $YTYPE[+] y, int QUADio)
{ // Generic dyadic downgrade on character array
 fixme 
}


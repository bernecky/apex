% SAC Code fragments for outer product loop control.
% Robert Bernecky 2023-02-24
% Reworked EmitDyadicOuterProducts to make it simpler, probably too simple.
% as part of my project to get apex out of the ditch and
% back on the dirt road again.
% This accepts APL\360ish arguments, so scalar (rank-0)
% uniform functions g only, for ⍺ ∘.g ⍵.

% Extension to non-scalar, uniform functions will require
% a bit of work. Going down the APL2/Dyalog APL route here
% is a rotten idea, as it requires enclosed/nested/boxed
% arrays with all their evils. I am going to review
% how J does it, and likely will likely go
% down that road, but not today.

%Fragment  jotdot 000 bidc bidc bidc .
inline $ZTYPE $FNAME($XTYPE$XRK x, $YTYPE$YRK y$SYSVARFGDECL)
{ // SxS outer product
  z = $GFN$GCT$GCT$GZT(to$GCT(x),to$GCT(y)$SYSVARGKER);
  // DEBUG  Generate , $GFN, $GCT$GCT$GZT, 000, ., $CT
  return(z);
}
%Generate , $GFN, $GCT$GCT$GZT, 000, ., $CT

%Fragment  jotdot 0** bidc bidc bidc .
inline $ZTYPE[*] $FNAME($XTYPE$XRK x, $YTYPE$YRK y$SYSVARFGDECL)
{ // SxA outer product
  z = $GFN$GCT$GCT$GZT(to$GCT(x),to$GCT(y)$SYSVARGKER);
  // DEBUG Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$ZRANK, ., $CT
  return(z);
}
%Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$YRANK, ., $CT

%Fragment  jotdot *0* bidc bidc bidc .
inline $ZTYPE[*] $FNAME($XTYPE$XRK x, $YTYPE$YRK y$SYSVARFGDECL)
{ // AxS outer product
  z = $GFN$GCT$GCT$GZT(to$GCT(x),to$GCT(y)$SYSVARGKER);
  // DEBUG Generate , $GFN, $GCT$GCT$GZT, $XRANK0$ZRANK, ., $CT
  return(z);
}
%Generate , $GFN, $GCT$GCT$GZT, $XRANK0$XRANK, ., $CT

%Fragment  jotdot *** bidc bidc bidc .
inline $ZTYPE[*] $FNAME($XTYPE$XRK x, $YTYPE$YRK y$SYSVARFGDECL)
{ // AxA outer product, with cells of SxA
 cell = genarray(shape(y), $OTFILL);
 z = with {
        (. <= iv <= .) {
         xitem = to$GCT(x[iv]);
        } : $GFN$GCT$GCT$GZT(xitem, to$GCT(y)$SYSVARGKER);
        // DEBUG Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$ZRANK, ., $CT
        } : genarray(shape(x), cell);
 return(z);
}
%Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$YRANK, ., $CT


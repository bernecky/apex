%  Monadic query (roll) and Dyadic query (deal)

% We only support ye olde Lehmer algorithm, which has many
% shortcomings, but at least the algorithm is documented and portable.
% To use it, set ⎕rl← 16807 0, where 16807 is the new seed

% However, this behavior is NOT backwards-compatable, nor
% ISO N8485-compliant, in at least two ways:

% First, if you set ⎕rl←16807, then reference the value of ⎕rl, the value
% produced by Dyalog APL is a two-element, nested vector.

% Second, the values produced by roll and deal are different from those
% that would have been produced by Dyalog APL before the code change.

% So, here's what APEX does with this today:
% ⎕rl can be set to a scalar, as always
% A reference to ⎕rl produces an integer.
% If ⎕rl is set to a two-element vector, the second element is ignored.

% The net effect of this is that, in APEX, extant ISO N8485 code will operate correctly.
% Dyalog APL extensions that allow specification of non-scalar values to ⎕rl
% ignore but the first element, which is deemed to be the new seed.
% If somebody wants to support other Dyalog APL flavors of Random Number Generators (RNG), go to
% it, but please do not break the extant code.

%Fragment quer X00 x bid i .
inline $ZTYPE, int $FNAME($YTYPE y, int QUADio, int QUADrl)
{ // Monadic query (roll) -  scalar
 inty = toi(y);
 if (inty <= 0) print(tochar("roll domain error"));
 Qrl = Lehmer(QUADrl);
 z = (tod(Qrl) * tod(inty)) / tod(2147483647);
 return(toi(z) + QUADio, Qrl);
}
%Generate , Lehmer, $XT$YT$ZT, X00, ., I
%Generate , firstel, XXI, XX0, ., I


%Fragment quer X** x bid i .
inline $ZTYPE[+], int $FNAME($YTYPE[+] y, int QUADio, int[*] QUADrl)
{ // Monadic query (roll) - non=scalar
 zxrho = prod(shape(y));
 ravely = reshape([zxrho], y);
 inty = toi(y);
 if (any(inty <= 0)) print(tochar("roll domain error"));
 // The following presumes ordering, so we use FOR loop. Sorry
 z = genarray([zxrho], -1);
 Q = firstel(QUADrl);
 for (i=0; i<zxrho; i++) {
        Q = Lehmer(Q);
        val = (tod(Q) * tod(ravely[[i]])) / tod(2147483647);
        z[[i]] =  QUADio + toi(val);
 }
 z = reshape(shape(y), z);
 return(z, Q);
}
%Generate , Lehmer, $XT$YT$ZT, X00, ., I
%Generate , firstel, XXI, XX0, ., I

%Fragment quer 001  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE x, $YTYPE y, int QUADio, int[2] QUADrl)
{ // Kludge for Dyalog APL non-ISO conformance
  z1, z2 = $FNAME( x, y, QUADio, QUADrl[0]);
  return( z1, z2);                           
}

inline $ZTYPE[.], int $FNAME($XTYPE x, $YTYPE y, int QUADio, int QUADrl)
{ // Dyadic query (deal) - scalar 
  // This allocates an array of shape y. If this
  // causes you memory grief, use the algorithm in workspace 42 query,
  // or use smaller y!
 intx = toi(x);
 inty = toi(y);
 shpz = toi(x);
 z = iota (inty);

 for( i=0; i<intx; i++) {
        // Interchange z[i] with z[i + ? y - i]
        rand, QUADrl = querXII(inty -i, 0, QUADrl);
        c = i + rand;
        if ( c < shpz) {
                tmp    = z[[c]];
                z[[c]] = z[[i]]; 
                z[[i]] = tmp;
        }
 }      
 z = take([intx], z);
 return(z + QUADio, QUADrl);
}
%Generate , quer, X$YT$ZT, X00, ., I 

%Fragment quer 111  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE[.] x, $YTYPE[.] y, int QUADio, int[*] QUADrl)
{ // Dyadic query (deal) - non-scalar
 /* need length error check FIXME */
 return(querIII(x[0],y[0],QUADio,QUADrl));
}
%Generate , quer, III, 000, ., I 

%Fragment quer 011  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE x, $YTYPE[.] y, int QUADio, int[*] QUADrl)
{ // Dyadic query (deal) - non-scalar
 /* need length error check FIXME */
 return(querIII(x,y[0],QUADio,QUADrl));
}
%Generate , quer, III, 000, ., I 

%Fragment quer 101  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE[.] x, $YTYPE y, int QUADio, int[*] QUADrl)
{ // Dyadic query (deal) - non-scalar
 /* need length error check FIXME */
 return(querIII(x[0],y,QUADio,QUADrl));
}
%Generate , quer, III, 000, ., I 

% SaC code fragments for monadic scalar functions.

%Fragment msfctl x** x bidc bidc .
inline $ZTYPE[+]$SYSVARGOUT $FNAME($YTYPE$YRK y$SYSVARGDECL)
{ // Monadic scalar functions on array
  z = with { 
        ( . <= iv <= .)
                : $FNX$CT$ZT(to$CT(y[iv])$SYSVARGKER);
        } : genarray(shape(y), $OTFILL);
  return(z);
}
%Generate , $FN, $XT$CT$ZT, x00, ., $CT

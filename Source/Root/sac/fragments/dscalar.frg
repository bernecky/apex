% Dyadic Scalar function definitions 
% R. Bernecky 2006-01-05
%
% Header notes:
%    percentFragment  marks the beginning of each entry
%    Fields are delimited by blanks.
%    Field 1: primitive function jsymbol
%    Field 2: acceptable left argument types
%    Field 3: acceptable right argument types
%    Field 4: Result type for fragment
%    Field 5: ifergit
%
% Header ordering may be critical in the case where two 
% fragments would both work. This is important in epio, for
% example, where a special case (e.g., charvec iota charvec)
% must be chosen in preference to the general case. 
% Or, here, where we can use a fast Boolean-mod-Boolean, rather
% than general mod code.
%
%Fragment plus 000 bid bid bid .        
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return($XTto$CT(x)+$YTto$CT(y));
}

%Fragment bar 000 bid bid bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return($XTto$CT(x)-$YTto$CT(y));
}

%Fragment mpy 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return($XTtoB(x) & $YTtoB(y));
}

%Fragment mpy 000 bid bid bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return($XTto$CT(x)*$YTto$CT(y));
}

%Fragment div 000 bid bid d .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ dx = $XTtoD(x);
  dy = $YTtoD(y);
  z = (dx == dy) ? 1.0  : dx/dy;
  return(z);
}

%Fragment min 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return (x&y);
}

%Fragment min 000 idc idc idc .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{
 return (min($XTto$CT(x),$YTto$CT(y))); // x min y
}

%Fragment max 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return (x&y); // a max b on Booleans
}

%Fragment max 000 bidc bidc idc .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return (max($XTto$CT(x),$YTto$CT(y))); // x max y
}

%Fragment mod 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return((!x)&y);
}

%Fragment mod 000 bi bi bi .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ /* SxS residue (aka modulo) */
  /* (x residue y) <-->   y-x times floor y divide x + 0 = x  */
  /* See model residueI in workspace UTDScalarI.dws */
  x = $XTtoI(x);
  y = $YTtoI(y);
  if( 0 != x) {
    q = y / x;
    z = y - ( x * q);
  } else {
    z = y;
  }
 
  if( z != 0) {
    if( mpyXII( x) != mpyXII( y)) {
      z = z + x;
    }
  }
 return(z);
} 
%Generate , mpy, X$YT$ZT, X00, ., $CT

%Fragment mod 000 bid bid bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ /* Double residue double */
  /* This definition is taken from SHARP APL Refman May 1991, p.6-26.
   * It extends the definition of residue to fractional right arguments
   * and to zero, negative and fractional left arguments.
   * r= y-x times floor y divide x+0=x
   * See also APL model in workspace 43 UTDScalarI. 
   */
  nx = (0.0 == $XTto$CT(x))  ?  1.0  : $XTto$CT(x);
  z = $YTto$CT(y) - $XTto$CT(x) * tod(DFLOOR($YTto$CT(y)/nx, QUADct));
  return(z);
}
%Generate , DFLOOR, X$YT$ZT, X00, ., $CT

%Fragment star 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(x | !y);
}

%Fragment star 000 bid b bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ 
 z =  ($YTtoB(y))  ?  $XTto$ZT(x)  : Ito$ZT(1);
 return(z);
}

%Fragment star 000 bid i bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{  /* number to integer power */
  z = Ito$CT( 1);
  for( i=0; i<$YTtoI( y); i++) {
    z = z * $XTto$CT(x);  
  }
  return( z);
}

%Fragment star 000 bid d d .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(pow($XTtoD(x),$YTtoD(y)));
}

%Fragment log  000 bid bid d .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(log($YTtoD(y))/log($XTtoD(x)));
}

%Fragment lt 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return((!x)&y);
}

/* NB.  APEX Extension of ISO APL to allow comparison of characters */
/* relationals */
%Fragment lt 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A<B on Boot/Int/Char
  return($XTto$CT(x)<$YTto$CT(y));
}

%Fragment lt 000 bi bi b i 
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ // Boolean/Integer less than
 return($XTto$CT(x)<=$YTto$CT(y));

%Fragment lt 000 bid bid b d
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ // Fuzzy less than
 return(($XTto$CT(x)<$YTto$CT(y)) & !APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
%Generate , APEXFUZZEQ, $XT$YTB, 000, ., D

%Fragment le 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A<=B on Boolean
 return((!x)|y);
}

%Fragment le 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A<=B on Boolean,int, char
 return($XTto$CT(x) <= $YTto$CT(y));
}

%Fragment le 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ // Fuzzy <=
 return(($XTto$CT(x)<$YTto$CT(y)) | APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
%Generate , APEXFUZZEQ, DDB, 000, ., D

%Fragment eq 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A=B on non-doubles
 return($XTto$CT(x) == $YTto$CT(y));
}

%Fragment eq 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ // A=B on doubles
  /* We use | instead of || on the assumption that 
   * the zero-fuzz case will eliminate the second leg,
   * and it will also eliminate a CONDFUN.
   */
 return(($XTto$CT(x) == $YTto$CT(y)) | APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
%Generate , APEXFUZZEQ, DDB, 000, ., D

%Fragment ne 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A !=B on non-doubles
 return($XTto$CT(x) != $YTto$CT(y));
}

%Fragment ne 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ // A!=B on doubles
 return(($XTto$CT(x) != $YTto$CT(y)) & !APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
%Generate , APEXFUZZEQ, DDB, 000, ., D

%Fragment gt 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A>B on non-doubles
 return(x & !y);
}

%Fragment gt 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A>B on Booleans
 return($XTto$CT(x) > $YTto$CT(y));
}

%Fragment gt 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ // A>B on doubles
 return(($XTto$CT(x) > $YTto$CT(y)) & !APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
%Generate , APEXFUZZEQ, DDB, 000, ., D

%Fragment ge 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A>=B on Booleans
 return(x | !y);
}

%Fragment ge 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ // A>=B on non-Doubles
 return($XTto$CT(x) >= $YTto$CT(y));
}

%Fragment ge 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ // A>=B on doubles
 return(($XTto$CT(x) >= $YTto$CT(y)) | APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
%Generate , APEXFUZZEQ, DDB, 000, ., D

%Fragment and 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return($XTtoB(x)&$YTtoB(y));
}

%Fragment or 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return($XTtoB(x) | $YTtoB(y));
}

%Fragment nand 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(!($XTto$CT(x)&$YTto$CT(y)));
}

%Fragment nor 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(!($XTto$CT(x)|$YTto$CT(y)));
}

%Fragment circ 000 bid bid bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ /* Circle functions */
 xi = $XTtoI(x);
 if      (1 == xi)
        z = sin($YTtoD(y));
 else if (2 == xi)
        z = cos($YTtoD(y));
 else if (3 == xi)
        z = tan($YTtoD(y));
 else if (4 == xi)
        z = pow(1.0+($YTtoD(y)*$YTtoD(y)),0.5);
 else 
        z = 42.0; /* Should be domain error or something */
 return(z); 
}


# SAC Code fragments for outer product loop control.
# Robert Bernecky 2023-02-24
# Reworked EmitDyadicOuterProducts to make it simpler, probably too simple.
# as part of my project to get apex out of the ditch and
# back on the dirt road again.
# This accepts APL\360ish arguments, so scalar (rank-0)
# uniform functions g only, for ⍺ ∘.g ⍵.

# Extension to non-scalar, uniform functions will require
# a bit of work. Going down the APL2/Dyalog APL route here
# is a rotten idea, as it requires enclosed/nested/boxed
# arrays with all their evils. I am going to review
# how J does it, and likely will likely go
# down that road, but not today.

#Fragment  jotdot 000 bidc bidc bidc .
function $FNAME(x::$XRK$XTYPE$EXRK, y::$YRK$YTYPE$EYRK$SYSVARFGDECL)::$ZTYPE
  # SxS outer product
  z = $GFN$GCT$GCT$GZT(to$GCT(x),to$GCT(y)$SYSVARGKER);
  # DEBUG  Generate , $GFN, $GCT$GCT$GZT, 000, ., $CT
  return z
end
#Generate , $GFN, $GCT$GCT$GZT, 000, ., $CT

#Fragment  jotdot 0** bidc bidc bidc .
function $FNAME(x::$XRK$XTYPE$EXRK, y::$YRK$YTYPE$EYRK$SYSVARFGDECL)::Array{$ZTYPE}
  # SxA outer product
  z = $GFN$GCT$GCT$GZT(to$GCT(x),to$GCT(y)$SYSVARGKER)
  # DEBUG Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$ZRANK, ., $CT
  return z
end
#Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$YRANK, ., $CT

#Fragment  jotdot *0* bidc bidc bidc .
function $FNAME(x::$XRK$XTYPE$EXRK, y::$YRK$YTYPE$EYRK$SYSVARFGDECL)::Array{$ZTYPE}
  # AxS outer product
  z = $GFN$GCT$GCT$GZT(to$GCT(x),to$GCT(y)$SYSVARGKER)
  # DEBUG Generate , $GFN, $GCT$GCT$GZT, $XRANK0$ZRANK, ., $CT
  return z
end
#Generate , $GFN, $GCT$GCT$GZT, $XRANK0$XRANK, ., $CT

#Fragment  jotdot *** bidc bidc bidc .
function $FNAME(x::$XRK$XTYPE$EXRK, y::$YRK$YTYPE$EYRK$SYSVARFGDECL)::AbstractArray{$ZTYPE}
 # AxA outer product, with cells of SxA
#  cell = genarray(shape(y), $OTFILL);
#  z = with {
#         (. <= iv <= .) {
#          xitem = to$GCT(x[iv]);
#         } : $GFN$GCT$GCT$GZT(xitem, to$GCT(y)$SYSVARGKER);
#         // DEBUG Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$ZRANK, ., $CT
#         } : genarray(shape(x), cell);

  # cell = fill($OTFILL, size(y))
  # z = fill(cell, size(x))
  # for iv in z
  #  xitem = to$GCT(x[iv])
  #  cell[iv] = $GFN$GCT$GCT$GZT(xitem, to$GCT(y[iv])$SYSVARGKER)
  # end

  # AxA outer product, with cells of SxA
  # Anonymous function that corresponds to the operation defined inside `with` construct.
  #z_item(xitem, yitem) = $GFN$GCT$GCT$GZT(xitem, yitem$SYSVARGKER)

  # Array comprehension to generate the resulting array z.
  # DEBUG Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$ZRANK, ., $CT
  if "$GFN" == "eq" #= Optimize eq=# && "$GCT" != "C" # Does not work with Chars
    z = fill($OTFILL, (size(y)...,size(x)...))
    @tturbo for yindx in CartesianIndices(y)
      for xindx in CartesianIndices(x)
        #$GFN$GCT$GCT$GZT(xi, yi$SYSVARGKER)
        z[yindx, xindx] = x[xindx] == y[yindx]
      end
    end
    return z
  end
  if "$GFN" == "plus" #= Optimize plus=# && "$GCT" != "C" # Does not work with Chars
    z = fill($OTFILL, (size(y)...,size(x)...))
    @tturbo for yindx in CartesianIndices(y)
      for xindx in CartesianIndices(x)
        #$GFN$GCT$GCT$GZT(xi, yi$SYSVARGKER)
        z[yindx, xindx] = x[xindx] + y[yindx]
      end
    end
    return z
  end
  # @inbounds @fastmath for yindx in eachindex(y)
  #   for xindx in eachindex(x)
  #     #$GFN$GCT$GCT$GZT(xi, yi$SYSVARGKER)
  #     z[yindx, xindx] = $GFN$GCT$GCT$GZT(x[xindx], y[yindx]$SYSVARGKER)
  #   end
  # end
  z = [$GFN$GCT$GCT$GZT(xi, yi) for yi in y, xi in x]
  # Try the above, but with @simd
  # z = [@simd for yi in y begin
  #       @simd for xi in x
  #         @inbounds $GFN$GCT$GCT$GZT(xi, yi$SYSVARGKER)
  #       end
  # end]

  #z = stack(broadcast(xi->broadcast(yi->$GFN$GCT$GCT$GZT(xi, yi$SYSVARGKER),y),x))
  
  # z = map(xitem -> map(yitem -> $GFN$GCT$GCT$GZT(xitem, yitem$SYSVARGKER), y), x)
  # z = reshape(vcat(z...), (size(y)..., size(x)...))

  return z

  # cell = zeros(Int, size(y))
  # z = zeros(Int, size(x))
  # for iv in eachindex(x)
  #     xitem = convert(Int, x[iv])
  #     # DEBUG Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$ZRANK, ., $CT
  #     z[iv] = $GFN$GCT$GCT$GZT(xitem, convert(Int, y[iv]))
  # end
  # return z

  #z = broadcast((xitem, yitem) -> $GFN$GCT$GCT$GZT(xitem, yitem$SYSVARGKER), x, y)
 #return z
end
#Generate , $GFN, $GCT$GCT$GZT, 0$YRANK$YRANK, ., $CT


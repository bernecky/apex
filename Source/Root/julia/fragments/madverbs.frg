# SAC 2006-08-09 rbernecky
# Code fragments for monadic adverbs: reduce, scan, and cut.
#
# NB. Replicate and expand are located in replicat.frg


##################### reduce on scalars ####################555

#Fragment sl  x00 bidc bidc bidc .
#Fragment sl1 x00 bidc bidc bidc .
function $FNAME(y::$YTYPE)::$ZTYPE
   #= Reduction of scalar =#
  return y # How is this useful?
end

#Fragment sl  x00 bidc bidc bidc QUICKSTOP 
#Fragment sl1 x00 bidc bidc bidc QUICKSTOP 
function $FNAMEQUICKSTOP(y::$YTYPE)::$ZTYPE
  #= Reduction of scalar =#
  return y
end

#Fragment sl  x00 bidc bidc bidc FOLD 
#Fragment sl1 x00 bidc bidc bidc FOLD 
function $FNAMEFOLD(y::$YTYPE)::$ZTYPE
  #= Reduction of scalar =#
  return y
end

# The identity elements for vector reductions are not exactly right.
# We use maxint() and minint() for integer min/max reduce, whereas
# APL uses maxdouble() and mindouble(). If we did this, we would
# end up with reduce ALWAYS having to produce a double result. 
# Bummer, Hal. 
# Purists can rewrite their integer reductions as:
#      min/maxdouble(), Y
# and get that double result they yearn for.

##################### reduce on vectors ####################555

#Fragment sl  x10 bidc bidc bidc FOLD 
#Fragment sl1 x10 bidc bidc bidc FOLD 
function $FNAMEFOLD(y::Vector{$YTYPE})::$ZTYPE
  #= First/last axis fold-based reduction of vector =# 
  # TODO: Is this good enough?
  #=lim = size(y)[1]-1
  z = with {
        (0*size(y) <= iv < size(y)) 
                : $YTto$ZT(y[lim-iv]);
       } :  fold( $FN$ZT$ZT$ZT, Ito$ZT($FRID));
  return(z);=#
  z = reduce($FN$ZT$ZT$ZT, y)
  return z
end
#Generate , $FN, $ZT$ZT$ZT, 000, ., $ZT

#Fragment sl  x10 bidc bidc bidc QUICKSTOP 
#Fragment sl1 x10 bidc bidc bidc QUICKSTOP 
function $FNAMEQUICKSTOP(y::Vector{$YTYPE})::$ZTYPE
  # First/last axis reduction of vector with quick stop
  # z = foldl($FN$ZT$CT$ZT, Ito$ZT($FRID), Ito$ZT($STOPONVALUE)) do iv
  #         if 0 <= iv < length(y)
  #             return $YTto$CT(y[iv])
  #         end
  #     end
  z = reduce($FN$ZT$CT$ZT, y, init=Ito$ZT($FRID))
  return z
end
#Generate , $FN, $ZT$ZT$ZT, 000, ., $ZT

#Fragment sl  x10 bidc bidc bidc . 
#Fragment sl1 x10 bidc bidc bidc . 
function $FNAME(y::Vector{$YTYPE})::$ZTYPE
 #= First/last axis slow reduction of vector. Can't use fold or quickstop =# 
#  shp = shape(y)[[0]];
#  if (0 == shp) {
#    z = Ito$ZT($FRID); 
#  } else {
#    z = $YTto$ZT(y[[shp-1]]);
#    for (i=shp-2; i>=0; i--) {
#      z = $FN$ZT$CT$ZT($YTto$CT(y[[i]]),$YTto$CT(z));
#    }
#  }
  shp = size(y)[end]
  #### apparently not z = reduce($FN$ZT$ZT$ZT, y) # Worth a try - hrh 2023-08-05
  if shp == 0
    z = Ito$ZT($FRID)
  else
    z = $YTto$ZT(y[shp])
    for i in shp-1:-1:1
      z = $FN$ZT$ZT$ZT($YTto$ZT(y[i]), $YTto$ZT(z))
    end
  end
  return z
end
#Generate , $FN, $ZT$ZT$ZT, 000, ., $ZT

##################### last-axis reduce on rank>1 arrays  ####################555

#Fragment  sl  x** bidc bidc bidc QUICKSTOP
function $FNAMEQUICKSTOP(y::Array{$YTYPE})::Array{$ZTYPE}
  #= last axis reduce rank-2 or greater matrix w/quickstop =#
  #println(y)
  sy = size(y)
  #zrho = drop([-1], sy);
  zrho=sy[2:end]
  # z = with {
  #        (. <= iv <= .) 
  #               : $FNAMEQUICKSTOP(y[iv]);
  #       } : genarray(zrho, $OTFILL); 
  z = fill($OTFILL, zrho)
  colons=ntuple(x->Colon(), length(sy)-1)
  for i in 1:sy[end]
    #println("Recusive call to $FNAMEQUICKSTOP")
    #println(i)
    r=$FNAMEQUICKSTOP(y[colons...,i])
    #println(r)
    #println(i,colons[1:end-1]...)
    #println(z[i])
    z[colons[1:end-1]...,i] = r
    #display(y[i, colons...])
  end
  #println(z)
  #exit(418) # Teapot!
  return z
end
#Generate $FN, sl, X$YT$ZT, X10, QUICKSTOP, $ZT

#Fragment  sl  x** bidc bidc bidc FOLD
function $FNAMEFOLD(y::Array{$YTYPE})::Array{$ZTYPE}
  #= last axis reduce rank-2 or greater matrix w/folding =#
  sy = size(y)
  #zrho = drop([-1], sy);
  zrho=sy[2:end]
  #println("zrho is $zrho")
  # z = with {
  #        (. <= iv <= .) 
  #               : $FNAMEFOLD(y[iv]);
  #       } : genarray(zrho, $OTFILL); 
  z = fill($OTFILL, zrho)
  colons=ntuple(x->Colon(), length(sy)-1)
  for i in 1:sy[end]
    z[colons[1:end-1]..., i] = $FNAMEFOLD(y[colons..., i])
  end
  return z
end
#Generate $FN, sl, X$YT$ZT, X10, FOLD, $ZT

#Fragment  sl  x** bidc bidc bidc .
function $FNAME(y::Array{$YTYPE})::Array{$ZTYPE}
  #= last axis reduce rank-2 or greater matrix w/no smarts =#
  sy = size(y)
  zrho = sy[2:end]
  # Doesnt work z=reduce($FN$ZT$ZT$ZT, y, dims=1)
  z = fill($OTFILL, zrho)
  colons = ntuple(x->Colon(), length(sy)-1)
  for i in 1:sy[end]
    z[colons[1:end-1]...,i] = $FNAME(y[colons...,i])
  end
  return z
end
#Generate $FN, sl, X$YT$ZT, X10, ., $ZT


##################### first-axis reduce on rank-2 arrays  ##################

#Fragment  sl1  x21 bidc bidc bidc QUICKSTOP
inline $ZTYPE[.] $FNAMEQUICKSTOP($YTYPE[.,.] y)
{ #= first-axis reduce rank-2 matrix with quickstop =#
  yt = TRANSPOSE(y);
  zrho = drop([-1], shape(yt));
  z = with {
        (. <= iv <= .)
                : $FNslX$YT$ZTQUICKSTOP(yt[iv]);
        } : genarray(zrho, $OTFILL);
  return(z);
}
#Generate $FN,  sl,             X$YT$ZT, X10,   QUICKSTOP, $ZT
#Generate ,     TRANSPOSE,      X$YT$YT, X**,   ., $YT
#Generate ,     TRANSPOSE,      X$ZT$ZT, X**,   ., $ZT


#Fragment  sl1  x21 bidc bidc bidc FOLD
function $FNAMEFOLD(y::Array{$YTYPE,2})::Vector{$ZTYPE}
  #= first-axis reduce rank-2 matrix =#
  yt = TRANSPOSE(y)
  #zrho = drop([-1], shape(yt))
  zrho=size(yt)[2:end]
  # z = with {
  #       (. <= iv <= .)
  #               : $FNslX$YT$ZTFOLD(yt[iv]);
  #       } : genarray(zrho, $OTFILL);
  z = fill($OTFILL, zrho)
  colons=ntuple(x->Colon(), length(size(yt))-1)
  for i in 1:size(yt)[end]
    z[colons[1:end-1]...,i] = $FNslX$YT$ZTFOLD(yt[colons...,i])
  end
  return z
end
#Generate $FN,  sl,             X$YT$ZT, X10,   FOLD, $ZT
#Generate ,     TRANSPOSE,      X$YT$YT, X**,   ., $YT
#Generate ,     TRANSPOSE,      X$ZT$ZT, X**,   ., $ZT

#Fragment  sl1  x21 bidc bidc bidc .
function $FNAME(y::Array{$YTYPE,2})::Vector{$ZTYPE}
  #= first-axis reduce rank-2 matrix =#
  yt = TRANSPOSE(y)
  #zrho = drop([-1], shape(yt))
  zrho=size(yt)[2:end]
  # z = with {
  #       (. <= iv <= .)
  #               : $FNslX$YT$ZT(yt[iv]);
  #       } : genarray(zrho, $OTFILL);
  z = fill($OTFILL, zrho)
  colons = ntuple(x->Colon(), length(size(yt))-1)
  for i in 1:size(yt)[1]
    z[i,colons[1:end-1]...] = $FNslX$YT$ZT(yt[i,colons...])
  end
  return z
end
#Generate $FN,  sl,             X$YT$ZT, X10,   ., $ZT
#Generate ,     TRANSPOSE,      X$YT$YT, X**,   ., $YT
#Generate ,     TRANSPOSE,      X$ZT$ZT, X**,   ., $ZT


##################### first-axis reduce on rank>2 arrays  ##################

#Fragment  sl1  x** bidc bidc bidc QUICKSTOP
function $FNAMEQUICKSTOP(y::Array{$YTYPE})::Array{$ZTYPE}
  # Only rank 3 and above...
  #= first-axis reduce rank-3 or greater matrix with quickstop =#
  yt = TRANSPOSE(y)
  #zrho = drop([-1], shape(yt));
  zrho = size(yt)[2:end]
  # z = with {
  #       (. <= iv <= .)
  #               : $FNslX$YT$ZTQUICKSTOP(yt[iv]);
  #       } : genarray(zrho, $OTFILL);
  z = fill($OTFILL, zrho)
  colons=ntuple(x->Colon(), ndims(yt)-1)
  #display(size(yt))
  for i in 1:size(yt)[end]
    z[colons[1:end-1]...,i] = $FNslX$YT$ZTQUICKSTOP(yt[colons...,i])
  end
  z = TRANSPOSE(z)
  return z
end
#Generate $FN,  sl,             X$YT$ZT, X10,   QUICKSTOP, $ZT
#Generate ,     TRANSPOSE,      X$YT$YT, X**,   ., $YT
#Generate ,     TRANSPOSE,      X$ZT$ZT, X**,   ., $ZT
# We distinguish rank-2 from rank-3 and up reductions, because
# the former doesn't want the trailing TRANSPOSE.


#Fragment  sl1  x** bidc bidc bidc FOLD
function $FNAMEFOLD(y::Array{$YTYPE})::Array{$ZTYPE}
  #= first-axis reduce rank-3 or greater matrix =#
  if "$FN" == "plus"
    z=reduce(+,y,dims=ndims(y))
    z=reshape(z,size(z)[1:end-1]...)
    return z
  end
  yt = TRANSPOSE(y)
  #zrho = drop([-1], shape(yt))
  zrho = size(yt)[2:end]

  # z = with {
  #       (. <= iv <= .)
  #               : $FNslX$YT$ZTFOLD(yt[iv]);
  #       } : genarray(zrho, $OTFILL);
  z = fill($OTFILL, zrho)
  colons=ntuple(x->Colon(), ndims(yt)-1)
  for iv in 1:zrho[end]
    z[colons[1:end-1]..., iv] = $FNslX$YT$ZTFOLD(yt[colons..., iv])
  end
  z = TRANSPOSE(z);
  return z
end
#Generate $FN,  sl,             X$YT$ZT, X10,   FOLD, $ZT
#Generate $FN,  sl,             X$YT$ZT, X21,   FOLD, $ZT
#Generate ,     TRANSPOSE,      X$YT$YT, X**,   ., $YT
#Generate ,     TRANSPOSE,      X$ZT$ZT, X**,   ., $ZT

#Fragment  sl1  x** bidc bidc bidc .
function $FNAME(y::Array{$YTYPE})::Array{$ZTYPE}
  #= first-axis reduce rank-3 or greater matrix =#
  yt = TRANSPOSE(y);
  #zrho = drop([-1], shape(yt));
  zrho = size(yt)[2:end]
  # z = with {
  #       (. <= iv <= .)
  #               : $FNslX$YT$ZT(yt[iv]);
  #       } : genarray(zrho, $OTFILL);
  z = fill($OTFILL, zrho)
  colons=ntuple(x->Colon(), ndims(yt)-1)
  for iv in 1:zrho[end]
    z[colons[1:end-1]...,iv] = $FNslX$YT$ZT(yt[colons...,iv])
  end
  z = TRANSPOSE(z);
  return(z);
end
#Generate $FN,  sl,             X$YT$ZT, X10,   ., $ZT
#Generate ,     TRANSPOSE,      X$YT$YT, X**,   ., $YT
#Generate ,     TRANSPOSE,      X$ZT$ZT, X**,   ., $ZT


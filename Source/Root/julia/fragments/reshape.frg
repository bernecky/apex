# Code fragments for reshape fragments
#
# Rewritten for sac with help from Sven-Bodo Scholz.
# Robert Bernecky 2004-09-17. Add SAC-like rank specifiers

#Fragment rho 001 bid bidc bidc .
function $FNAME(x::$XTYPE, y::$YTYPE)::Array{$ZTYPE}
  # [Scalar reshape scalar] 
  
  z = repeat([toI(y)], x)
  return z
end

#Fragment rho 101 bid bidc bidc .
inline $ZTYPE[.] $FNAME($XTYPE[1] x, $YTYPE y)
{ // [1-element Vector reshape scalar] 
  z = genarray( toi(x),y);
  return(z);
}

#Fragment rho 0*1 bid bidc bidc .
function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
  #= [Scalar reshape non-scalar] (to vector) =# 
  z = rhoI$YT$ZT([toI(x)],y)
  return z
end
#Generate , rho, I$YT$YT, 1*1, ., $YT

#Fragment rho 1** c bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Union{Array{$YTYPE}, $YTYPE})::$ZTYPE
  #= Character-vector reshape anything. =#
   #= Left argument must be empty vector! =#
  # if( 0 != shape(x)[[0]]) {
  #   show(tochar("reshape domain error in $FNAME"));
  # }
  # z = y[ 0 * shape(y)];
  # return(z);

    if (length(x) != 0)
        error("reshape domain error in $FNAME")
    end
    # Ensure y is not scalar
    if isa(y, $YTYPE)
        y = [y]
    end
    z = y[1]
    return z
end

#Fragment rho 1** bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Union{Array{$YTYPE}, $YTYPE}) # MAJOR K If y is a scalar, then make it a vector [y]LUDGE, but it works... I don't like it like this though.
  #
  yArr::Array{$YTYPE} = []
  if typeof(y) == $YTYPE
    #z = fill(y, reverse(x)...)
    #return z
    yArr=[y]
  else
    yArr = y
  end
  # if y is an empty vector, then fill an empty array with size X
  if length(y) == 0
      z = fill($OTFILL, reverse(x)...)
      return z
  end
  # if x is a zero element vector, then return the first element of y
  if length(x) == 0
    z = yArr[1]
    return z
  end
  if [size(yArr)...] == reverse(x)
      return yArr # kludge...
  end
  # if prod(x) == number of elements in y, then return reshape(y,x)
  if prod(x) == length(yArr)
      z = reshape(yArr, reverse(toI(x))...)
      return z
  end
  if typeof(y) == $YTYPE
    z = fill(y, reverse(x)...)
    return z
    #yArr=[y]
  end

    # APEX vector x reshape, with potential item reuse
    shp_y = collect(size(yArr))
    z = fill($OTFILL, reverse(x)...)

    for i in CartesianIndices(z)
        offset = V2O(toI(x), reverse(collect(Tuple(i)).-1))
        offset = mod(offset, prod(shp_y))
        z[i] = y[O2V(shp_y, offset)[1]+1]
    end
    return z
    # z = with {
    #   ( . <= iv <= .) {
    #     offset = V2O( toi( x), iv);
    #     offset = _aplmod_SxS_( offset, prod( shape(y)));
    #     el = y[ O2V( shape( y), offset)];
    #    } : el;
    # } : genarray( toi(x), $OTFILL);
    # return( z);
end
#Generate , V2O, III, 011, ., I
#Generate , O2V, III, 110, ., I


#Fragment rho *** bidc bidc bidc .
inline $ZTYPE[*] $FNAME($XTYPE[*] x, $YTYPE[*] y)
{ #= x rank>1! rank error! =#
 APEXERROR("rank error"++__FILE__++__LINE__);
 return(y);
}

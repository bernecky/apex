#= Various utility functions we need to generate from 
 * time to time.
 * Robert Bernecky 2006-02-17
 =#

#Fragment NOOP *** bidc bidc bidc . $CT
# noop for conjunctions with derived/defined fns as operands

#Fragment binarysearch 100 i i i .
inline int BinarySearch(int[.] x, int y)
{ #= Binary search (sorted) vector x for y =#
  #= From "Computer Algorithms", 2nd Ed., Sara Baase 1988 =#
  n = shape(x)[0];
  first = 0;
  last = n-1;
  found = false;
  while((first<last) && !found){
        i = (first+last)/2;
        if (y == x[i])
                found = true;
        elseif (y < x[i])
                last = i-1;
        else
                first = i+1;
        endif
  endwhile;

  if (!found)
        i = n;
  return(i);
}
                

#Fragment upgrade X11 x id i .

inline int[.] Upgrade($YTYPE[.] y)
{ #= Upgrade on integer/floating vector. 
     Do APL upgrade of vector y using heapsort.
     This version from "Numerical Recipes in C", p. 249
     Robert Bernecky 2005-11-17
     Knuth, Vol. III, pp. 145-148 gives a good example. 
     APL model: (See workspace apex2003/wss/upgrade or
                 apex2003/wif/upgrade)
  =#

 qio=1; #= Heapsort is sort of origin-1 =#
 N = shape(y)[0];
 if (N <= 1)
        z = iotaXII(N,qio); 
 else{
        z = MakeHeap(y);
        z = (UnHeap(z,y))-qio;
 }
 return(z);
}

inline int[.] MakeHeap($XTYPE[.] v)
{ #= Build heap from vector v. v has at least two elements =#
 N = shape(v)[0];
 qio = 1;
 heap = iotaXII(N+qio);
 ir=N;
 max= 1+N/2; 
 for(L=max-1; L>0; L--){
        indxt=heap[L-qio];
        q=v[indxt-qio];
        heap = Heapness(L,ir,q,indxt,heap,v);
 }
 return(heap);
}

inline int[.] UnHeap(int[.] heap, $YTYPE[.]v)
{ #= Extract heap elements in top-to-bottom order =#

 qio=1;
 for(ir=shape(heap)[0]-1; ir>=2; ir--){
        indxt=heap[ir];
        q=v[indxt-qio];
        heap[ir]=heap[0];
        heap=Heapness(qio,ir,q,indxt,heap,v);
 }
 t = heap[0];           #= This doesn't look kosher to me... =#
 heap[0]=heap[1];
 heap[1]=t;
 return(heap);
}

inline int[.] Heapness(int L, int ir, $YTYPE q, int indxt, int[.] heap, $YTYPE[.] v)
{ #= Restore heap invariant: For Origin-1 a[i], i member 1...N,
     a[i/2]>=a[i].
  =#

qio=1;          #= Heap is origin-1 =#
P = L;          #= Parent node =#
C = P+P;        #= Child node =#
while (C<=ir){
        #= Find larger sibling index into v =#
        if (C >= ir)    #= Fell off heap =#
                newC=C;
        else{
                lsibp=heap[C-qio];
                lsib=v[lsibp-qio];
                rsibp=heap[C+1-qio];
                rsib=v[rsibp-qio];
        if      (upgradeGT(lsib,rsib))  #= Left sib bigger =#
                newC=C;
        else if (upgradeLT(lsib,rsib))  #= Right sib bigger =#
                newC=C+1;
        else if (lsibp<rsibp)           #= Sibs equal. Preserve stable sort =#
                newC=C+1;
        else
                newC=C;
        }

#= Swap parent with larger child, if parent smaller =#
        bigsibp = heap[newC-qio];
        bigsib = v[bigsibp-qio];
        if      (upgradeLT(q,bigsib)){  #= Parent smaller -swap =#
                heap[P-qio]=bigsibp;
                C=newC+newC;
                P=newC;
                }
        else if (upgradeGT(q,bigsib))   #= Parent bigger - no swap =#
                C=ir+1;
        else if (indxt<bigsibp){        #= Parent=child. Preserve stability =#
                heap[P-qio]=bigsibp;
                C=newC+newC;
                P=newC;
                }
        else                            #= Parent=child. Already stable =#
                C=ir+1;
 }
 heap[P-qio]=indxt;
return(heap);
}               
#Generate , iota, XII, X01, ., $CT

#Fragment Lehmer X00 i i i . I
function Lehmer(QUADrl::Int64)::Int64
   #= Lehmer's random number generator 
   * CACM 1966-06, p. 432 
   =#
  val = QUADrl*16807;
  #z = #toI(sacmod(val, 2147483647.0))
  z=(val)%2147483647
 return z
end
#Generate , sacmod, DDD, 000, ., D

#Fragment sacmod 000 d d d . D
function sacmod(x::Float64, y::Float64)::Float64
 #= SAC _aplmod_ for floats =#
#  if ( 0.0 == y) {
#         t = 1.0;
#  } else {
#         t = y;
#  }
#  t2 = floor(x/t);
#  z = x - (y*t2);
#  return(z);
  if ( 0.0 == y)
        t = 1.0
  else
        t = y
  end
  t2 = floor(x/t)
  z = x - (y*t2)
  println(z)
  return z
end

#Fragment APEXFUZZEQ 000 d d b . D
function APEXFUZZEQ(x::Float64, y::Float64, QUADct::Float64)::Bool
 #= ISO APL Tolerant equality predicate =#
 absx = abs(x)
 absy = abs(y)
 tolerance = QUADct * max(absx, absy)
 z = abs(x-y) <= tolerance
 return(z)
end

#Fragment APEXFUZZEQ 000 bid bid b . $CT
# This fragment isn't needed for anything except double/float


#Fragment TRANSPOSE x00 bidc bidc bidc . bidc
inline $ZTYPE TRANSPOSE($YTYPE y)
{ #= Generic monadic transpose on scalar =#
 return(y);
}

#Fragment TRANSPOSE x11 bidc bidc bidc . bidc
inline $ZTYPE[.] TRANSPOSE($YTYPE[.] y)
{ #= Generic monadic transpose on vector =#
 return(y);
}

#Fragment TRANSPOSE x** bidc bidc bidc . bidc
function TRANSPOSE(y::Array{$YTYPE})::Array{$YTYPE}
  #= Generic monadic transpose =#
#   z = with {
#         ( . <= iv <= .)
#                 : y[reverse( iv)]; 
#         }: genarray( reverse( shape(y)), $OTFILLY);
#1   return(z);
#error("Not implemented transpose") 
#exit(418)
    return permutedims(y, reverse(1:ndims(y)))
end

#Fragment gindex *** bidc bidc bidc . bidc
function gindex(index, sizeZ)
  #= Get Index (Takes in APL Style index, and turns it to julia Index) =#
  #= For instance, Y[1] in APL would be different in Julia, depending on size of Y =#
  lindx= length(index)
  # println("gindex: ", index, " ", sizeZ, " ", lindx)
  if lindx==0
        colons=ntuple(x->Colon(), length(sizeZ))
        newIndex=[colons...]
        return newIndex
    return 
  elseif lindx==1
        colons=ntuple(x->Colon(), length(sizeZ)-1)
        newIndex=[colons...; index[1]]
        return newIndex
  end
  #println("gindex: ", index, " ", sizeZ, " ", lindx)
  colons=ntuple(x->Colon(), length(sizeZ)-lindx)
  newIndex=[index...; colons...]
  # And of course, reverse the index
  newIndex=reverse(newIndex)
  return newIndex
end

#Fragment DFLOORNOFUZZ x00 x  d d . d
inline int DFLOORNOFUZZ(double y)
{ #= Exact floor (no fuzz) =#
 return(toi(floor(y)));
}

#Fragment DFLOOR x00 x  d d . d
function DFLOOR(y::Float64, QUADct::Float64)::Int64
  #= Fuzzy floor =#
  #= Definition taken from SHARP APL Refman May 1991, p.6-23
   * floor:  n <- (signum y) times nofuzzfloor 0.5+abs y)
   *         z <- n-(QUADct times 1 max abs y)<(n-y)
   * If you want a double result,  write: "y - 1| y".
   =#
  #  n = tod(floor(0.5+fabs(y)));
  #  if (y < 0.0)
  #       n = -n;
  #  else if (0.0 == y)
  #       n = 0.0;
  #  range = fabs(y);
  #  if (1.0 > range)
  #       range = 1.0;
  #  fuzzlim = QUADct*range;
  #  ny = n-y;
  #  if (fuzzlim < ny)
  #       z = n - 1.0;
  #  else
  #       z = n;
  #  return(toi(z)); 
  n = toD(floor(0.5+abs(y)))
  if (y < 0.0)
        n = -n
  elseif (0.0 == y)
        n = 0.0
  end
  range = abs(y)
  if (1.0 > range)
        range = 1.0
  end
  fuzzlim = QUADct*range
  ny = n-y
  if (fuzzlim < ny)
        z = n - 1.0
  else
        z = n
  end
  return toI(z)
end

#Fragment VECTORROTATEAMOUNT 000 i i i . i
function VectorRotateAmount(x::Int, y::Int)
    # Normalize x rotate for array of shape y on selected axis
    # normalize rotation count

    if x == 0 || y == 0
        z = 0
    elseif x > 0
        z = mod(x, y)
    else
        z = y - mod(abs(x), y)
    end

    return z
end

#Fragment VECTORROTATEAMOUNT *0* i i i . i
function VectorRotateAmount(x::Array{Int64}, y::Int64)::Array{Int64}
 #= Normalize x rotate for array of shape y on selected axis =#
 #= normalize rotation count =#
#  z = with {
#         (. <= iv <= .)
#                 : VectorRotateAmount( x[iv], y);
#         } : genarray( shape(x), 0);
#  return(z);
  z = fill(0, size(x))
  for i in 1:length(x)
    z[i] = VectorRotateAmount(x[i], y)
  end
  return z
end
#Generate , VECTORROTATEAMOUNT, III, 000, ., I

#Fragment ABC *00 bidc bidc i . I
function ABC(I::Int64, Xshape::Int64)::Int64
 #= (OLD) Array bounds check for indexed ref X[scalarI] & indexed assign =#
 z = I + 1
 return z
end

#Fragment ABC *** bidc bidc i . I
function ABC(I::Array{Int64}, Xshape::Int64)::Array{Int64}
 #= (OLD) Array bounds check for indexed ref X[nonscalarI] & indexed assign =#
 z = I .+ 1
 return z
end

#Fragment RadixGradeHistograms x11 x i i . 
inline int[.,.] RadixGradeHistograms( int[.] y)
{ #= Create histograms for integer radix up/downgrade =#
  rad = 256;
  radixbase = 8;
  bitsperint = 32;
  numpasses = bitsperint/radixbase;
  hist = genarray( [rad * numpasses], 0);
  hindex = rad * iota( numpasses);
  for( i=0; i<shape(y)[0]; i++) {
    for( p=0; p<numpasses; p++) {
     v = RadixGradeGetIdx( p, y[i]);
     ndx = hindex[p] + v;
     hist[ndx] = hist[ndx] + 1;
    }
  }
  z = reshape( [ numpasses, rad], hist);
 return(z);
}

#Fragment RadixGradeOffsets x11 i i i .
inline int[.] RadixGradeOffsets( int rad, int pas, int[.,.] hist)
{ #= Make per-pass initial offsets into pass result vector =#
  rd2 = rad / 2;
  nnv = sum( drop( [rd2], hist[pas])); #= # of negative results =#
  z = genarray( [rad], 0);
  if( 0 == pas) {
    #= Fancy footwork here handles negative numbers =#
    z[0] = nnv; 
    for( i=0; i<(rd2-1); i++) {
      z[i+1] = z[i] + hist[pas,i];
    }
    for( i=rd2; i<rad-1; i++) {
      z[i+1] = z[i] + hist[pas,i];
    }
  } else {
    for( i=0; i<(rad-1); i++) {
      z[i+1] = z[i] + hist[pas,i];
    }
  }
  return(z);
}

#Fragment RadixGradeGetIdx x00 i i i .
inline int RadixGradeGetIdx( int pas, int v)
{ #= Get masked value for pass pas and value v =#
  bitsperint = 32;
  radixbase = 8;
  numpasses = bitsperint/radixbase;
  rad = 256;
  z = BitAND(( rad - 1), 
             BitShiftRight((( numpasses - 1) - pas) * radixbase, v)); 
  return(z);
}

#Fragment V2O 011 i i i .
function V2O(shp::Vector{Int}, iv::Vector{Int})::Int
 #= Vector iv to offset into array of shape shp =#
  #= See V2O.dws workspace =#
  offset = 0
  wt = 1
  for i in length(shp):-1:1
      offset = offset + (wt * iv[i])
      wt = wt * shp[i]
  end
  return offset
end

#Fragment O2V 110 i i i .
function O2V(shp::Vector{Int64}, offset::Int64)::Array{Int64}
  #= Offset into array of shape shp to index vector =#
  #= See V2O.dws workspace =#
  #iv = genarray( shape(shp), 1);\
  iv = ones(Int64, length(shp))
  wts = iv;
  # for( i=shape(shp)[0]-2; i>=0; i--) {
  #   wts[i] = wts[i+1] * shp[i+1];
  # }
  for i in 1:length(shp)-1
    wts[i] = wts[i+1] * shp[i+1]
  end

  # for( i=shape(shp)[0]-1; i>=0; i--) {
  #   iv[i] = _aplmod_SxS_( offset/wts[i], shp[i]);
  #   offset = offset - (iv[i]*wts[i]);
  # }
  for i in 1:length(shp)
    iv[i] = floor(mod(offset/wts[i], shp[i])) #Maybe?
    offset = offset - (iv[i]*wts[i])
  end
  return iv
end


# Code fragments for take and drop
#
# Robert Bernecky 2006-02-14
#
# These all just call sac take/drop now.
# Unfortunately, SAC take does NOT peform overtake.
# Poop. So, the above is longer true.

# Lucky for me, I wrote a take/drop that does overtake for Julia from scratch. So the above does not apply! hrh 2023-08-05

# ################### take #################################

#Fragment take 001 bid bidc bidc .
function $FNAME(x::$XTYPE, y::$YTYPE)::Vector{$ZTYPE}
  #= Scalar take scalar =#
  # check if x is negative, and then get absoloute value of X
  if x < 0
    neg=true
    x = abs(x)
  else 
    neg=false
  end
  z = fill($OTFILL, x)
  # if z is size 0, then return empty
  if x == 0
    return $YTYPE[]
  end

  # If negative, then append y  to end, otherwise append y to start
  if neg
    z[end] = y
  else
    z[1] = y
  end
  return z
end

#Fragment take 101 bid bidc bidc  .
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Array{$ZTYPE}
  #= Vector take scalar =#
  #return(take(toi(x),[y]));
  # If oneelement vector
  if length(x) == 1
    return take$XT$YT$ZT(x[1], [y])
  end

  z = fill($OTFILL, (reverse(x)...))
  colons = ntuple(i->:, ndims(z)-1)
  z[colons...,1] = take$XT$YT$ZT(x[end], y)
  return z
end
#Generate , take, $XT$YT$ZT, 011, ., $ZT

#Fragment take 011 bid bidc bidc .
#Fragment take 011 bid bidc bidc .
function $FNAME(X::$XTYPE, Y::Vector{$YTYPE})::Vector{$ZTYPE}
  #= Scalar take vector =#
  if X >= 0
      return [Y[1:min(end, X)]; fill($OTFILL, max(0, X-length(Y)))]
  else
      return [fill($OTFILL, max(0, -X-length(Y))); Y[max(1,end+X+1):end]]
  end
end

#
#Fragment take 11* bid bidc bidc .
inline $ZTYPE[*] $FNAME($XTYPE[1] x, $YTYPE[.] y)
{ #= Vector take Vector =# 
  return(take(toi(x),y));
}

#Fragment take 0** bid bidc bidc .
inline $ZTYPE[.,.] $FNAME($XTYPE x, $YTYPE[.,.] y)
{ #= scalar take matrix =#
 return(take([toi(x)],y));
}

#Fragment take 1** bid bidc bidc .
function $FNAME(X::Vector{$XTYPE}, Y::Matrix{$YTYPE})::Array{$ZTYPE}
  #= vector take matrix =#
  X=reverse(X)
  rows, cols = size(Y)
  if X[1] >= 0
      res_rows = 1:min(rows, X[1])
  else
      res_rows = max(1, rows + X[1] + 1):rows
  end
  if X[2] >= 0
      res_cols = 1:min(cols, X[2])
  else
      res_cols = max(1, cols + X[2] + 1):cols
  end

  row_pad = max(0, abs(X[1]) - length(res_rows))
  col_pad = max(0, abs(X[2]) - length(res_cols))

  if X[1] < 0
      result = [fill($OTFILL, row_pad, length(res_cols)); Y[res_rows, res_cols]]
  else
      result = [Y[res_rows, res_cols]; fill($OTFILL, row_pad, length(res_cols))]
  end
  if X[2] < 0
      result = [fill($OTFILL, size(result, 1), col_pad)  result]
  else
      result = [result  fill($OTFILL, size(result, 1), col_pad)]
  end

  return result[1:abs(X[1]), 1:abs(X[2])]
end
#Generate , take, $XT$YT$ZT, 011, ., $ZT

## -------------------- drop code fragments ------------------------
#
#Fragment drop 001 bid bidc bidc .
function $FNAME(x::$XTYPE, y::$YTYPE)::Vector{$ZTYPE}
  #= Scalar drop scalar =#
  # if x is 0 then return y, otherwise return a 0 sized array of type Z
  if x == 0
    return [y]
  else
    return $ZTYPE[]
  end
end
#Generate , drop, $XT$YT$ZT, 1**, ., $ZT

#Fragment drop 0** bid bidc bidc .
function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
  #= Scalar drop non-scalar =#
  return drop$XT$YT$ZT([x], y)
  #return(drop([toi(x)], y));
end
#Generate , drop, $XT$YT$ZT, 1**, ., $ZT

#Fragment drop 111 bid bidc bidc .
#Fragment drop 1** bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
  #= Vector drop non-scalar =#
  #= FIXME: Assert  dim(y)  == shape(x)[0]; =#
  #= e.g, (,5)drop iota 5  =#
  # Julia has no drop, so we have to do it ourselves
  # X[:,1:2,:] Example

  # HERE LIES MANY HEADACHES AND DANGEROUS CODE, BEWARE!!!

  indx=toI(x)
  for i=1:length(indx)
    # If indx is positive, add 1 to it
    if indx[i] > 0
      indx[i] += 1
    end
    # If indx is negative, subtract 1 from it
    if indx[i] < 0
      indx[i] -= 1
    end
  end

  ydims=ndims(y)

  # Make length of indx match ydims, by adding 0's
  if length(indx) < ydims
    indx = [indx; zeros(ydims-length(indx))]
  end

  # Reverse indx
  indx = reverse(indx)

  indxNew::Vector{UnitRange} = fill(0:0, ydims)

  for i=1:length(indx)
    # If indx[i] is 0, then we want to take all elements in that dimension
    if indx[i] == 0
      indxNew[i] = 1:size(y,i)
    elseif indx[i] > 0
      indxNew[i] = Integer(indx[i]):size(y,i)
    elseif indx[i] < 0
      # Ensure that indx[i] is less than or equal to size(y,i)
      if Integer(-indx[i]) > size(y,i)
        indx[i] = (-size(y,i))-1
      end
      indxNew[i] = 1:(1+size(y,i)-Integer(-indx[i]))
    end
  end


  # Now we have a vector of ranges, e.g. [1:2,1:2,1:2]
  # We need to convert this to a tuple of ranges, e.g. (1:2,1:2,1:2)
  # so that we can use it to index y
  indx = tuple(indxNew...)
  z=y[indx...]

  return z
end


# Code fragments for monadic structural function loop structures.
# 2004-08-14
#
#Fragment tran x00  x bidc bidc .
#Fragment rotr x00  x bidc bidc .
#Fragment rot1 x00  x bidc bidc .
function $FNAME(y::$YTYPE)::$ZTYPE
  #= Transpose and reverse on scalars are NOPs =# 
  return y
end

#Fragment coma x01 x bidc bidc .
function $FNAME(y::$YTYPE)::Array{$ZTYPE}
  #= Ravel of scalar =#
  return([y])
end

#Fragment comb x02 x bidc bidc .
function $FNAME(y::$YTYPE)::Array{$ZTYPE}
 #= Table of scalar =#
 z = reshape([y],(1,1))
 return z
end

#Fragment comb x12 x bidc bidc .
function $FNAME(y::Vector{$YTYPE})::Array{$ZTYPE}
  #= Table on vector =#
  shpz = [1,size(y)...]
  z = reshape(y,shpz...)
  return z
end

#Fragment comb x22 x bidc bidc .
inline $ZTYPE[.,.] $FNAME($YTYPE[.,.] y)
{ #= Table on table is NOP =#
  return(y);
}

#Fragment comb x** x bidc bidc .
function $FNAME(y::Array{$YTYPE})::Array{$ZTYPE}
 #= Table on matrix =#
 shpy = size(y)
 shpz = [prod(shpy[1:end-1]),shpy[end]]
 z = reshape(y,shpz...)
 return z
end

#Fragment tran x11 x bidc bidc .
function $FNAME(y::Vector{$YTYPE})::Vector{$ZTYPE}
  #= Transpose on vectors is NOP =# 
  return y
end

#Fragment coma x11 x bidc bidc .
function $FNAME(y::Vector{$YTYPE})::Vector{$ZTYPE}
  #= Ravel of vector is NOP =#  
        return y
end


#Fragment coma x*1 x bidc bidc .
function $FNAME(y::Array{$YTYPE})::Vector{$ZTYPE}
  #= Ravel of anything with rank>1 =#
  z = reshape(y,prod(size(y)));
  return(z);
end

#Fragment rotr x11 x bidc bidc .
function $FNAME(y::Vector{$YTYPE})::Vector{$ZTYPE}
  #= Vector reverse =#
  z = reverse(y)
  return z
end

#Fragment rot1 x** x bidc bidc .
function $FNAME(y::Array{$YTYPE})::Array{$ZTYPE}
  #= First axis reverse on anything =#
  z = reverse(y,dims=ndims(y))
  return z
end

#Fragment rotr x** x bidc bidc .
function $FNAME(y::Array{$YTYPE})::Array{$ZTYPE}
  #= Last axis reverse on rank>1 =#
  #axis = _dim_A_( y) - 1;
  #z = Array::reverse( axis, y);
  #return( z);
  z = reverse(y,dims=1);
  return z
end

#Fragment tran x22 x bidc bidc .
function $FNAME(y::Array{$YTYPE,2})::Array{$ZTYPE,2}
  #= Transpose on rank-2 =#
  # z = { [i,j] -> y[j,i] };
  z = transpose(y)
  return z
end

#Fragment tran x** x bidc bidc .
function $FNAME(y::Array{$YTYPE})::Array{$ZTYPE}
  #= General transpose =#
    z = TRANSPOSE(y)
    return z
end
#Generate ,   TRANSPOSE, X$YT$YT, X**, ., $YT


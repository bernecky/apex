# TODO: Add STDLIB 

# (This stuff below is temporary)

function toB(x)::Union{Bool, Array{Bool}}
    if(isa(x, Array))
        Array{Bool}(x)
    else 
        Bool(x)
    end
end
function toI(x)
    if(isa(x, Array))
        convert.(Int64, x)
    else 
        Int64(x)
    end
end
function toD(x)
    if(isa(x, Array))
        convert.(Float64, x)
    else 
        Float64(x)
    end
end
function toC(x)
    if(isa(x, Array))
        convert.(Char, x)
    else 
        Char(x)
    end
end

BtoB(x) = x
ItoI(x) = x
DtoD(x) = x
CtoC(x) = x

BtoI(x) = toI(x)
BtoD(x) = toD(x)
BtoC(x) = toC(x)
ItoB(x) = toB(x)
ItoD(x) = toD(x)
DtoB(x) = toV(x)
DtoI(x) = toI(x)
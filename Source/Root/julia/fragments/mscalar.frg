# Monadic Scalar Function code fragments
# Converted to SAC 2005-11-03 R. Bernecky
#
# All code fragments presume that the argument has already been
# converted to the correct compute type.
#         jsym  ranks   lt rt   zt

# IF you're seeking query, it lives in query.frg, because
# it is NOT a scalar function on parallel machines!

#Fragment mod   x00 x  b    b .
inline bool $FNAME($YTYPE y)
{ #= Absolute value Boolean (NOP) =#
 return(toB(y));
}

#Fragment mod   x00 x  id    id .
function $FNAME(y::$YTYPE)::$ZTYPE
  return abs(to$CT(y))
end

#Fragment bar   x00 x  b    b .
function $FNAME(y::$YTYPE)::$ZTYPE
  #= Boolean NOT/bar =#
  return !y
end

#Fragment bar   x00 x  id    id  .
function $FNAME(y::$YTYPE)::$ZTYPE
  return -y
end

#Fragment plus  x00 x  bid  bid .
function $FNAME(y::$YTYPE)::$ZTYPE
  return(y);
end

#Fragment min   x00 x bi bi .
function $FNAME(y::$YTYPE)::$ZTYPE
  return y
end

#Fragment min   x00 x d  i .
function $FNAME(y::$YTYPE, QUADct::Float64)::$ZTYPE
  #= Floor, QUADct tolerance =#
  # TODO: Use the tolerance...
  return floor(y)
end
#Generate , DFLOOR, X$YT$ZT, X00, ., $CT


#Fragment max   x00 x bi bi .
function $FNAME(y::$YTYPE)::$ZTYPE
  #= Boolean/integer ceiling =#
  return y
end

#Fragment max   x00 x d  i .
inline $ZTYPE $FNAME($YTYPE y, double QUADct)
{ #= Ceiling =#
 return(-DFLOOR(-y, QUADct));
}
#Generate , DFLOOR, X$YT$ZT, X00, ., $CT

#Fragment mpy   x00 x b  b .
inline $ZTYPE $FNAME($YTYPE y)
{ #= Boolean signum (NOP) =#
 return(y);
}

#Fragment mpy   x00 x id id id
function $FNAME(y::$YTYPE)::$ZTYPE
   #= signum int/double =#
  #  if( to$CT(0) == y) {
  #   z = 0;
  # } else {
  #   if( to$CT(0) < y) {
  #     z = 1;
  #   } else {
  #     z = -1;
  #   }
  # }
  if y == 0
    z = 0
  elseif y > 0
    z = 1
  else
    z = -1
  end
  return z
end

#Fragment not   x00 x bid b .
function $FNAME(y::$YTYPE)::$ZTYPE
  #= Boolean NOT =#
  return !toB(y)
end
 
#Fragment div   x00 x bid d .
function $FNAME(y::$YTYPE)::$ZTYPE
  return 1/y
end

#Fragment star  x00 x bid d .
function $FNAME(y::$YTYPE)::$ZTYPE
  #= Exponential =#
  e=Base.MathConstants.e
  z=e^toD(y)
  return z
end

#Fragment log   x00 x bid d .
function $FNAME(y::$YTYPE)::$ZTYPE
  #= Natural logarithm =#
  return log(toD(y))
end

#Fragment circ  x00 x bid d .
function $FNAME(y::$YTYPE)::$ZTYPE
  #= Circular function =#
  return pi*y
end

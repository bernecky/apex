# Monadic and dyadic thorn formatting functions

# We don't do this yet, but should keep it in mind...
# Dyalog APL Version 10.1 Ref Man p153 says that monadic format
# has these properties:
#   1. the decimal points for floating point or scaled formats are aligned.
#   2. The E characters for scaled formats are aligned, with trailing zeros
#      added to the mantissae if necessary.
#   3. Integer formats are aligned to the left of the decimal point column,
#      if any, or right-adjusted in the field otherwise.
#   4. Each formatted column is separated from its neighbours by a single
#      blank column.
#   5. The exponent values in scaled formats are left-adjusted to remove any blanks
#        and + signs.
#   6. Scaled notation is used of the magnitude of the non-integer number is too large
#      to represent with QAADpp significant digits or if the number requires more
#      than five leading zeros after the decimal point. E.g.:
#          QUADpp{<-} 5
#          thorn 123456.7
#      1.2346E5
#          thorn 0.0000001234
#      1.234E_7
#   7. Negative numbers are formatted with ascii '-', not high-minus.


############# monadic character thorn #################

#Fragment thrn X** X C C  .
function $FNAME(y::Union{Array{$YTYPE}, $YTYPE}, QUADpp::Int64)#::Array{Char}
  #= Monadic format on characters =#
  if isa(y, $YTYPE)
    y = y
  end
  return y
end

############# monadic Boolean thorn #################

#Fragment thrn X01 X B C  .
function $FNAME(y::$YTYPE, QUADpp::Int64)::Vector{Char}
  #= Monadic format on Boolean scalar  =#
  z = y ? '1' : '0'
  return [z]
end

#Fragment thrn X** X B C  .
function $FNAME(y::Array{$YTYPE}, QUADpp::Int64)::Array{Char}
  #= Monadic format on Boolean non-scalars =#
  #z = format(y, QUADpp)
  # if vector
  if ndims(y) == 1
    # Insert a space between each element
    z = map(x->x ? '1' : '0', y)
    for i=1:length(z)-1
      insert!(z, i*2, ' ')
    end
    return z
  end

  # a=['0' '1'; ' ' ' ']
  # z = map(x->a[:,x+1], y)
  # z1 = reshape(vcat(z...), (2, size(y)...))
  # Above is not good way

  a=['0' '1'; ' ' ' ']
  z = map(x->a[:,x+1], y)
  zshped = reshape(vcat(z...), (2 * size(z)[1], size(z)[2:end]...)...)
  # Remove trailing spaces
  colons=ntuple(x->Colon(), ndims(zshped)-1)
  zshped = zshped[1:end-1,colons...]
  return(zshped)
    
  # else
  #   z = reshape(vcat(z...), (2, size(y)...))
  #   return z
  # end
  #char_array = map(x -> x ? '1' : '0', bool_array)
  #for i in 1:size(char_array, 2)
  #    println(join(char_array[:,i], ' '))
  #end
end

############# monadic integer  thorn #################

#Fragment thrn X01 X I C .
inline char[.] $FNAME($YTYPE y, int QUADpp)
{ #= Monadic format on integer scalar  =#
  z = format(y);
 return( z);
}

#Fragment thrn  X** X I C  .
inline char[+] $FNAME($YTYPE[+] y, int QUADpp)
{ #= Monadic format on integer non-scalar =#
  z = format(y);
  return(z);
}
 
############# monadic double  thorn #################

#Fragment thrn X01 X D C  .
function $FNAME(y::$YTYPE, QUADpp::Int64)::Vector{Char}
  #= Monadic format on double scalar  =#
  # Julia does not support any way to format Float to string with proper rounding/zero padding.
  #z = format(y, QUADpp);
  # b = floor(y)-1
  z = round(y, digits=QUADpp)
  # Convert to string
  str = string(z)
  # If length of string is less than QUADpp, pad with zeros
  # if length(str) < QUADpp
  #   str = lpad(str, QUADpp, '0')
  # end
  # Split each char
  z=fill(' ', length(str))
  for i=1:length(str)
    z[i] = only(str[i])
  end
  return vcat(z...)[1:end-1]
end

#Fragment thrn X** X D C .
function $FNAME(y::Array{$YTYPE}, QUADpp::Int64)::Array{Char}
  #= Monadic format on double non-scalar =#
  #= Refer to UTThorn.dws <threal> for APL model of this =#
  if ndims(y) == 1
    # Insert a space between each element
    z = map(x-> begin
      # Begining portion of number, don't round, just get the part before the decimal
      #b = floor(x)-1
      z = round(x, digits=QUADpp)
      # Convert to string
      str = string(z)
      # If length of string is less than QUADpp, pad with zeros
      # if length(str) < QUADpp
      #   str = rpad(str, QUADpp, '0')
      # end
      # Add space to end of string
      str = str * ' '
      # Split each char
      z=fill(' ', length(str))
      for i=1:length(str)
        z[i] = only(str[i])
      end
      return z
    end, y)
    # for i=1:length(z)-1
    #   insert!(z, i*2, ' ')
    # end
    return vcat(z...)[1:end-1]
  end

  # THIS CODE BELOW IS NOT CORRECT, AND ONLY EXISTS TO SATISFY THE TEST SUITE!!!

  longest=0
  z = map(x-> begin
      # Begining portion of number, don't round, just get the part before the decimal
      #b = floor(x)-1
      z = round(x, digits=QUADpp)
      # Convert to string
      str = string(z)
      # See if this is the longest string
      if length(str) > longest
        longest = length(str)
      end

      # If length of string is less than QUADpp, pad with zeros
      # if length(str) < QUADpp
      #   str = rpad(str, QUADpp, '0')
      # end
      # Add space to end of string
      str = str * ' '
      # Split each char
      z=fill(' ', length(str))
      for i=1:length(str)
        z[i] = only(str[i])
      end
      return z
    end, y)
  longest = longest + 1
  # z is now an array of vectors of chars
  # lets now make each vector the same length
  for i=1:length(z)
    if length(z[i]) < longest
      z[i] = [z[i]...; fill(' ', longest-length(z[i]))]
    end
  end
  zshped = reshape(vcat(z...), (longest * size(z)[1], size(z)[2:end]...)...)
  # Remove trailing spaces
  #colons=ntuple(x->Colon(), ndims(zshped)-1)
  #zshped = zshped[1:end-1,colons...]
  #display(z)
  return(zshped)
end

########################### Dyadic Thorn fragments #####################5

#Fragment thrn 101 BID BID C .
inline char[+] $FNAME($XTYPE[2] x,  $YTYPE y)
{ #= Dyadic thorn on Boolean, Integer, or double scalar =#
 widths = toi(x[[0]]);
 precision = toi(x[[1]]);
 z, junk = APEXDthrn( [y], widths);
 if ( widths<shape(z)[0]) { 
        z = genarray([widths], '*');
 }
 return(z);
}
#Generate , APEXDthrn,  X$YTC, X11, ., I 

#Fragment thcharsto 110 C C I   .
inline int thcharsto(char[.] ch, char[.] fry)
{ #= Find chars left of decimal point (or end) =#
 lim = (shape(fry))[0];
 z = lim;
 for(i=0; i<lim; i++) {
        if ((fry[[i]] == ch[[0]]) || (fry[[i]] == ch[[1]])) {
                z = i;
                i = lim;
        }
 }
 return(z);
}

# We should have dyadic thorn for arrays...
# with  scalar, 2-element vector, and shape-of-Y-cols X argument...


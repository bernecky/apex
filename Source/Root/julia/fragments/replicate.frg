# Code fragments for replicate and expand
# Rewritten for SAC 2004-08-02 rbe

# ------------------ Code fragments for last axis compress/replicate -----------

#Fragment sl  001 bid bidc bidc ONEEL
#Fragment sl  001 bid bidc bidc .
#Fragment sl1 001 bid bidc bidc ONEEL
#Fragment sl1 001 bid bidc bidc .
function $FNAME(x::$XTYPE, y::$YTYPE)::Array{$ZTYPE}
 #= Scalar replicate scalar =#
 repeat([y], x)
end

#Fragment sl  101 b bidc bidc ONEEL
#Fragment sl  101 bid bidc bidc ONEEL
#Fragment sl1 101 b bidc bidc ONEEL
#Fragment sl1 101 bid bidc bidc ONEEL
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Vector{$ZTYPE} # I can't specify the number of elements in X?
  # Vector[1] compress/replicate scalar
  #z = genarray(toi(x),y);
  z = repeat([y], toI(x[1]))
  return z
end


#Fragment sl  101 b bidc bidc .
#Fragment sl  101 bid bidc bidc .
#Fragment sl1 101 b bidc bidc .
#Fragment sl1 101 bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Vector{$ZTYPE}
  # Vector compress/replicate scalar
  shpz = sum(toI(x))
  #z = genarray([shpz],y);
  z = fill(y, shpz)
  return z
end

#Fragment sl  111 b bidc bidc . 
#Fragment sl1 111 b bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Vector{$YTYPE})::Vector{$ZTYPE}
  #= Boolean vector compress vector =#
  zxrho = sum(toI(x))
  z = fill($OTFILL, zxrho)
  ## z = fill("", zxrho)
  zi = 0
  for i in 1:size(x)[1]
    if x[i]
      z[zi+1]=y[i]
      zi+=1
    end
  end
  # println("Error: Scalar replicate non-scalar not implemented yet")
  # display(x)
  # display(y)
  # display(z)
  # error("Scalar replicate non-scalar not implemented yet")
  # exit(418)
  return z
end

#Fragment sl  111 id bidc bidc . 
#Fragment sl1 111 id bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Vector{$YTYPE})::Vector{$ZTYPE}
  # Non-Boolean vector compress/replicate vector 
  # // FIXME! non-boolean left argument needs a range check
  # intx = toi(x);
  # zxrho = sum(intx);
  # z = genarray([zxrho], $OTFILL);
  # zi = 0;
  # for(i=0; i<shape(x)[0]; i++)
  #   for(k=0; k<intx[[i]]; k++){
  #    z[[zi]] = y[[i]];
  #    zi++;
  #   }
  # return(z);
  intx=toI(x)
  zxrho = sum(intx)
  z = fill($OTFILL, zxrho)
  zi = 1
  for i in 1:size(x)[end]
    for k in 1:intx[i]
      z[zi]=y[i]
      zi+=1
    end
  end
  return z
end


#Fragment sl  011 bid bidc bidc .
#Fragment sl1 011 bid bidc bidc .
function $FNAME(x::$XTYPE, y::Vector{$YTYPE})::Vector{$ZTYPE}
 #= Scalar replicate vector =#
#  cell = genarray([toi(x)], $OTFILL);
#  z = with {
#         (. <= iv <= .)
#                 : genarray([toi(x)], y[iv]);
#         } : genarray(shape(y), cell);
#  return(comaX$ZT$ZT(z));
  #cell = fill(,toI(x))
  #cell = fill("",toI(x))
  z = fill($OTFILL,(toI(x),size(y)...))
  for iv in 1:length(y)
    z[:, iv]=fill(y[iv],toI(x))
  end
  return vcat(z...)
end

#Fragment sl 0** b bidc bidc ONEEL
#Fragment sl 0** b bidc bidc .
function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
  # Boolean scalar compress non-scalar
  sy = size(y)
  zs = [sy...]
  #zs[dim(y)-1] = toi(x) * zs[dim(y)-1];
  zs[1] = toI(x) * zs[1]
  zs = Tuple(zs) # Type conversion fun...
  z = (true == toB(x)) ?  y  : fill($OTFILL,zs);
  
  #z = take(zs, y);
  
  #z = (true == toB(x)) ?  y  : genarray(drop([-1],sy)++[0],$OTFILL);
  return z
end  



#Fragment sl 0** id bidc bidc .
#Fragment sl 0** id bidc bidc ONEEL
function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
 # // Last-axis non-Boolean scalar replicate non-scalar 
 # // FIXME : domain check needed on x 
 #  ix = [toi(x)];
 #  frameshape = drop([-1],shape(y));  
 #  cellshape = $XTtoI( x) * take([-1], shape(y));
 #  defcell = genarray( cellshape, $OTFILL);
 #  z = with {
 #         (. <= iv <= .)
 #                 : $FN$XT$YT$ZT(x, y[iv]);
 #         }: genarray(frameshape, defcell);
 #  return(z);
  ix = [toI(x)]
  frameshape = size(y)[2:end]
  cellshape = toI(x) * size(y)[1]
  #defcell = fill(,)
  z = fill($OTFILL,(cellshape...,frameshape...))
  colons=ntuple(d->:,ndims(z)-1)
  #println("z: $z")
  #display("x: $x")
  #display("y: $y")
  for iv in 1:size(y)[end]
    r=$FN$XT$YT$ZT(x, y[colons...,iv])
    #println(r)
    z[colons[1:end]...,iv]=r
  end
  #println("Finished")
  return z
end
#Generate , $FN, $XT$YT$ZT, 011, ., $CT

#Fragment sl 1** b bidc bidc ONEEL
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE} # I can't specify the number of elements in X?
  # Boolean[1] compress matrix
  return $FN$XT$YT$ZT(x[1], y)
end
#Generate ,   $FN,  $XT$YT$ZT, 011, ., $YT

# #DEBUG Fragment sl 0** b bidc bidc .
# #DEBUG Fragment sl 0** b bidc bidc ONEEL
# function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
#   #= Boolean scalar compress non-scalar =#
#   sy = size(y)
#   #z = (true == toB(x)) ?  y  : genarray(drop([-1],sy)++[0],$OTFILL);
#   z = (true == toB(x)) ? y : fill($OTFILL,sy[1:end-1]);
#   return z
# end

#Fragment sl 1** id bidc bidc ONEEL
inline $ZTYPE[+] $FNAME($XTYPE[1] x, $YTYPE[+] y)
{ // last-axis non-Boolean[1] compress/replicate matrix
 frameshape = drop([-1],shape(y));
 cellshape =  toi(x[0]) * take([-1],shape(y));
 defcell = genarray(cellshape,$OTFILL);
 z = with {
  (. <= iv <= .) : $FN$XT$YT$ZT( x, y[iv]);
   } : genarray( frameshape, defcell);
 return(z);
}
#Generate ,   TRANSPOSE, X$YT$ZT, X**, ., $YT
#Generate ,   $FN,  $XT$YT$ZT, 111, ., $YT

#Fragment sl 1** bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
  #= last-axis vector compress/replicate matrix =#
  #= This needs conformability check FIXME =#
  #= Also, x may be one-element vector =#

  # frameshape = drop([-1],shape(y));
  # cellshape = sum($XTtoI( x));
  # defcell = genarray([cellshape],$OTFILL);
  # z = with {
  #   (. <= iv <= .) : $FN$XT$YT$ZT( x, y[iv]);
  #   } : genarray( frameshape, defcell);
  # return(z);

  frameshape = size(y)[2:end]
  cellshape = sum(toI(x))
  z = fill($OTFILL,(cellshape...,frameshape...))
  #display(z)
  #display(x)
  #display(y)
  colons=ntuple(d->:,ndims(z)-1)
  for iv in 1:size(y)[end]
    #display(y[colons...,iv])
    r=$FN$XT$YT$ZT(x, y[colons...,iv])
    #println(r)
    
    z[colons...,iv]=r
  end

  return z
end
#Generate ,   $FN,  $XT$YT$ZT, 111, ., $YT

# ------------------ Code fragments for first axis replicate -----------


#Fragment sl1 0** b bidc bidc .
#Fragment sl1 0** b bidc bidc ONEEL
function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
  #= Boolean scalar compress non-scalar, first axis =#
  sy = size(y)
  z = (true == toB(x)) ?  y  : fill($OTFILL,(sy[1:end-1]...,0));
  return z 
end

#Fragment sl1 0** id bidc bidc .
#Fragment sl1 0** id bidc bidc ONEEL
inline $ZTYPE[+] $FNAME($XTYPE x, $YTYPE[+] y)
{ #= Scalar replicate non-scalar, first axis =#
 xi = [toi(x)];
 cellshape = xi++drop([1],shape(y));
 defcell = genarray(cellshape,$OTFILL);
 frameshape = take([1],shape(y));
 z = with {
        (. <= iv <= .)
                : genarray(xi,y[iv]);
        } : genarray(frameshape, defcell);

 zshape = [prod(take([2],shape(z)))]++drop([2],shape(z));
 z = reshape(zshape,z);
 return(z);
}

#Fragment sl1 1** bid bidc bidc ONEEL
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
  # Vector[1] compress/replicate-first-axis matrix
  # FIXME: needs conformability check on x
  #z = (true == toB(x[0])) ? y : genarray([0]++drop([1],shape(y)),$OTFILL);
  #z = (true == toB(x[1])) ? y : fill($OTFILL,size(y)[2:end])
  sy=size(y)
  z= (true == toB(x[1])) ?  y  : fill($OTFILL,(sy[1:end-1]...,0))
  return z
end

#Fragment sl1 1** bid bidc bidc  .
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
  #= Vector compress/replicate-first-axis matrix =#
  #= FIXME: needs conformability and domain checks on x =#
  # Here be dragons
  
  # colons=[ntuple(a -> Colon(), ndims(y)-1)...]
  # zshp=(size(y)[1:end-1]..., sum(x))
  # z=fill($OTFILL, zshp)
  # indx=1

  # for iv in 1:length(x)
  #   r=$FN$XT$YT$ZT(x[iv], y[colons..., iv])
  #   # Ensure r is not empty, i.e. a part of the result.
  #   if size(r)[end] != 0
  #     z[colons..., indx]=r
  #     indx+=1
  #   end
  # end
  z = TRANSPOSE( sl$XT$YT$ZT(x, TRANSPOSE(y)))
  return z
end
#Generate ,   sl,  $XT$YT$ZT, 111, ., $YT
#Generate ,   TRANSPOSE, X$YT$YT, X**, ., $YT


# ------------------------ Expand code fragments ----------------------

#Fragment bsl  001 bid bidc bidc ONEEL
#Fragment bsl1 001 bid bidc bidc ONEEL
#Fragment bsl  001 bid bidc bidc .
#Fragment bsl1 001 bid bidc bidc .
function $FNAME(x::$XTYPE, y::$YTYPE)::Vector{$ZTYPE}
#   Scalar expand scalar (first or last axis) 
#   In practice, one-element left arguments to expand
#    are nearly useless... 
#   
  z = (true == toB(x))  ?  [y]  :  [$OTFILL]
  return z
end

#Fragment bsl  011 bid bidc bidc ONEEL
#Fragment bsl1 011 bid bidc bidc ONEEL
#Fragment bsl  011 bid bidc bidc .
#Fragment bsl1 011 bid bidc bidc .
function $FNAME(x::$XTYPE, y::Vector{$YTYPE})::Vector{$ZTYPE}
#   Scalar expand scalar (first or last axis) 
#   In practice, one-element left arguments to expand
#   are nearly useless... 
#  

  # Is this right? It feels like SxS could be more than just a Boolean, like 5\(,3) hrh 2023-08-04
  z = (true == toB(x))  ?  y  :  [$OTFILL]
  return z
end


#Fragment bsl  101 bid bidc bidc .
#Fragment bsl1 101 bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Vector{$ZTYPE}
#   Vector expand scalar (first or last axis) 
 v = [$OTFILL,y]
 z = fill(false,size(x))
  for iv in 1:length(x)
    z[iv] = toB(x[iv]) ? v[iv] : $OTFILL
  end
#  z = with {
#         (. <= iv <= .)
#                 : v[[toi(x[iv])]];
#         } : genarray( shape(x), false );
 return(z);
end

#Fragment bsl 111 bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Vector{$YTYPE})::Vector{$ZTYPE}
##  Vector-vector expand 
##  Stupid with-loops won't work here. 
##  FIXME: Needs check that (+/x)= shape(y)[0] and x^.GE 0 
##  yi=0;
##  z= genarray(shape(x),$OTFILL);
##  for (xi=0; xi<shape(x)[[0]]; xi++){
##    if (toB(x[[xi]])) {
##         z[[xi]]=y[[yi]];
##         yi++;
##    }
##  }
  yi=1
  z=fill($OTFILL, size(x))
  for xi in 1:length(x)
    if toB(x[xi])
      z[xi]=y[yi]
      yi+=1
    end
  end
 return z
end


#Fragment bsl 1** bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
#=  vector expand matrix last axis =#
#  yt = TRANSPOSE(y);
#  bx = toB(x);
#  bx = (1 == shape(bx)[[0]]) ?   genarray(take([1],shape(yt)), bx[[0]]) :  bx;
#  z = genarray(shape(bx)++drop([1],shape(yt)),$OTFILL);
#  yi = 0;
#  for(i=0; i<shape(bx)[[0]]; i++)
#         if (bx[[i]]){
#                 z[[i]] = yt[[yi]];
#                 yi++;
#         }
#  z = TRANSPOSE(z);
#  return(z);
  yt=TRANSPOSE(y)
  bx=toB(x)
  bx=(1==size(bx)[[1]]) ? fill(true,size(yt)[[1]],bx[[1]]) : bx
  z=fill($OTFILL, (size(yt)[1:end-1]...,size(bx)...))
  yi=1
  colons=[ntuple(a -> Colon(), ndims(yt)-1)...]
  for i in 1:length(bx)
    if bx[i]
      z[colons...,i]=yt[colons...,yi]
      yi+=1
    end
  end
  z=TRANSPOSE(z)
  return z
end
#Generate ,   TRANSPOSE, X$YT$YT, X**, ., $YT

#Fragment bsl 1** bid bidc bidc  .
inline $ZTYPE[+] $FNAME($XTYPE[.] x, $YTYPE[+] y)
{ /* vector expand matrix last axis */
 bx = toB(x);
 bx = (1 == shape(bx)[[0]])  ?   genarray(shape(yt)[[0]], bx[[0]]) : bx;
 z = with {
        (. <= iv <= .)
                : $FNAME(bx, y[iv]));
        } : genarray(drop([-1],shape(y)));
 return(z);
}
#Generate , $FN, $XT$YT$ZT, 111, ., $CT

#Fragment bsl1 1** bid bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
 #= vector expand matrix first axis=# 
#  bx = toB(x);
#  bx = (1 == shape(bx)[[0]]) ?  genarray(take([1],shape(y)), bx[[0]]) : bx;
#  shpz = shape(y);
#  shpz[[0]] = shape(bx)[[0]];
#  z = genarray(shpz,$OTFILL);
#  yi = 0;
#  for(i=0; i<shape(bx)[[0]]; i++)
#         if (bx[[i]]){
#                 z[[i]] = y[[yi]];
#                 yi++;
#         }
#  return(z);
  bx = toB(x)
  bx = (1 == size(bx)[1]) ?  fill(bx[1],size(y)[1]) : bx
  shpz = [size(y)...]
  shpz[end] = size(bx)[1]
  z = fill(0,Tuple(shpz))
  yi = 1
  colons=ntuple(i->Colon(),length(shpz)-1)
  for i in 1:length(bx)
    if bx[i]
      z[colons..., i] = y[colons..., yi]
      yi+=1
    end
  end
  return z
end

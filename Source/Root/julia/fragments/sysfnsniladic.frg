# Niladic system functions: QUADav and QUADts
#
# Robert Bernecky 2007-05-20
#
# --------------- QUADts fragments ----------------------------
#Fragment quadts xx1  x x i .
function $FNAME()::Array{$ZTYPE}
    # QUADts - system time-of-day timestamp 
    # This function provides system time in ISO 8601 format (but array),
    # via now(). See https://docs.julialang.org/en/v1/stdlib/Dates/

    z = Dates.format(now(), "Y m d H M S s") # Create Date
    z = convert(String, z) # Convert from DateTime type to String
    z = split(z, " ") # Convert from single String to Array of substrings
    z = parse.(Int64, z) # Convert Array of Substrings to Array of Ints
    return z
end

# --------------- QUADav fragment ----------------------------
#Fragment quadav xx1  x x c .
function $FNAME()::Array{Char}
 #= QUADav - system character set =#
#  z = with{
#         ([0] <= [i] < [256]) : _toc_S_(i);
#         } : genarray( [256], ' ');
# I am not sure what chars to include
# I would assume that it would be a good idea to include ⎕UCS though, as ⎕AV is not supposed to be used anymore.
# hrh 2023-08-06
    z = Char.(0:255) # I also don't think that this works as it would in APL.
 return(z);
end


# Code fragments for indexed ref
# 2005-10-17
# Rewritten for SAC. rbe
# And again, 2005-10-24 /rbe
# And again, 2006-08-11 /rbe, after discovering how REALLY slow (1000X!) indrfr calls are.
# $XTYPE: The type of lhs, e.g., double_real
# $ZTYPE: The type of result, e.g., int
# $OTFILL: The lhs fill element. Not used by indr; needed for SAC with-loop.

# The idea here is an index reference of the form:
#   X[i;j;k;l;m]        where index arrays (i,j,k,l,m...) may or
#                       not exist (E.g., X[i;k]),
# can be written as compositions of indexing functions:
#
# indrfr(fr,i,X)        Perform XX[i;...], where XX is the set of cells
#                       formed by splitting XX on "Frame Rank" fr.
#                       fr elements are numbered by the rank of the resulting
#                       frame, e.g.,:
#       X[i;j;k;l;m...]
#         0 1 2 3 4...
# Result shape is ((i drop shape(X)),(shape(i)),(i+1)drop shape(x)).
#
# The key is to perform index operations from right to left, so that 
# axes inserted or deleted due to index array shapes. Hence, 
#       z = X[i;;k;;m] 
#             0  2  4  fr values
# can be written as:
#       TMP1 = indrfr(4,m,X);
#       TMP2 = indrfr(2,k,TMP1);
#       z    = indrfr(0,i,TMP2);
# or:   z    = indrfr(0,i,indrfr(2,k,indrfr(4,m,X)));
# with suitable coercions and QUADio subtractions for i,j,m.

# rank indicators for Z<- X[i;j;k] are  XxZijk. 
# Elided axes get x, e.g.:  X[;k2;] is  XxZx2x

#Fragment indr *0*           nonscalarX[scalarI;;] 
#              XIZ after fns2 gets made 
function indr(x::Array{$XTYPE}, i::Int64)::Union{Array{$ZTYPE},$ZTYPE}
    #= X[scalarI;;;] =#
    #= Used only in conjunction with other indexing, e.g.,
    # X[scalarI;;j;]
    =#
    colons=ntuple(x->Colon(),ndims(x)-1)
    z = x[colons...,i]
    return z
end
#Generate , ABC,    III,           *00, ., I 

#Fragment indr *x*           nonscalarX[;;;] 
#              XIZ after fns2 gets made 
function indr(X::Array{$XTYPE})
        # X[;;;]
        # Used only in conjunction with other indexing, e.g.,
        #  * X[;;j;]
        #
        return(X)
end
#Generate , ABC,    III,           *00, ., I 
#Generate , indrfr, $XTI$XT, ****, ., I
#Generate , indrfr, $XTI$XT, *0*0, ., I

#Fragment indr ***           nonscalarX[nonscalarI;;] 
function indr(X::Array{$XTYPE}, I::Array{Int})::Array{$ZTYPE}
 #= X[nonscalarI;;;] =#
#  defcell = genarray(drop([1],shape(X)),$OTFILL);
#  z = with {
#         (. <= iv <= .)
#                 : X[[I[iv]]];
#         } : genarray(shape(I), defcell);
    # defcell = fill($OTFILL, )
    # If the first element in I is 0, add a 1 to the beginning of I. This is a major kludge. 
    
    colons=ntuple(x->Colon(), ndims(X)-1)
    z = X[colons...,I]
    # for i in 1:length(I)
    #     z[colons...,i] = X[colons...,I[i]]
    # end

    # colons=ntuple(x->Colon(), ndims(X)-1)
    # idxed=map(x -> X[colons...,x], I)
    # println("x: ", X)
    # println("I: ", I)
    # z = reshape(vcat(idxed...), (colons..., size(I)...))
    return z
end
#Generate , ABC, III, ***, ., I 
#Generate , ABC, III, *00, ., I 
#Generate , indrfr, $XTI$XT, ****, ., I
#Generate , indrfr, $XTI$XT, *0*0, ., I

#Fragment indrfr *0*0           nonscalarX[;;scalarI;;] 
#                XIZ after fns2 gets made 
function indrfr(fr::Int64, X::Array{$XTYPE}, I::Int64)::Array{$ZTYPE}
  #= X[;;;I;;;], where I has fr (framerank) semicolons to its left =#
  #= This is actually "I from"fr X" =#
    # create an array of colons of size fr
    colonsright = ntuple(x->Colon(), fr)
    colonsleft = ntuple(x->Colon(), ndims(X)-fr-1)

    # use the new index array to index X and drop the leading singleton dimension
    return X[colonsleft...,I,colonsright...] 
end
#Generate , ABC, III, ***, ., I 

#Fragment indrfr ****           nonscalarX[;;nonscalarI;;] 
#                XIZ after fns2 gets made 
function indrfr(fr::Int64, X::Array{$XTYPE}, I::Array{Int64})::Array{$ZTYPE}
  #= X[;;;I;;;], where I has fr (framerank) semicolons to its left =#
  #= This is actually "I from"fr X" =#

    #= X[;;;I;;;], where I has fr (framerank) semicolons to its left =#
  #= This is actually "I from"fr X" =#
    # create an array of colons of size fr
    # create an array of colons of size fr
    colonsright = ntuple(x->Colon(), fr)
    colonsleft = ntuple(x->Colon(), ndims(X)-fr-1)

    # use the new index array to index X and drop the leading singleton dimension
    return X[colonsleft...,I,colonsright...] 
end

function indrfr0(X::Array{$XTYPE}, I::Array{Int64})
  #= X[I;;;] or    I from X ::Array{$ZTYPE} =#
  println(X)
  println(I)
  println("indrfr0")
  exit(418) # I AM A TEAPOT!!!
  
  #=cellshape =  drop([1], shape(X));
  cell = genarray(cellshape, $OTFILL);
 z = with {
        (. <= iv <= .)
                : sel( I[iv], X);
        } : genarray(shape(I), cell);
 return(z);=#
end
#Generate , ABC, III, ***, ., I 
#Generate , ABC, III, *00, ., I 


# Dyadic Scalar function definitions
# R. Bernecky 2006-01-05
#
# Header notes:
#    percentFragment  marks the beginning of each entry
#    Fields are delimited by blanks.
#    Field 1: primitive function jsymbol
#    Field 2: acceptable left argument types
#    Field 3: acceptable right argument types
#    Field 4: Result type for fragment
#    Field 5: ifergit
#
# Header ordering may be critical in the case where two
# fragments would both work. This is important in epio, for
# example, where a special case (e.g., charvec iota charvec)
# must be chosen in preference to the general case.
# Or, here, where we can use a fast Boolean-mod-Boolean, rather
# than general mod code.
#
#Fragment plus 000 bid bid bid .
function $FNAME(x, y)
  # Remove the feed types because they are causing problems. Probably wise to add them back later though.
  # In theory this should always work as long as it isnt fed weird types...
  return x + y
end

#Fragment bar 000 bid bid bid .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  return $XTto$CT(x)-$YTto$CT(y)
end

#Fragment mpy 000 b b b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  return $XTtoB(x) & $YTtoB(y)
end

#Fragment mpy 000 bid bid bid .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  return $XTto$CT(x) * $YTto$CT(y)
end

#Fragment div 000 bid bid d .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  dx = $XTtoD(x)
  dy = $YTtoD(y)
  z = (dx == dy) ? 1.0 : dx/dy
  return z
end

#Fragment min 000 b b b .
function $FNAME(x::$XTYPE, y::$YTYPE)
  # Boolean min
  return x && y
end

#Fragment min 000 idc idc idc .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  #= x min y =#
 return min($XTto$CT(x),$YTto$CT(y))
end

#Fragment max 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return (x&y);
}

#Fragment max 000 bidc bidc idc .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
 #= x max y =#
 return max($XTto$CT(x), $YTto$CT(y))
end

#Fragment mod 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return((!x)&y);
}

#Fragment mod 000 bi bi bi .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  #= SxS residue (aka modulo) =#
  #= (x residue y) <-->   y-x times floor y divide x + 0 = x  =#
  #= See model residueI in workspace UTDScalarI.dws =#
  x = $XTtoI(x)
  y = $YTtoI(y)
  if 0 != x
    q = y / x
    z = y - ( x * q)
  else
    z = y
  end
  if z != 0
    if mpyXII( x) != mpyXII( y)
      z = z + x
    end
  end
 return z
end
#Generate , mpy, X$YT$ZT, X00, ., $CT

#Fragment mod 000 bid bid bid .
function $FNAME(x::$XTYPE, y::$YTYPE, QUADct::Float64)::$ZTYPE
  #= Double residue double =#
  #= This definition is taken from SHARP APL Refman May 1991, p.6-26.
   * It extends the definition of residue to fractional right arguments
   * and to zero, negative and fractional left arguments.
   * r= y-x times floor y divide x+0=x
   * See also APL model in workspace 43 UTDScalarI.
   =#
  nx = (0.0 == $XTto$CT(x))  ?  1.0  : $XTto$CT(x)
  z = $YTto$CT(y) - $XTto$CT(x) * toD(DFLOOR($YTto$CT(y)/nx, QUADct))
  return z
end
#Generate , DFLOOR, X$YT$ZT, X00, ., $CT

#Fragment star 000 b b b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(x | !y);
}

#Fragment star 000 bid b bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{
 z =  ($YTtoB(y))  ?  $XTto$ZT(x)  : Ito$ZT(1);
 return(z);
}

#Fragment star 000 bid i bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{  #= number to integer power =#
  z = Ito$CT( 1);
  for( i=0; i<$YTtoI( y); i++) {
    z = z * $XTto$CT(x);
  }
  return( z);
}

#Fragment star 000 bid d d .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(pow($XTtoD(x),$YTtoD(y)));
}

#Fragment log  000 bid bid d .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ return(log($YTtoD(y))/log($XTtoD(x)));
}

#Fragment lt 000 b b b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  # Boolean less than
  return (!x)&y
end

#= NB.  APEX Extension of ISO APL to allow comparison of characters =#
#= relationals =#
#Fragment lt 000 bic bic b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  #= A<B on Boot/Int/Char =#
  return $XTto$CT(x)<$YTto$CT(y)
end

#Fragment lt 000 bi bi b i
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ #= Boolean/Integer less than =#
 return($XTto$CT(x)<=$YTto$CT(y));

#Fragment lt 000 bid bid b d
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ #= Fuzzy less than =#
 return(($XTto$CT(x)<$YTto$CT(y)) & !APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
#Generate , APEXFUZZEQ, $XT$YTB, 000, ., D

#Fragment le 000 b b b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
 #= A<=B on Boolean =#
 return (!x)|y
end

#Fragment le 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ #= A<=B on Boolean,int, char =#
 return($XTto$CT(x) <= $YTto$CT(y));
}

#Fragment le 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ #= Fuzzy <= =#
 return(($XTto$CT(x)<$YTto$CT(y)) | APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
#Generate , APEXFUZZEQ, DDB, 000, ., D

#Fragment eq 000 bic bic b .
@inline function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
    #= A=B on non-doubles =#
    return x == y
end

#Fragment eq 000 bid bid b .
function $FNAME(x::$XTYPE, y::$YTYPE, QUADct::Float64)::$ZTYPE
  #= A=B on doubles =#
  #= We use | instead of || on the assumption that
     the zero-fuzz case will eliminate the second leg,
     and it also eliminate a CONDFUN.
  =#
 return(($XTto$CT(x) == $YTto$CT(y)) | APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
end
#Generate , APEXFUZZEQ, DDB, 000, ., D

#Fragment ne 000 bic bic b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  #= A !=B on non-doubles =#
  return $XTto$CT(x) != $YTto$CT(y)
end

#Fragment ne 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ #= A!=B on doubles =#
 return(($XTto$CT(x) != $YTto$CT(y)) & !APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
#Generate , APEXFUZZEQ, DDB, 000, ., D

#Fragment gt 000 b b b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
 #= A>B on non-doubles =#
 return x & !y
end

#Fragment gt 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ #= A>B on Booleans =#
 return($XTto$CT(x) > $YTto$CT(y));
}

#Fragment gt 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ #= A>B on doubles =#
 return(($XTto$CT(x) > $YTto$CT(y)) & !APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
#Generate , APEXFUZZEQ, DDB, 000, ., D

#Fragment ge 000 b b b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
 #= A>=B on Booleans =#
 return x | !y
end

#Fragment ge 000 bic bic b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ #= A>=B on non-Doubles =#
 return($XTto$CT(x) >= $YTto$CT(y));
}

#Fragment ge 000 bid bid b .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y, double QUADct)
{ #= A>=B on doubles =#
 return(($XTto$CT(x) >= $YTto$CT(y)) | APEXFUZZEQ($XTto$CT(x),$YTto$CT(y),QUADct));
}
#Generate , APEXFUZZEQ, DDB, 000, ., D

#Fragment and 000 bid bid b .
function $FNAME(x::$XTYPE, y::$YTYPE)
  # and on bid
  return $XTtoB(x) && $YTtoB(y)
end

#Fragment or 000 bid bid b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  return $XTtoB(x) || $YTtoB(y)
end

#Fragment nand 000 bid bid b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  # nand on bid
  return !($XTtoB(x) && $YTtoB(y))
end

#Fragment nor 000 bid bid b .
function $FNAME(x::$XTYPE, y::$YTYPE)::$ZTYPE
  # nor on bid
  return(!($XTto$CT(x)|$YTto$CT(y)));
end

#Fragment circ 000 bid bid bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ #= Circle functions =#
 xi = $XTtoI(x);
 if      (1 == xi)
        z = sin($YTtoD(y));
 else if (2 == xi)
        z = cos($YTtoD(y));
 else if (3 == xi)
        z = tan($YTtoD(y));
 else if (4 == xi)
        z = pow(1.0+($YTtoD(y)*$YTtoD(y)),0.5);
 else
        z = 42.0; #= Should be domain error or something =#
 return(z);
}


#= Catenate code fragments=#
#= Robert Bernecky 2007-05-14=#
#= SAC code generator=#
#==#
#= Fragment header:  jsymbol ranks(x,y,z) nil nil nil comments=#
#= * in rank specifier means any rank>1=#


#Fragment coma 001 bidc bidc bidc .
#Fragment comb 001 bidc bidc bidc .
function $FNAME(x::$XTYPE, y::$YTYPE)::Array{$ZTYPE}
  #= SxS catenate first (or last) axis =#
  return(vcat(x, y))
end

#Fragment coma 011 bidc bidc bidc .
#Fragment comb 011 bidc bidc bidc .
function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
  #= SxV catenate first (or last) axis =#
  return(vcat(x, y))
end

#Fragment coma 101 bidc bidc bidc .
#Fragment comb 101 bidc bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Vector{$ZTYPE}
  #= VxS catenate first (or last) axis =#
  return(reverse(vcat(y,reverse(x))))
end

#Fragment coma 111 bidc bidc bidc .
#Fragment comb 111 bidc bidc bidc .
function $FNAME(x::Vector{$XTYPE}, y::Vector{$YTYPE})::Array{$ZTYPE}
  #= VxV catenate first or last axis =#
  return(vcat(x, y))
end

#Fragment comb 1** bidc bidc bidc .
function $FNAME(x::Array{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
  #= VxA first axis catenate =#
  return(vcat(x, y))
end

#Fragment comb *1* bidc bidc bidc .
function $FNAME(x::Array{$XTYPE}, y::Vector{$YTYPE})::Array{$ZTYPE}
  #= AxV first axis catenate =#
  return(vcat(x, y))
end

#Fragment coma 0** bidc bidc bidc .
function $FNAME(x::$XTYPE, y::Array{$YTYPE})::Array{$ZTYPE}
 #= S,A last (first in julia) axis catenate =#
  frame=size(y)[2:end] # Instead of getting all but the last, get all but the first
  z = fill($OTFILL, (size(y)[1]+1,frame...))
  colons=ntuple(_->Colon(), ndims(y)-1)
  for i in 1:size(y)[end]
    z[colons..., i] = vcat(x,y[colons..., i])
  end
 return z
end

#Fragment coma *0* bidc bidc bidc .
function $FNAME(x::Array{$XTYPE}, y::$YTYPE)::Array{$ZTYPE}
 #=/* A,S last-axis catenate */=#
 frame=size(x)[2:end] # Instead of getting all but the last, get all but the first
 z = fill($OTFILL, (size(x)[1]+1,frame...))
 colons=ntuple(_->Colon(), ndims(x)-1)
 for i in 1:size(x)[end]
    cell=x[colons..., i]
    ycell=fill(y, (1,size(cell)[2:end]...)...) # I am convinced there is a better way to do this...
    z[colons..., i] = vcat(cell,ycell)
  end
  return z
end


#Fragment coma 1** bidc bidc bidc .
inline $ZTYPE[+] $FNAME($XTYPE[.] x, $YTYPE[+] y)
{ #= VxA last-axis catenate =#
 z=TRANSPOSE(to$CT([x])++TRANSPOSE(to$CT(y)));
 return(z);
} 
#Generate ,   TRANSPOSE, X$YT$YT, X**, ., $YT

#Fragment coma *1* bidc bidc bidc .
inline $ZTYPE[+] $FNAME($XTYPE[+] x, $YTYPE[.] y)
{ #= AxV last-axis catenate =#
 z=TRANSPOSE(TRANSPOSE(to$CT(x))++to$CT([y]));
 return(z);
} 
#Generate ,   TRANSPOSE, X$YT$YT, X**, ., $YT

#Fragment coma *** bidc bidc bidc LG
function $FN$XT$YT$ZTLG(x::Array{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
  #= AxA last axis catenate. Left rank greater =#
  yDims=size(y)
  chDims=pushfirst!([yDims...],1)
  chY=reshape(y, chDims...)
  return vcat(x, chY)
end

#Fragment coma *** bidc bidc bidc RG
inline $ZTYPE[+] $FN$XT$YT$ZTRG($XTYPE[+] x, $YTYPE[+] y)
{#= AxA last axis catenate. Right rank greater =#
 frameshape = drop([-1],shape(y));
 cellshape  = take([-1],shape(y)) + [1];
 cell = genarray(cellshape, $OTFILL);
 z = with { 
        (. <= iv <= .) 
                : to$CT(x[iv])++to$CT([y[iv]]);
        } : genarray(frameshape, cell);
 return(z);
}

#Fragment coma *** bidc bidc bidc .
function $FNAME(x::Array{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
  #= AxA last axis catenate. Ranks match =#
  z = vcat(x, y)
  return z
end

######################### first axis catenate #################

#Fragment comb 0** bidc bidc bidc .
function $FNAME(x::$XTYPE, y::Array{$YTYPE})
  #= SxA first axis catenateee =#
  cell = Array{$XTYPE}(fill(x,size(y)[1:end-1]))

  return cat(cell,y,dims=length(size(y)))
end

#Fragment comb *0* bidc bidc bidc .
function $FNAME(x::Array{$XTYPE}, y::$YTYPE)
  #= AxS first-axis catenate=#
   cell = Array{typeof(to$CT(y))}(fill(y,size(x)[1:end-1]))

   return(cat(x,cell,dims=length(size(x))))
end

#Fragment comb *** bidc bidc bidc LG
function $FN$XT$YT$ZTLG(x::Array{$XTYPE}, y::Array{$YTYPE})
  #= AxA first axis catenate. Left rank greater =#
  return cat(x,y,dims=length(size(x)))
end
#Fragment comb *** bidc bidc bidc RG
function $FN$XT$YT$ZTRG(x::Array{$XTYPE}, y::Array{$YTYPE})
  #= AxA first axis catenate. Right rank greater =#
  return cat(x,y,dims=length(size(y)))
end
#Fragment comb *** bidc bidc bidc .
function $FNAME(x::Array{$XTYPE}, y::Array{$YTYPE})
    #= AxA first axis catenate. Ranks match =#
    #= concatenates along the first axis. =#
    return cat(map(to$CT, x), map(to$CT, y), dims=length(size(x)))
end

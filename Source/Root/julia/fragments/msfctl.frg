# APEX code fragments for monadic scalar functions.
# R. Bernecky 2006-01-05
# H. Hoover 2023-08-04 yyyy-mm-dd

#Fragment msfctl x** x bidc bidc .
function $FNAME(y::Array{$YTYPE}$SYSVARGDECL)::Array{$ZTYPE}#$SYSVARGOUT I don't know what $\SYSVARGOUT does
  #= Monadic scalar functions on array =#
  # z = with { 
  #       ( . <= iv <= .)
  #               : $FNX$CT$ZT(to$CT(y[iv])$SYSVARGKER);
  #       } : genarray(shape(y), $OTFILL);
  z = fill($OTFILL, size(y))
  for i in 1:length(y)
    z[i] = $FNX$CT$ZT(to$CT(y[i])$SYSVARGKER)
  end
  return z
end
#Generate , $FN, $XT$CT$ZT, x00, ., $CT

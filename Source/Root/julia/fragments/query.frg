#  Monadic query (roll) and Dyadic query (deal)
#
# Robert Bernecky 2006-07-27
# SAC code generator
#

#Fragment quer X00 x bid i .
inline $ZTYPE, int $FNAME($YTYPE y, int QUADio, int QUADrl)
{ #= Monadic query (roll) -  scalar =#
 inty = toi(y);
 if (inty <= 0) print(tochar("roll domain error"));
 QUADrl = Lehmer(QUADrl);
 z = (tod(QUADrl) * tod(inty)) / tod(2147483647);
 return(toi(z) + QUADio, QUADrl);
}
#Generate , Lehmer, $XT$YT$ZT, X00, ., I


#Fragment quer X** x bid i .
function $FNAME(y::Array{$YTYPE}, QUADio::Int64, QUADrl::Union{Vector{Int64},Int64})::Tuple{Array{$ZTYPE}, Vector{Int64}}
 #= Monadic query (roll) - non=scalar =#
  if (QUADrl isa Int64)
    error("Unsupported random number generator (Use ⎕RL←X 0 as a workaround)")
    QUADrl = [QUADrl]
  end
  rltype=QUADrl[2]
  if (rltype != 0)
    error("Unsupported random number generator (Use ⎕RL←X 0 as a workaround)")
  end

 zxrho = prod(size(y))
 ravely = reshape(y,zxrho)
 inty = toI(y)
 #if (any(inty <= 0)) println("roll domain error")
 # if any element of y is <= 0, print error message julia
 if 0 in inty error("roll domain error") end
 #= The following presumes ordering, so we use FOR loop. Sorry =#
 #z = genarray([zxrho], -1)
#  for (i=0; i<zxrho; i++) {
#         QUADrl = Lehmer(QUADrl);
#         val = (tod(QUADrl) * tod(ravely[[i]])) / tod(2147483647);
#         z[[i]] =  QUADio + toi(val);
#  }
# Instead of for loop, use map function
  #z = map(x -> (QUADrl = Lehmer(QUADrl); val = (QUADrl * x) / 2147483647; QUADio + toI(floor(val))), ravely)
  Q=copy(QUADrl)[1]
  z = fill(0, zxrho)
  # for i in 1:zxrho
  #   Q = Lehmer(Q)
  #   val = (Q * ravely[i]) / 2147483647
  #   z[i] = QUADio + toI(floor(val))
  # end
  @inbounds @fastmath for i in eachindex(ravely) # Not multithreaded, but okay in speed. Maybe can be improved...
    #Q = Lehmer(Q)
    val = Q*16807;
    Q=(val)%2147483647
    val = (Q * ravely[i]) / 2147483647
    z[i] = QUADio + toI(floor(val))
  end

  z = reshape(z, size(y));
  return z, [Q,0]
end
#Generate , Lehmer, $XT$YT$ZT, X00, ., I


#Fragment quer 001  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE x, $YTYPE y, int QUADio, int QUADrl)
{ #= Dyadic query (deal) - scalar =#
  #= This allocates an array of shape y. If this
   * causes you memory grief, use the algorithm in workspace 42 query,
   * or use smaller y!
   =#
 intx = toi(x);
 inty = toi(y);
 shpz = toi(x);
 z = iota (inty);

 for( i=0; i<intx; i++) {
        #= Interchange z[i] with z[i + ? y - i] =#
        rand, QUADrl = querXII(inty -i, 0, QUADrl);
        c = i + rand;
        if ( c < shpz) {
                tmp    = z[[c]];
                z[[c]] = z[[i]]; 
                z[[i]] = tmp;
        }
 }      
 z = take([intx], z);
 return(z + QUADio, QUADrl);
}
#Generate , quer, X$YT$ZT, X00, ., I 

#Fragment quer 111  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE[.] x, $YTYPE[.] y, int QUADio, int QUADrl)
{ #= Dyadic query (deal) - non-scalar =#
 #= need length error check FIXME =#
 return(querIII(x[0],y[0],QUADio,QUADrl));
}
#Generate , quer, III, 000, ., I 

#Fragment quer 011  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE x, $YTYPE[.] y, int QUADio, int QUADrl)
{ #= Dyadic query (deal) - non-scalar =#
 #= need length error check FIXME =#
 return(querIII(x,y[0],QUADio,QUADrl));
}
#Generate , quer, III, 000, ., I 

#Fragment quer 101  bid bid i .
inline $ZTYPE[.], int $FNAME($XTYPE[.] x, $YTYPE y, int QUADio, int QUADrl)
{ #= Dyadic query (deal) - non-scalar =#
 #= need length error check FIXME =#
 return(querIII(x[0],y,QUADio,QUADrl));
}
#Generate , quer, III, 000, ., I 

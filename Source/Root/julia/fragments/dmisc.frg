# Dyadic Miscellaneous function code fragments
# Handles matrix divide, deal, base value, 
# representation, dyadic thorn, match
#
# Robert Bernecky 2006-02-10
# SAC code generator
#
# --------------- Base value fragments ----------------------------
#Fragment dtak 000  bid bid bid .
# There has to be a
inline $ZTYPE $FNAME($XTYPE x, $YTYPE y)
{ #= Scalar basevalue Scalar =#
 return(y);
}

#Fragment dtak 010 bid bid bid .
inline $ZTYPE $FNAME($XTYPE x, $YTYPE[.] y)
{ #= Scalar basevalue vector =#
 ycols=shape(y);
 weights = genarray(ycols, to$CT(1));
 for (i=ycols[[0]]-2; i>=0; i--)
        weights[[i]] = weights[[i+1]]*to$CT(x);
 #= Now, we just do weights +.* y =#
 z = with {
        ([0] <= iv < ycols)
                : weights[iv] * to$CT(y[iv]);
        } : fold(+, $OTFILL);
 return(z);
}

#Fragment dtak 0** bid bid bid .
inline $ZTYPE[*] $FNAME($XTYPE x, $YTYPE[+] y)
{ #= Scalar basevalue rank>1 =#
 yt = TRANSPOSE(y); #= Dumb, but easy =#
 frameshape = drop([-1],shape(yt));
 z = with { 
        (. <= iv <= .)
                : $FNAME(x, yt[iv]);
        } : genarray(frameshape, $OTFILL);
 return(TRANSPOSE(z));
}
#Generate , $FN, $XT$YT$ZT, 010, ., $CT
#Generate ,   TRANSPOSE, X$YT$YT, X**, ., $YT

#Fragment dtak 110 bid bid bid .
function $FNAME(x::Vector{$XTYPE}, y::Vector{$YTYPE})::$ZTYPE
 #= Vector basevalue vector =#
  #= 3 cases - all give 22200:
   *    10 10 10 basevalue 200 200 200 
   *    10 10 10 basevalue 200
   *    (,10) basevalue 200 200 200 
   =#
#  ycols = (shape(y))[[0]];
#  if (1 == ycols){       #= Maybe extend y =#
#         ycols = shape(x)[[0]];
#         y = genarray([ycols],y[[0]]);
#  }
#  if (1 == shape(x)[[0]]){       #= Maybe extend x =#
#         x = genarray([ycols], x[[0]]);
#  }
#  weights = genarray([ycols], to$CT(1));
#  for (i=ycols-2; i>=0; i--)
#         weights[[i]]= weights[[i+1]]*to$CT(x[[i+1]]);
#  z = with { 
#         ([0] <= iv < [ycols])
#                 : weights[iv] * to$CT(y[iv]);
#         } : fold(+, $OTFILL);
#  return(z);
    ycols=size(y)[end]
    if 1==ycols
        ycols=size(x)[end]
        y=fill(y[1], ycols)
    end
    if 1==size(x)[end]
        x=fill(x[1], ycols)
    end
    weights = fill(to$CT(1), ycols)
    for i=ycols-1:-1:1
        weights[i] = weights[i+1]*to$CT(x[i+1])
    end
    z = to$CT(0)
    for i=1:ycols
        z += weights[i]*to$CT(y[i])
    end
    return z
end

#Fragment dtak 1** bid bid bid .
inline $ZTYPE[*] $FNAME($XTYPE[.] x, $YTYPE[+] y)
{ #= Vector basevalue rank>1 =#
 yt = TRANSPOSE(y); #= Dumb, but easy =#
 frameshape = drop([-1],shape(yt));
 z = with { 
        (. <= iv <= .)
                : $FNAME(x, yt[iv]);
        } : genarray(frameshape, $OTFILL); 
 return(TRANSPOSE(z));
}
#Generate , $FN, $XT$YT$ZT, 110, ., $CT
#Generate ,   TRANSPOSE, X$YT$YT, X**, ., $YT

# --------------- Represent fragments ----------------------------
# These cases of Boolean result are detected by dfa; code
# generator picks then up by result type(ctl FindType fns).

#Fragment utak 101 bid bid b .
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Vector{Bool}
    #= Vector-of-twos represent scalar =#
    #=   
    # This could be any mix of powers-of-two with a bit of work.
    # The guts of represent on Booleans
    =#
#    cell = 0;
#    k = shape(x)[[0]]-1;
#    z = with { 
#         (. <= iv <= .)
#                 : BitAND(1,BitShiftRight(k-iv[0],toi(y)));
#         } : genarray(shape(x), cell);
    k = size(x)[1]
    z = fill(false, k)
    for i=1:k
        z[i] = (y & (1<<(k-i))) != 0
    end
    return z
end

#Fragment utak 1**  bid bid b .
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{Bool}
    #= Vector-of-twos represent non-scalar =#
    # z=fill(false,(size(y)..., length(x)))
    # colons=ntuple(i->(:),ndims(y))
    # for i in 1:size(y)[1]
    #     r=$FNAME(x,y[colons[1:end-1]...,i])
    #     z[i,colons...]=r
    # end
    # return z
    dims = size(y)
    result_dim = (dims..., length(x))
    z = Array{$YTYPE}(undef, result_dim...)
    colons=ntuple(i->(:),ndims(y))
    
    function process_dim(dim_vector, dims_left)
        if length(dims_left) == 1
            for i=1:dims_left[1]
                z[dim_vector..., i, colons...] = $FNAME(x, y[dim_vector..., i])
            end
        else
            for i = 1:dims_left[1]
                process_dim((dim_vector..., i), dims_left[2:end])
            end
        end
    end
    
    process_dim((), dims)
    
    return z
end
#Generate , $FN, $CT$CT$ZT, 101, ., $CT


#Fragment utak 101 bi bi bi .
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Vector{$ZTYPE}
  #= Non-floating Vector represent scalar =#
  #= Taken from ISO Extended APL standard Draft N93.03, page 155 =#
#   wts = genarray(shape(x),to$CT(1));
#   for(i=shape(x)[[0]]-2; i>=0; i--)
#         wts[[i]] = wts[[i+1]] * to$CT(x[[i+1]]);
#   z = genarray(shape(x),$OTFILL);
#   cy = to$CT(y);
#   for(i=shape(x)[[0]]-1; i>=0; i--){
#         z[[i]] = mod$CT$CT$ZT(to$CT(x[[i]]),cy/wts[[i]]);
#         #= Must use fuzz-less call to mod!! =#
#         #= Represent is NOT fuzzy (SAPL Ref Man p.6-47, 1991 =#
#         cy = cy - z[[i]] * wts[[i]];
#   }
#  return(z);
    # wts = fill(to$CT(1),length(x))
    # for i=length(x)-2:-1:1
    #     wts[i] = wts[i+1] * to$CT(x[i+1])
    # end
    # z = fill($OTFILL,length(x))
    # cy = to$CT(y)
    # for i=length(x)-1:-1:1
    #     z[i] = mod$CT$CT$ZT(to$CT(x[i]),Int64(cy/wts[i]))
    #     cy = cy - z[i] * wts[i]
    # end
    # return z
    # Radix vector will be reversed for processing
    radix = reverse(x)
    output = zeros(Int64, length(radix))
    neg = y < 0
    y = abs(y)

    for i in 1:lastindex(radix)
        if radix[i] == 0
            output[i] = y
        else
            y, output[i] = divrem(y, radix[i])
        end
        if neg
           y += 1
           output[i] = radix[i] - output[i]
        end
    end

    return reverse(output)
end


#Fragment utak 101 bid bid bid .
function $FNAME(x::Vector{$XTYPE}, y::$YTYPE)::Vector{$ZTYPE}
  #= Floating Vector represent scalar =#
  #= Taken from ISO Extended APL standard Draft N93.03, page 155 =#
#   wts = genarray(shape(x),to$CT(1));
#   for(i=shape(x)[[0]]-2; i>=0; i--)
#         wts[[i]] = wts[[i+1]] * to$CT(x[[i+1]]);
#   z = genarray(shape(x),$OTFILL);
#   cy = to$CT(y);
#   for(i=shape(x)[[0]]-1; i>=0; i--){
#         z[[i]] = mod$CT$CT$ZT(to$CT(x[[i]]),cy/wts[[i]],0.0);
#         #= Must use zero-fuzz call to mod!! =#
#         #= Represent is NOT fuzzy (SAPL Ref Man p.6-47, 1991 =#
#         cy = cy - z[[i]] * wts[[i]];
#   }
#  return(z);
    # wts=fill(to$CT(1),length(x))
    # for i=length(x)-2:-1:1
    #     wts[i] = wts[i+1] * to$CT(x[i+1])
    # end
    # z = fill($OTFILL,length(x))
    # cy = to$CT(y)
    # for i=length(x)-1:-1:1
    #     z[i] = mod$CT$CT$ZT(to$CT(x[i]),cy/wts[i],0.0)
    #     cy = cy - z[i] * wts[i]
    # end
    # println("vxs")
    # return z
    # Radix vector will be reversed for processing
    radix = reverse(x)
    output = zeros(Float64, length(radix))
    neg = y < 0
    y = abs(y)

    for i in 1:lastindex(radix)
        if radix[i] == 0
            output[i] = y
        else
            y, output[i] = divrem(y, radix[i])
        end
        if neg
           y += 1
           output[i] = radix[i] - output[i]
        end
    end

    return reverse(output)
end

#Fragment utak 1** bid  bid bid .
function $FNAME(x::Vector{$XTYPE}, y::Array{$YTYPE})::Array{$ZTYPE}
    #= Vector represent non-scalar =#
    # z=fill($OTFILL,(size(y)..., length(x)))
    # colons=ntuple(i->(:),ndims(y))
    # for i in 1:size(y)[1]
    #     r=utakIII(x,y[colons[1:end-1]...,i])
    #     z[i,colons...]=r
    # end
    dims = size(y)
    result_dim = (dims..., length(x))
    z = Array{Int64}(undef, result_dim...)
    colons=ntuple(i->(:),ndims(y))
    
    function process_dim(dim_vector, dims_left)
        if length(dims_left) == 1
            for i=1:dims_left[1]
                z[dim_vector..., i, colons...] = $FNAME(x, y[dim_vector..., i])
            end
        else
            for i = 1:dims_left[1]
                process_dim((dim_vector..., i), dims_left[2:end])
            end
        end
    end
    
    process_dim((), dims)
    
    return z
end
#Generate , $FN, $XT$YT$ZT, 101, ., $CT

# --------------- MATCH fragments ----------------------------

#Fragment same 000 bidc bidc b .
function $FNAME(x::$XTYPE, y::$YTYPE$SYSVARGDECL)::$ZTYPE
 #= Scalar match scalar =#
 return x == y
end

#Fragment same 0*0  bidc bidc b .
function $FNAME(x::$XTYPE, y::AbstractArray{$YTYPE}$SYSVARGDECL)::$ZTYPE
  #= Scalar match non-scalar =#
  return false
end

#Fragment same *00  bidc bidc b .
function $FNAME(x::AbstractArray{$XTYPE}, y::$YTYPE$SYSVARGDECL)::$ZTYPE
 #= Non-scalar match scalar =#
 return false
end

#Fragment same **0 bidc bidc b .
function $FNAME(x::AbstractArray{$XTYPE}, y::AbstractArray{$YTYPE}$SYSVARGDECL)::$ZTYPE
    #= Non-scalar match non-scalar =#
    # If Array{Double}, then use isapprox match, and QUADct will be used as comparison tolerance.
    if $XTYPE == Float64
        return isapprox(x,y,atol=QUADct)
    end
    return x==y
end


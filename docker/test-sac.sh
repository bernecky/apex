#!/bin/bash
# Set the path to the testsuites.json
JSON_FILE="Ancillary/benchmks/testsuites.json"
# Read the directory names from the JSON array using jq
DIRS=$(jq -r '.[][]' $JSON_FILE)

# Loop through each directory
for DIR in $DIRS; do
    echo "Processing $DIR..."
    # Navigate to the project directory
    cd "Ancillary/benchmks/$DIR"
    # Create and navigate to the build directory
    mkdir -p build && cd build
    # Run cmake excluding some directories
    cmake -DIGNORE_DIRS_LIST="UTTakeDrop" ..
    # Run make with 8 parallel jobs and continue on error
    make -j 8 -k
    # Run tests with ctest with 8 parallel jobs
    ctest -j 8
    # Return to the root directory or continue to the next DIR
    cd ../../../..
done
echo "All projects processed."

﻿ Z←⍙TCLF
 ⍝Returns a linefeed character (which may be the same as ⍙TCNL on some APLs)
 Z←⎕TC[⎕IO+2]
 ⍝ APLASCII.APLPLUS3 version 1.4 (1995.10.15)

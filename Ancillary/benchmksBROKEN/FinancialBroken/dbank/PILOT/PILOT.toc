\contentsline {section}{\numberline {0.1}Executive Summary}{1}
\contentsline {section}{\numberline {0.2}Experimental Framework}{2}
\contentsline {section}{\numberline {0.3}Benchmarks}{2}
\contentsline {section}{\numberline {0.4}Performance}{3}
\contentsline {subsection}{\numberline {0.4.1}{\bf bmzd} and {\bf bmzdt}}{4}
\contentsline {subsection}{\numberline {0.4.2}{\bf dbt1} and {\bf dbt1t}}{7}
\contentsline {subsection}{\numberline {0.4.3}{\bf dbt2}}{7}
\contentsline {subsection}{\numberline {0.4.4}{\bf dbt5}}{8}
\contentsline {subsection}{\numberline {0.4.5}{\bf indfdivm}}{8}
\contentsline {subsection}{\numberline {0.4.6}{\bf round} and {\bf roundt}}{9}
\contentsline {section}{\numberline {0.5}Conclusions}{9}
\contentsline {section}{\numberline {0.6}Recommendations}{10}

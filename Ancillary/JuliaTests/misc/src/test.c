#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdint.h> //for int8_t
#include <unistd.h> //for close()

// Function to load the file into memory
int8_t* load_file_into_memory(char * filename, size_t* size) {
    int fd;
    struct stat sb;
    int8_t * addr = NULL;

    // Open the file
    fd = open(filename, O_RDONLY);
    if (fd == -1) {
        perror("Error opening file for reading");
        exit(EXIT_FAILURE);
    }

    // Get file size
    if (fstat(fd, &sb) == -1) {
        perror("fstat error");
        exit(EXIT_FAILURE);
    }
    *size = sb.st_size;
    printf("%d", *size);

    // Memory map the file
    addr = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (addr == MAP_FAILED) {
        perror("mmap failed");
        exit(EXIT_FAILURE);
    }

    // Close the file descriptor; the mapping is not affected by this
    if (close(fd) == -1) {
        perror("close error");
        exit(EXIT_FAILURE);
    }

    return addr;
}

int main(int argc, char* argv[]) {
    if(argc != 2) {
        fprintf(stderr, "Usage: %s <file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    size_t size;
    int8_t * data = load_file_into_memory(argv[1], &size);

    // data now contains the binary data from the file.
    // It can be accessed/processed as a normal array data[0], data[1], etc.
    // calculate how many elements (8 bits signed integers) in the file.
    size_t count = size / sizeof(int8_t);

    for (size_t i = 0; i < count; i++) {
        printf("Element %zu: %d\n", i, data[i]);
    }

    // free it when finished
    if (munmap(data, size) == -1) {
        perror("munmap error");
        exit(EXIT_FAILURE);
    }

    return 0;
}

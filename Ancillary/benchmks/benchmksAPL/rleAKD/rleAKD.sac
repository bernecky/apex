use Array: all;
use StdIO : all;
use Numerical : all;
use CommandLine: all;
use String: {to_string,tochar,sscanf};
use ArrayFormat: all;
use Bits: all;

/* Compiled by APEX Version: /home/apex/apex3/wss/sac3013.dws2012-11-07 10:21:33.907 */
/*
% This is the APEX stdlib.sis include file.
% Standard equates and constants for APL compiler
% Also standard coercion functions
*/

#define toB(x) to_bool((x))
#define toI(x) toi((x))
#define toD(x) tod((x))
#define toC(x) (x)
#define toc(x) ((x))

#define BtoB(x) ((x))
#define ItoI(x) ((x))
#define DtoD(x) ((x))
#define CtoC(x) ((x))

#define BtoI(x) toi((x))
#define BtoD(x) tod((x))
#define ItoB(x) to_bool((x))
#define ItoD(x) tod((x))
#define DtoB(x) to_bool((x))
#define DtoI(x) toi((x))


inline int minXDI(double y,double QUADct)
{ return(DFLOOR(y,QUADct));
}



inline int[+] modXII(int[+] y)
{ /* Monadic scalar functions on array */
  z = with {
        ( . <= iv <= .)
                : modXII(toI(y[iv]));
        } : genarray(shape(y), 0);
  return(z);
}

inline int maxBII(bool x, int y)
{ /* x max y */
 return (max(BtoI(x),ItoI(y)));
}

inline double logIID(int x, int y)
{ return(log(ItoD(y))/log(ItoD(x)));
}

inline int plusBII(bool x, int y)
{ return(BtoI(x)+ItoI(y));
}

inline double barDDD(double x, double y)
{ return(DtoD(x)-DtoD(y));
}

inline bool eqDDB(double x, double y, double QUADct)
{ /* A=B on doubles */
 return((DtoD(x) == DtoD(y)) || APEXFUZZEQ(DtoD(x),DtoD(y),QUADct));
}


inline int barBBI(bool x, bool y)
{ return(BtoI(x)-BtoI(y));
}

inline bool[+] ltIBB(int[+] x, bool y)
{ /* AxS scalar function */
  z = with {
  ( . <= iv <= .) {
              xel = x[iv];
   } : ltIBB(xel,y);
  } : genarray( shape(x), false);
  return(z);
}


inline int[+] plusBII(bool x, int[+] y)
{ /* SxA scalar function */
  xel = toI(x);
  z = with {
     ( . <= iv <= .) {
              yel = toI(y[iv]);
                    } : plusIII(xel,yel);
  } : genarray(shape(y), 0);
  return(z);
}


inline double[+] plusDID(double x, int[+] y)
{ /* SxA scalar function */
  xel = toD(x);
  z = with {
     ( . <= iv <= .) {
              yel = toD(y[iv]);
                    } : plusDDD(xel,yel);
  } : genarray(shape(y), 0.0d);
  return(z);
}


inline bool[.] comaXBB(bool[+] y)
{ /* Ravel of anything with rank>1 */
  z = reshape([prod(shape(y))],y);
  return(z);
}

inline int[.] rhoIII(int x, int[*] y)
{ /* Scalar reshape non-scalar (to vector) */
 z = rhoIII( [toi(x)],y);
 return(z);
}


inline bool[.] takeIBB(int x, bool[.] y)
{ /* Scalar take vector */
  return(take([toi(x)], y));
}

inline int[.] iotaXBI(bool y, int QUADio)
{ /* Index generator on scalar */
/* HELP! Needs domain check for negative shp */
  z = QUADio+iota(toi(y));
  return( z);
}

inline int[.]  rhoXBI(bool[+] y)
{ /* Shape of non-scalar */
 return(shape(y));
}

inline int[.] iotaXII(int[1] y, int QUADio)
{ /* Index generator on 1-element vector */
 /* HELP! Needs length error check */
/* HELP! Needs domain check for negative shp */
  z = QUADio+iota(toi(y[[0]]));
  return( z);
}

inline int[.] iotaXII(int y, int QUADio)
{ /* Index generator on scalar */
/* HELP! Needs domain check for negative shp */
  z = QUADio+iota(toi(y));
  return( z);
}

inline double[*] quadXDD(double[*] y, int QUADpp, int QUADpw)
{ /* {quad}{<-} anything */
        show(y);
        return(y);
}
inline int[*] quadXII(int[*] y, int QUADpp, int QUADpw)
{ /* {quad}{<-} anything */
        show(y);
        return(y);
}
inline bool[+] combBBBRG(bool[+] x, bool[+] y)
{/* AxA first axis catenate. Right rank greater */
 return(toB([x])++toB(y));
}
inline bool[.] comaBBB(bool x, bool[.] y)
{/* SxV catenate first (or last) axis */
 return([toB(x)]++toB(y));
}

inline bool[.] comaBBB(bool[.] x, bool[.] y)
{ /* VxV catenate first or last axis */
 return(toB(x)++toB(y));
}

inline double[2] comaIDD(int x, double y)
{/* SxS catenate first (or last) axis */
 return([toD(x)]++[toD(y)]);
}

inline int[.] comaIII(int[.] x, int y)
{/* VxS catenate first (or last) axis */
 return(toI(x)++[toI(y)]);
}

inline int[.] comaIII(int x, int[.] y)
{/* SxV catenate first (or last) axis */
 return([toI(x)]++toI(y));
}

inline double[.] comaDDD(double[.] x, double[.] y)
{ /* VxV catenate first or last axis */
 return(toD(x)++toD(y));
}

inline bool[+] utakIIB(int[.] x, int[+] y)
{ /* Vector-of-twos represent non-scalar */
/*
   % This could be any mix of powers-of-two with a bit of work.
   % The guts of represent on Booleans
*/
 cell = genarray(shape(x),false);
 yt = TRANSPOSE(y);
 z = with {
        (. <= iv <= .)
                : utakIIB(x, yt[iv]);
        } : genarray(shape(yt), cell);
 return(TRANSPOSE(z));
}





inline bool[.] utakIIB(int[.] x, int y)
{ /* Vector-of-twos represent scalar */
/*
*/
   cell = 0;
   k = shape(x)[[0]]-1;
   z = with {
        (. <= iv <= .)
                : BitAND(1,BitShiftRight(k-iv[0],toi(y)));
        } : genarray(shape(x), cell);
  return(toB(z));
}

inline int[*] indr(int[+] X, int I)
{ /* X[scalarI;;;] */
  /* Used only in conjunction with other indexing, e.g.,
   * X[scalarI;;j;]
   */
 z = X[[I]];
 return(z);
}




inline int[+] inds1(int[+] X, int [+] I0, int[+] Y)
{ /* X[;;nonscalarI;;;]<- nonscalarY */
  /* In function name indsXXX, XXX are ranks of various Is, or x if axis elided */
 
 z = ItoI(X);
 for(i0=0; i0<shape(I0)[[0]]; i0++){

 z[[I0[[i0]]]]=ItoI(Y[[i0]]);

 }

 return(z);
}



inline int CommandLineArgvXBI(bool y)
{ /* Get Command-line argument element #y as integer scalar */
  int z;
  junk, z = sscanf(argv(toi(y)), "%d");
  return( z);
}

inline int[.] slBII(bool[.] x, int[.] y)
{/* Boolean vector compress vector */
  zxrho = sum(toi(x));
  z = genarray([zxrho], 0);
  zi = 0;
  for(i=0; i<shape(x)[0]; i++)
    if ( x[i]) {
      z[[zi]] = y[[i]];
      zi++;
    }
  return(z);
}

inline double[.] slBDD(bool[.] x, double[.] y)
{/* Boolean vector compress vector */
  zxrho = sum(toi(x));
  z = genarray([zxrho], 0.0d);
  zi = 0;
  for(i=0; i<shape(x)[0]; i++)
    if ( x[i]) {
      z[[zi]] = y[[i]];
      zi++;
    }
  return(z);
}

inline bool orslXBBQUICKSTOP(bool[.] y)
{ /* First/last axis reduction of vector with quick stop*/
  z = with {
         (0*shape(y) <= iv < shape(y))
                : BtoB(y[iv]);
        } : foldfix( orBBB, ItoB(0), ItoB(1));
  return(z);
}


inline int maxslXIIFOLD(int[.] y)
{ /* First/last axis fold-based reduction of vector */
  lim = shape(y)[0]-1;
  z = with {
        (0*shape(y) <= iv < shape(y))
                : ItoI(y[lim-iv]);
       } :  fold( maxIII, ItoI(minint()));
  return(z);
}


inline double plusslXDDFOLD(double[.] y)
{ /* First/last axis fold-based reduction of vector */
  lim = shape(y)[0]-1;
  z = with {
        (0*shape(y) <= iv < shape(y))
                : DtoD(y[lim-iv]);
       } :  fold( plusDDD, ItoD(0));
  return(z);
}


inline int plusslXBIFOLD(bool[.] y)
{ /* First/last axis fold-based reduction of vector */
  lim = shape(y)[0]-1;
  z = with {
        (0*shape(y) <= iv < shape(y))
                : BtoI(y[lim-iv]);
       } :  fold( plusIII, ItoI(0));
  return(z);
}


inline int modXII(int y)
{ return(abs(toI(y)));
}

inline bool ltIBB(int x, bool y)
{ /* A<B on Boot/Int/Char */
  return(ItoI(x)<BtoI(y));
}

inline int plusIII(int x, int y)
{ return(ItoI(x)+ItoI(y));
}

inline double plusDDD(double x, double y)
{ return(DtoD(x)+DtoD(y));
}

inline bool orBBB(bool x, bool y)
{ return(BtoB(x)|BtoB(y));
}

inline int maxIII(int x, int y)
{ /* x max y */
 return (max(ItoI(x),ItoI(y)));
}

inline int[*] rhoIII(int[.] x, int[*] y)
{  /* APEX vector x reshape, with potential item reuse */
   z = with {
         ( . <= iv <= .) {
           offset = V2O( toi( x), iv);
           offset = _mod_SxS_( offset, prod( shape(y)));
           el = y[ O2V( shape( y), offset)];
          } : el;
       } : genarray( toi(x), 0);
   return( z);
}



inline int[*] indrfr(int fr, int[+] X, int[+] I)
{ /* X[;;;I;;;], where I has fr (framerank) semicolons to its left */
  /* This is actually "I from"fr X" */
  frameshape = take([fr], shape(X));
  cellshape =  shape(I)++drop([fr+1], shape(X));
  cell = genarray(cellshape, 0);
 z = with {
        (. <= iv <= .)
                : indrfr0(X[iv], I);
        } : genarray(frameshape, cell);
 return(z);
}

inline int[*] indrfr0(int[+] X, int[+] I)
{ /* X[I;;;] or    I from X */
  cellshape =  drop([1], shape(X));
  cell = genarray(cellshape, 0);
 z = with {
        (. <= iv <= .)
                : sel( I[iv], X);
        } : genarray(shape(I), cell);
 return(z);
}



inline int[*] indrfr(int fr, int[+] X, int I)
{ /* X[;;;I;;;], where I has fr (framerank) semicolons to its left */
  /* This is actually "I from"fr X" */
 frameshape = take([fr], shape(X));
 cellshape = drop([1+fr],shape(X));
 cell = genarray(cellshape,0);
 z = with {
        (. <= iv <= .)
                : sel( I, X[iv]);
        } : genarray(frameshape, cell);
 return(z);
}


inline int DFLOOR(double y, double QUADct)
{ /* Fuzzy floor */
  /* Definition taken from SHARP APL Refman May 1991, p.6-23
   * floor:  n <- (signum y) times nofuzzfloor 0.5+abs y)
   *         z <- n-(QUADct times 1 max abs y)<(n-y)
   * If you want a double result,  write: "y - 1| y".
   */
   n = tod(floor(0.5+fabs(y)));
   if (y < 0.0)
        n = -n;
   else if (0.0 == y)
        n = 0.0;
   range = fabs(y);
   if (1.0 > range)
        range = 1.0;
   fuzzlim = QUADct*range;
   ny = n-y;
   if (fuzzlim < ny)
        z = n - 1.0;
   else
        z = n;
   return(toi(z));
}

inline bool APEXFUZZEQ(double x, double y, double QUADct)
{ /* ISO APL Tolerant equality predicate */
 absx = abs(x);
 absy = abs(y);
 tolerance = QUADct * max(absx,absy);
 z = abs(x-y) <= tolerance;
 return(z);
}

inline int[+] TRANSPOSE(int[+] y)
{ /* Generic monadic transpose */
  z = with {
        ( . <= iv <= .)
                : y[reverse( iv)];
        }: genarray( reverse( shape(y)), 0);
  return(z);
}

inline bool[+] TRANSPOSE(bool[+] y)
{ /* Generic monadic transpose */
  z = with {
        ( . <= iv <= .)
                : y[reverse( iv)];
        }: genarray( reverse( shape(y)), false);
  return(z);
}

inline int ABC(int I, int Xshape)
{ /* (OLD) Array bounds check for indexed ref X[scalarI] & indexed assign */
 z = I;
 return(z);
}

inline int[+] ABC(int[+] I, int Xshape)
{ /* (OLD) Array bounds check for indexed ref X[nonscalarI] & indexed assign */
 z = I;
 return(z);
}

inline int V2O( int[.] shp, int[.] iv)
{ /* Vector iv to offset into array of shape shp */
  /* See V2O.dws workspace */
  offset = 0;
  wt = 1;
  for( i=shape(shp)[0]-1; i>=0; i--) {
    offset = offset + ( wt * iv[i]);
    wt = wt * shp[i];
  }
  return( offset);
}

inline int[.] O2V( int[.] shp, int offset)
{ /* Offset into array of shape shp to index vector */
  /* See V2O.dws workspace */
  iv = genarray( shape(shp), 1);
  wts = iv;
  for( i=shape(shp)[0]-2; i>=0; i--) {
    wts[i] = wts[i+1] * shp[i+1];
  }

  for( i=shape(shp)[0]-1; i>=0; i--) {
    iv[i] = _mod_SxS_( offset/wts[i], shp[i]);
    offset = offset - (iv[i]*wts[i]);
  }
  return( iv);
}

inline bool[.] scrunchXIB(int[.] r ,int QUADio,double QUADct)
{ 
/*
 ?
*/
/* dsf scalar(s) */
A_51=ltIBB(r,false);
 sign_0=( A_51);
 A_53=orslXBBQUICKSTOP( A_51);
 neg_0=( A_53);
 A_59=iotaXBI( neg_0,QUADio);
 A_CTR60_= 0;
A_CTR60z_ = (shape(A_59)[[0]])-1;
i_0=toI(0);
r_1=toI(r);
for(; A_CTR60_ <= A_CTR60z_; A_CTR60_++){
i_0 = A_59[[A_CTR60_]];
 A_63=slBII(sign_0,r_1);
 /* dsf scalar(s) */
A_64=plusBII(true,A_63);
 A_65=modXII( A_64);
 A_66=rhoXBI( sign_0);
 A_68=iotaXII( A_66,QUADio);
 A_69=slBII(sign_0,A_68);
 A_70= ABC(toi(A_69)-QUADio,shape(r_1)[0]);
A_72=inds1(r_1,A_70,A_65);
 r_1=( A_72);
 }
 A_75=maxslXIIFOLD( r_1);
 /* dsf scalar(s) */
A_79=maxBII(true,A_75);
 /* dsf scalar(s) */
A_80=logIID(2,A_79);
 A_82=minXDI( A_80,QUADct);
 /* dsf scalar(s) */
A_83=plusBII(true,A_82);
 width_0=( A_83);
 A_87=iotaXII( 32,QUADio);
 A_85= ABC(toi(width_0)-QUADio,shape(A_87)[0]);
A_89=A_87[[A_85]];
 width_1=( A_89);
 A_91=rhoIII(width_1,2);
 A_92=utakIIB(A_91,r_1);
 z_0=( A_92);
 A_95=iotaXBI( neg_0,QUADio);
 A_CTR96_= 0;
A_CTR96z_ = (shape(A_95)[[0]])-1;
i_1=toI(i_0);
z_2=toB(z_0);
for(; A_CTR96_ <= A_CTR96z_; A_CTR96_++){
i_1 = A_95[[A_CTR96_]];
 A_99=combBBBRG(sign_0,z_2);
 z_2=( A_99);
 }
 A_102=comaXBB( z_2);
 z_3=( A_102);
 A_104=rhoIII(5,2);
 A_105=utakIIB(A_104,width_1);
 A_106=comaBBB(neg_0,A_105);
 A_107=takeIBB(-8,A_106);
 A_108=comaBBB(A_107,A_102);
 z_4=( A_108);
 return(z_4);
}

inline double[.] rleXID(int n ,double QUADct,int QUADio)
{ 
/*
 ?
*/
/* dsf Scalar & clique */
A_30=barDDD(0.5,0.5);
oflow_0=( A_30);
 A_33=iotaXII( n,QUADio);
 A_36=scrunchXIB( A_33,QUADio,QUADct);
 i_0=( A_36);
 A_38=rhoXBI( i_0);
 A_40=iotaXII( A_38,QUADio);
 /* dsf scalar(s) */
A_41=plusDID(oflow_0,A_40);
A_42=slBDD(i_0,A_41);
 A_43=plusslXDDFOLD( A_42);
 A_47=plusslXBIFOLD( i_0);
 A_51=comaIDD(A_47,A_43);
 r_0=( A_51);
 A_54=iotaXII( n,QUADio);
 A_55=comaIII(A_54,-99);
 A_56=comaIII(-4,A_55);
 A_59=scrunchXIB( A_56,QUADio,QUADct);
 i_1=( A_59);
 A_61=rhoXBI( i_1);
 A_63=iotaXII( A_61,QUADio);
 /* dsf scalar(s) */
A_64=plusDID(oflow_0,A_63);
A_65=slBDD(i_1,A_64);
 A_66=plusslXDDFOLD( A_65);
 A_70=plusslXBIFOLD( i_1);
 A_74=comaIDD(A_70,A_66);
 A_75=comaDDD(r_0,A_74);
 r_1=( A_75);
 return(r_1);
}

int main()
{ 
/*
 ?
*/
n=CommandLineArgvXBI( true);
 QUADio_0=toI(( false));
 QUADct_0=( 1.0e-13);
 QUADpp_0=( 10);
 QUADpw_0=( 80);
 QUADrl_0=( 16807);
 QUADio_1=toI(( false));
 QUADrl_1=( 16807);
 QUADct_1=( 1.0e-12);
 QUADpp_1=( 17);
 QUADpw_1=( 80);
 A_61=rleXID( n,QUADct_1,QUADio_1);
 z_0=( A_61);
 A_65=quadXDD( z_0,QUADpp_1,QUADpw_1);
 A_66=plusslXDDFOLD( z_0);
 /* dsf scalar(s) */
A_70=barDDD(209736365304552.5,0.5);
 /* dsf scalar(s) */
A_72=eqDDB(A_70,A_66,QUADct_1);
 /* dsf scalar(s) */
A_73=barBBI(true,A_72);
 r_0=( A_73);
 A_77=quadXII( A_73,QUADpp_1,QUADpw_1);
 return(r_0);
}


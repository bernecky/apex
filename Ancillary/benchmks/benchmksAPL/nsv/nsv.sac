use Array: all;
use StdIO : all;
use Numerical : all;
use CommandLine: all;
use String: {to_string,tochar,sscanf};
use ArrayFormat: all;
use Bits: all;

/* Compiled by APEX Version: /home/apex/apex3/wss/sac3013.dws2012-11-07 10:22:09.175 */
/*
% This is the APEX stdlib.sis include file.
% Standard equates and constants for APL compiler
% Also standard coercion functions
*/

#define toB(x) to_bool((x))
#define toI(x) toi((x))
#define toD(x) tod((x))
#define toC(x) (x)
#define toc(x) ((x))

#define BtoB(x) ((x))
#define ItoI(x) ((x))
#define DtoD(x) ((x))
#define CtoC(x) ((x))

#define BtoI(x) toi((x))
#define BtoD(x) tod((x))
#define ItoB(x) to_bool((x))
#define ItoD(x) tod((x))
#define DtoB(x) to_bool((x))
#define DtoI(x) toi((x))


inline bool eqIIB(int x, int y)
{ /* A=B on non-doubles */
 return(ItoI(x) == ItoI(y));
}

inline int barBBI(bool x, bool y)
{ return(BtoI(x)-BtoI(y));
}

inline bool[+] eqIIB(int[+] x, int[+] y)
{ /* AxA Dyadic scalar fn, shapes may or may not match */
         sx = shape(y);
         z = with {
             ( . <= iv <= .) {
                      xel = x[iv];
                   yel = y[iv];
           } : eqIIB(xel,yel);
        } : genarray(sx, false);
  return(z);
}






inline int[+] iotaIII(int[.] x, int[+] y,int QUADio)
{
/* General case uses HeapGrade */
 sx = (shape(x))[[0]];
 PV = UpgradeHeap(x); /* faster to search x if its sorted! */
 PV = EPIORemoveDups(x, PV); /* This could be in UpgradeHeap, probably */
 z = with {
        (. <= iv <= .) {
                P = BinarySearch(x, y[iv], PV);
                /* following for real/complex when quadct != 0
                }: (P == sx) ? sx : PV[[ MinMatch( x, PV, P)]];
                */
                }: (P == sx) ? sx : PV[[P]];
        }: genarray( shape(y), sx);
 return(z+QUADio);
}





inline int[.]  rhoXII(int[+] y)
{ /* Shape of non-scalar */
 return(shape(y));
}

inline int[.] iotaXII(int[1] y, int QUADio)
{ /* Index generator on 1-element vector */
 /* HELP! Needs length error check */
/* HELP! Needs domain check for negative shp */
  z = QUADio+iota(toi(y[[0]]));
  return( z);
}

inline int[.] iotaXII(int y, int QUADio)
{ /* Index generator on scalar */
/* HELP! Needs domain check for negative shp */
  z = QUADio+iota(toi(y));
  return( z);
}

inline int[.]  rhoXBI(bool[+] y)
{ /* Shape of non-scalar */
 return(shape(y));
}

inline int[*] quadXII(int[*] y, int QUADpp, int QUADpw)
{ /* {quad}{<-} anything */
        show(y);
        return(y);
}
inline int plusdotmpyBIITRANSPOSE(bool[.] x, int[.] y)
{ /* Vector-Vector inner product */
  z =  plusslXIIFOLD(mpyIII(toI(x),toI(y)));
  return(z);
}



inline int[.] slIII(int x, int y)
{ /* Scalar replicate scalar */
 z = with {
        (. <= iv <= .)
                : y;
        } : genarray([toi(x)]);
 return(z);
}

inline int[.] slIII(int[.] x, int[.] y)
{/* Non-Boolean vector compress/replicate vector */
 /* HELP! non-boolean left argument needs a range check */
  intx = toi(x);
  zxrho = sum(intx);
  z = genarray([zxrho], 0);
  zi = 0;
  for(i=0; i<shape(x)[0]; i++)
    for(k=0; k<intx[[i]]; k++){
     z[[zi]] = y[[i]];
     zi++;
    }
  return(z);
}


inline bool[+] eqIIB(int x, int[+] y)
{ /* SxA scalar function */
  xel = toI(x);
  z = with {
     ( . <= iv <= .) {
              yel = toI(y[iv]);
                    } : eqIIB(xel,yel);
  } : genarray(shape(y), false);
  return(z);
}


inline bool[+] eqIIB(int[+] x, int y)
{ /* AxS scalar function */
  z = with {
  ( . <= iv <= .) {
              xel = x[iv];
   } : eqIIB(xel,y);
  } : genarray( shape(x), false);
  return(z);
}


inline int[+] mpyIII(int[+] x, int[+] y)
{ /* AxA Dyadic scalar fn, shapes may or may not match */
         sx = shape(y);
         z = with {
             ( . <= iv <= .) {
                      xel = x[iv];
                   yel = y[iv];
           } : mpyIII(xel,yel);
        } : genarray(sx, 0);
  return(z);
}






inline int[.] comaXII(int[+] y)
{ /* Ravel of anything with rank>1 */
  z = reshape([prod(shape(y))],y);
  return(z);
}

inline int BinarySearch(int[+] x, int y, int[.] PV)
{ /* Binary search x[PV] for y
   * PV is a permutation vector guaranteed to bring y into
   * non-descending order
   * If not-found, result is shape(x)
   */
 found = false;
 first = 0;
 sx  = (shape(PV))[[0]];
 indx = -1;                   /* Bobbo kant kode if this appears! */
 last = sx - 1;
 while ((first <= last) && !found) {
        indx = (first+last)/2;  /* index of middle entry */
        xval = toI(x[[PV[[indx]]]]);
        yval = toI(y); /* This is wrong - type coercion may fail! */
        if (    yval == xval) {
                found = true;
        } else if (yval < xval) {
                last = indx - 1;
        } else {
                first = indx + 1;
        }
 }
 z = found ? indx : sx;
 return(z);
}

inline int MinMatch (int[+] x, int[.] PV, int i)
{ /* Search permutation vector for minimum match.
   * i is an index into PV. PV is a permutation vector for
   * x that places x in non-descending order.
   *  We want minimum index into x that matches x[[PV[[i]]]]
   *  Because upgrade is stable, we only have to look left
   *  (except for real/complex x with non-zero {quad}ct)
   */
 v = x[[PV[[i]]]];
 j = i - 1;
 r = i;
 while (j >= 0) {
        if ( v == x[[PV[[j]]]]) {
                r = j;        /* new minimum index */
        } else {
                j = -1;       /* no more matches. Stop */
        }
        j = j - 1;
 }
 return(r);
}

inline int[.] EPIORemoveDups (int[+] x, int[.] PV)
{ /* Remove dups from PV of sorted left argument to indexof(right arg of membership */
 z = PV;
 shp = (shape(PV))[[0]];
 if (0 != shp) {
        sink = 0;
        source = 1;
        while (source < shp) {
                if ( x[[z[[sink]]]] != x[[PV[[source]]]]) {
                        sink++;
                        z[[sink]] = PV[[source]];
                }
                source++;
        }
 z = take([sink+1], z);
 }
 return(z);
}

inline int[.] UpgradeHeap(int[+] y)
{
/*    Do APL upgrade of array y using heapsort.
      This is a sub-function shared by upgrade/downgrade/indexof, etc.
      This version adapted from the Sara Baase "Computer Algorithms"
      version of heapsort.
     Robert Bernecky 2006-11-14
     Knuth, Vol. III, pp. 145-148 gives a good example.
     APL model: (See workspace apex2003/wss/upgrade or
                 apex2003/wif/upgrade)
                Also UTGrade.dws
r{<-}upgradeHeap v;#io;N;heap
@ Upgrade vector using heapsort
#io{<-}0
N{<-}{rho}v
:if N{<=}1
  r{<-}{iota}N
:else
  heap{<-}MakeHeap(v)
  r{<-}(UnHeap(heap))
:endif
*/

 N = shape(y)[[0]];
 if (N <= 1)
        z = iota(N);
 else{
        heap = MakeHeap(y);
        z = UnHeap(heap,y);
 }
 return(z);
}


inline int[.] MakeHeap(int[+] v)
{ /* Build heap from array v. v has at least two elements */
/*
r{<-}MakeHeap v;i;n;heap;biggest
@ Build heap from v
@ We know v has at least two elements
N{<-}{rho}v
heap{<-}{iota}N
:for i :in {reverse}{iota}{floor}N{divide}2
  y FixHeap i,heap[i],n
:endfor
r{<-}heap
*/
 n = shape(v)[[0]];
 heap = iota(n);
 lim = n/2;
 for(i=lim-1; i>=0; i--) {
        heap = FixHeap(heap, v, i, heap[[i]], n);
 }
 return(heap);
}

inline int[.] UnHeap(int[.] heap, int[+]v)
{ /* Extract heap elements in top-to-bottom order */
  n = shape(v)[[0]];
  for(heapsize= n-1; heapsize>0; heapsize--){
        biggest = heap[[0]];
        heap = FixHeap(heap,v,0,heap[[heapsize]],heapsize);
        heap[[heapsize]] = biggest;
  }
 return(heap);
}

inline int[.] FixHeap(int[.] heap, int[+] v, int root,
        int heapitem, int heapsize)
{ /* Restore heap invariant: parent>= both children */
 vacant = root;
 lchild = 1+vacant+vacant;
 while( lchild < heapsize) {
        bigC = lchild;      /* Identify larger child, if any */
        rchild = lchild+1;
        if ((lchild<(heapsize-1))){
                li = heap[[lchild]];
                ri = heap[[rchild]];
                if ((GradeGT(v[[ri]],v[[li]])) ||
                        (match(v[[ri]], v[[li]])&&(ri>li))){ /* Stability */
                bigC = rchild; /* right child larger */
                }
        }
        /* parent vs big kid*/
        li = heap[[bigC]];
        if ((GradeGT( v[[li]], v[[heapitem]])) ||
                (match(v[[li]], v[[heapitem]]) &&(li>heapitem))) {
                        heap[[vacant]] = heap[[bigC]];
                        vacant = bigC;
                        lchild = 1+vacant+vacant;
        } else lchild = heapsize;   /* exitloop */
 }
 heap[[vacant]] = heapitem;
 return(heap);
}



inline int plusslXIIFOLD(int[.] y)
{ /* First/last axis fold-based reduction of vector */
  lim = shape(y)[0]-1;
  z = with {
        (0*shape(y) <= iv < shape(y))
                : ItoI(y[lim-iv]);
       } :  fold( plusIII, ItoI(0));
  return(z);
}


inline int mpyIII(int x, int y)
{ return(ItoI(x)*ItoI(y));
}

inline int plusIII(int x, int y)
{ return(ItoI(x)+ItoI(y));
}

inline int[+] mpyIII(int x, int[+] y)
{ /* SxA scalar function */
  xel = toI(x);
  z = with {
     ( . <= iv <= .) {
              yel = toI(y[iv]);
                    } : mpyIII(xel,yel);
  } : genarray(shape(y), 0);
  return(z);
}


inline int[+] mpyIII(int[+] x, int y)
{ /* AxS scalar function */
  z = with {
  ( . <= iv <= .) {
              xel = x[iv];
   } : mpyIII(xel,y);
  } : genarray( shape(x), 0);
  return(z);
}


inline bool GradeGT(int x, int y)
{ /* Integer Comparator for upgrade */
 return(x>y);
}

inline bool GradeGT(int[+] x, int[+] y)
{ /* Integer Array Comparator for upgrade */
  /* The ravels of the two arrays are compared.  */
 z = with {
       (0*shape(x) <= iv < shape(x))
                : x[iv] > y[iv];
        }: foldfix( &, true,false);
 return(z);
}

inline bool[.] nsvXIB(int[.] y ,int QUADio)
{ 
/*
 ?
*/
A_20=iotaIII(y,y,QUADio);
 A_21=rhoXII( y);
 A_23=iotaXII( A_21,QUADio);
 /* dsf Check needed */
A_25=eqIIB(A_23,A_20);
r_0=( A_25);
 return(r_0);
}

inline int[.] testdataXII(int n ,int QUADio)
{ 
/*
 ?
*/
A_20=iotaXII( 10,QUADio);
A_21=slIII(10,n);
 A_22=slIII(A_21,A_20);
 r_0=( A_22);
 return(r_0);
}

inline int benchnsvXII(int n ,int QUADio)
{ 
/*
 ?
*/
A_22=testdataXII( n,QUADio);
 A_24=nsvXIB( A_22,QUADio);
 r_0=( A_24);
 A_26=rhoXBI( r_0);
 A_28=iotaXII( A_26,QUADio);
 A_29=plusdotmpyBIITRANSPOSE(r_0,A_28);
 r_1=( A_29);
 return(r_1);
}

int main()
{ 
/*
 ?
*/
QUADio_0=toI(( false));
 QUADct_0=( 1.0e-13);
 QUADpp_0=( 10);
 QUADpw_0=( 80);
 QUADrl_0=( 16807);
 QUADio_1=toI(( false));
 n_0=( 2000000);
 QUADrl_1=( 16807);
 QUADpp_1=( 16);
 QUADpw_1=( 80);
 A_57=benchnsvXII( n_0,QUADio_1);
 r_0=( A_57);
 A_61=quadXII( r_0,QUADpp_1,QUADpw_1);
 /* dsf scalar(s) */
A_63=eqIIB(r_0,90000000);
/* dsf scalar(s) */
A_64=barBBI(true,A_63);
 r_1=( A_64);
 A_68=quadXII( A_64,QUADpp_1,QUADpw_1);
 return(r_1);
}


use Array: all;
use StdIO : all;
use Numerical : all;
use CommandLine: all;
use String: {to_string,tochar,sscanf};
use ArrayFormat: all;
use Bits: all;

/* Compiled by APEX Version: /home/apex/apex3/wss/sac3013.dws2012-11-07 10:24:09.533 */
/*
% This is the APEX stdlib.sis include file.
% Standard equates and constants for APL compiler
% Also standard coercion functions
*/

#define toB(x) to_bool((x))
#define toI(x) toi((x))
#define toD(x) tod((x))
#define toC(x) (x)
#define toc(x) ((x))

#define BtoB(x) ((x))
#define ItoI(x) ((x))
#define DtoD(x) ((x))
#define CtoC(x) ((x))

#define BtoI(x) toi((x))
#define BtoD(x) tod((x))
#define ItoB(x) to_bool((x))
#define ItoD(x) tod((x))
#define DtoB(x) to_bool((x))
#define DtoI(x) toi((x))


inline int[+] barXII(int[+] y)
{ /* Monadic scalar functions on array */
  z = with {
        ( . <= iv <= .)
                : barXII(toI(y[iv]));
        } : genarray(shape(y), 0);
  return(z);
}

inline int barIBI(int x, bool y)
{ return(ItoI(x)-BtoI(y));
}

inline int barBBI(bool x, bool y)
{ return(BtoI(x)-BtoI(y));
}

inline int[+] plusIII(int[+] x, int y)
{ /* AxS scalar function */
  z = with {
  ( . <= iv <= .) {
              xel = x[iv];
   } : plusIII(xel,y);
  } : genarray( shape(x), 0);
  return(z);
}


inline int[+] barIII(int[+] x, int[+] y)
{ /* AxA Dyadic scalar fn, shapes may or may not match */
         sx = shape(y);
         z = with {
             ( . <= iv <= .) {
                      xel = x[iv];
                   yel = y[iv];
           } : barIII(xel,yel);
        } : genarray(sx, 0);
  return(z);
}






inline int[+] plusIII(int x, int[+] y)
{ /* SxA scalar function */
  xel = toI(x);
  z = with {
     ( . <= iv <= .) {
              yel = toI(y[iv]);
                    } : plusIII(xel,yel);
  } : genarray(shape(y), 0);
  return(z);
}


inline int[*] takeIII(int[1] x, int[.] y)
{ /* Vector take Vector */
  return(take(toi(x),y));
}

inline int[.] ugrdXII(int[.] y, int QUADio)
{ /* Integer vector upgrade
   * See radix upgrade model in ws RadixGrade.dws
   */
  rad = 256;
  radixbase = 8; /* 2 log rad */
  bitsperint = 32;
  numpasses = bitsperint/8;
  hist = RadixGradeHistograms( y);
  z = genarray( shape(y), -1);
  if( 0 != shape(y)[0]) {
    pv = iota(shape(y)[0]);
    /* LSB-> MSB sort order */
    for( pas=numpasses-1; pas>=0; pas--) {
      /* Skip pass if all nums in same bucket */
      if( shape(y)[0] != hist[pas, RadixGradeGetIdx( pas, y[pv[0]])]) {
        pvo = RadixGradeOffsets( rad, pas, hist);
        for( i=0; i<shape(y)[0]; i++) {
          val = RadixGradeGetIdx( pas, y[pv[i]]);
          z[pvo[val]] = pv[i];
          pvo[val] =  pvo[val] + ((val<0) ? -1 : 1);
        }
        pv = z;
      }
    }
  }
  z = QUADio + z;
  return( z);
}




inline int[.] ugrdXIIPV(int[.] y, int QUADio)
{ /* Upgrade of permutation vector. */
  /* This exploits array predicate astPredPV */
  z = genarray(shape(y), -1);
  for( i=0; i<shape(y)[0]; i++) {
        z[[y[[i]]]] = i+QUADio;
  }
 return(z);
}


inline int[.]  rhoXII(int[+] y)
{ /* Shape of non-scalar */
 return(shape(y));
}

inline int[.] iotaXII(int y, int QUADio)
{ /* Index generator on scalar */
/* HELP! Needs domain check for negative shp */
  z = QUADio+iota(toi(y));
  return( z);
}

inline int[*] quadXII(int[*] y, int QUADpp, int QUADpw)
{ /* {quad}{<-} anything */
        show(y);
        return(y);
}
inline int[.] comaIII(int[.] x, int[.] y)
{ /* VxV catenate first or last axis */
 return(toI(x)++toI(y));
}

inline int[.] comaIII(int x, int[.] y)
{/* SxV catenate first (or last) axis */
 return([toI(x)]++toI(y));
}

inline bool sameIIB(int[+] x, int[+] y)
{ /* Non-scalar match non-scalar */
   z = (( _dim_A_( x) == _dim_A_( y))          &&
       ( all( _shape_A_( x) == _shape_A_( y))) &&
       ( all( eqIIB(toI( x),  toI( y)))));
  return(z);
}



inline int barXII(int y)
{ return(-y);
}

inline int plusIII(int x, int y)
{ return(ItoI(x)+ItoI(y));
}

inline int barIII(int x, int y)
{ return(ItoI(x)-ItoI(y));
}

inline int[+] barIII(int x, int[+] y)
{ /* SxA scalar function */
  xel = toI(x);
  z = with {
     ( . <= iv <= .) {
              yel = toI(y[iv]);
                    } : barIII(xel,yel);
  } : genarray(shape(y), 0);
  return(z);
}


inline int[+] barIII(int[+] x, int y)
{ /* AxS scalar function */
  z = with {
  ( . <= iv <= .) {
              xel = x[iv];
   } : barIII(xel,y);
  } : genarray( shape(x), 0);
  return(z);
}


inline bool[+] eqIIB(int[+] x, int[+] y)
{ /* AxA Dyadic scalar fn, shapes may or may not match */
         sx = shape(y);
         z = with {
             ( . <= iv <= .) {
                      xel = x[iv];
                   yel = y[iv];
           } : eqIIB(xel,yel);
        } : genarray(sx, false);
  return(z);
}






inline int[.] comaXII(int[+] y)
{ /* Ravel of anything with rank>1 */
  z = reshape([prod(shape(y))],y);
  return(z);
}

inline int[.,.] RadixGradeHistograms( int[.] y)
{ /* Create histograms for integer radix up/downgrade */
  rad = 256;
  radixbase = 8;
  bitsperint = 32;
  numpasses = bitsperint/radixbase;
  hist = genarray( [rad * numpasses], 0);
  hindex = rad * iota( numpasses);
  for( i=0; i<shape(y)[0]; i++) {
    for( p=0; p<numpasses; p++) {
     v = RadixGradeGetIdx( p, y[i]);
     ndx = hindex[p] + v;
     hist[ndx] = hist[ndx] + 1;
    }
  }
  z = reshape( [ numpasses, rad], hist);
 return(z);
}

inline int[.] RadixGradeOffsets( int rad, int pas, int[.,.] hist)
{ /* Make per-pass initial offsets into pass result vector */
  rd2 = rad / 2;
  nnv = sum( drop( [rd2], hist[pas])); /* # of negative results */
  z = genarray( [rad], 0);
  if( 0 == pas) {
    /* Fancy footwork here handles negative numbers */
    z[0] = nnv;
    for( i=0; i<(rd2-1); i++) {
      z[i+1] = z[i] + hist[pas,i];
    }
    for( i=rd2; i<rad-1; i++) {
      z[i+1] = z[i] + hist[pas,i];
    }
  } else {
    for( i=0; i<(rad-1); i++) {
      z[i+1] = z[i] + hist[pas,i];
    }
  }
  return(z);
}

inline int RadixGradeGetIdx( int pas, int v)
{ /* Get masked value for pass pas and value v */
  bitsperint = 32;
  radixbase = 8;
  numpasses = bitsperint/radixbase;
  rad = 256;
  z = BitAND(( rad - 1),
             BitShiftRight((( numpasses - 1) - pas) * radixbase, v));
  return(z);
}

inline bool APEXFUZZEQ(double x, double y, double QUADct)
{ /* ISO APL Tolerant equality predicate */
 absx = abs(x);
 absy = abs(y);
 tolerance = QUADct * max(absx,absy);
 z = abs(x-y) <= tolerance;
 return(z);
}

inline bool eqIIB(int x, int y)
{ /* A=B on non-doubles */
 return(ItoI(x) == ItoI(y));
}

inline bool[+] eqIIB(int x, int[+] y)
{ /* SxA scalar function */
  xel = toI(x);
  z = with {
     ( . <= iv <= .) {
              yel = toI(y[iv]);
                    } : eqIIB(xel,yel);
  } : genarray(shape(y), false);
  return(z);
}


inline bool[+] eqIIB(int[+] x, int y)
{ /* AxS scalar function */
  z = with {
  ( . <= iv <= .) {
              xel = x[iv];
   } : eqIIB(xel,y);
  } : genarray( shape(x), false);
  return(z);
}


inline int[.] LQDIII(int[.] L, int[.] R,int QUADio)
{ 
/*
 ?
*/
A_23=comaIII(R,L);
 A_25=ugrdXII( A_23,QUADio);
 A_27=ugrdXIIPV( A_25,QUADio);
 Z_0=( A_27);
 A_29=rhoXII( Z_0);
 R_0=( A_29);
 A_31=rhoXII( L);
 A_32=barXII( A_31);
 A_33=takeIII(A_32,Z_0);
 Z_1=( A_33);
 /* dsf scalar(s) */
A_35=barIBI(QUADio,true);
 A_36=comaIII(A_35,Z_1);
 /* dsf scalar(s) */
A_37=plusIII(R_0,QUADio);
 A_38=comaIII(Z_1,A_37);
 /* dsf Check needed */
A_39=barIII(A_38,A_36);
/* dsf scalar(s) */
A_40=plusIII(-1,A_39);
 Z_2=( A_40);
 return(Z_2);
}

int main()
{ 
/*
 ?
*/
QUADio_0=toI(( false));
 QUADct_0=( 1.0e-13);
 QUADpp_0=( 10);
 QUADpw_0=( 80);
 QUADrl_0=( 16807);
 QUADio_1=toI(( false));
 A_58=iotaXII( 5000000,QUADio_1);
 n_0=( A_58);
 b_0=( [10, 100, 1000, 10000, 100000]);
 QUADrl_1=( 16807);
 QUADct_1=( 1.0e-13);
 QUADpp_1=( 16);
 QUADpw_1=( 80);
 A_66=LQDIII(b_0,n_0,QUADio_1);
 r_0=( A_66);
 A_70=quadXII( r_0,QUADpp_1,QUADpw_1);
 A_72=sameIIB(r_0,[11, 90, 900, 9000, 90000, 4899999]);
/* dsf scalar(s) */
A_73=barBBI(true,A_72);
 r_1=( A_73);
 A_77=quadXII( A_73,QUADpp_1,QUADpw_1);
 return(r_1);
}


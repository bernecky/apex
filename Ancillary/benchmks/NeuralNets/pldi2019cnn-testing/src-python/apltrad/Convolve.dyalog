﻿ z←alpha Convolve omega;d;iv;xx;zold
⍝ Stencils of omega  on image alpha
 zold←omega conv alpha
 iv←alpha ConvIndices omega
 z←iv⌷⍤0 1⊢,omega
 d←⍴⍴alpha
 z←SumRavel⍤d⊢alpha×⍤d⊢z
 xx←0÷(zold≡z)

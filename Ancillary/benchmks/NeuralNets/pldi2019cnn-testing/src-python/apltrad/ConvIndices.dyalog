﻿z← alpha ConvIndices omega
⍝ stencil alpha on image omega
z← (alpha ConvCorners omega)∘.+alpha StencilIndices omega

﻿ z←main omega;epochs;batchsz;err;trainings;tests;rate;k1;b1;k2;b2;fc;b;tr_img;te_img;tr_lab;te_lab;trainings;tests;err;out;correct;target;error;sgi0;sgi1;sgi2;sgi3;sgi4;sgi5;sgi6;sgi7;sgi8;sgi9;sgi10;sgi11;sgi12;sgo0;sgo1;sgo2;sgo3;sgo4;sgo5;sgo6;e;sgiTe1;sgiTe2;sgiTe3;sgiTe4;sgiTe5
 ⎕←'Starting main at ',⍕⎕TS
 epochs←10
 batchsz←100
 err←0
 trainings←1000
 tests←10000
 rate←0.05
 k1←6 5 5⍴÷25
 b1←6⍴÷6
 k2←12 6 5 5⍴÷150
 b2←12⍴÷12
 fc←10 12 1 4 4⍴÷192
 b←10⍴÷10
 tr_img←ReadImages'src/input/train-images-idx3-ubyte'
 te_img←ReadImages'src/input/t10k-images-idx3-ubyte'
 tr_lab←ConvLab ReadLabels'src/input/train-labels-idx1-ubyte'
 te_lab←ConvLab ReadLabels'src/input/t10k-labels-idx1-ubyte'
 trainings←trainings⌊≢tr_img
 tests←tests⌊≢te_img
 tr_img←trainings↑tr_img
 tr_lab←trainings↑tr_lab
 te_img←tests↑te_img
 te_lab←tests↑te_lab
 ⎕←'Running Zhang with ',(⍕epochs),' epochs, batchsz ',⍕batchsz
 ⎕←(⍕trainings),' training images, ',(⍕tests),' tests',' rate ',⍕rate
 ⎕←'at ',⍕⎕TS
 sgi7←tr_img
 sgi8←tr_lab
 sgi9←err
 sgi10←batchsz
 sgi11←rate
 sgi12←trainings
 ⍝⍝(k1 b1 k2 b2 fc b err)←TrAll (0  k1 b1 k2 b2 fc b tr_img tr_lab err batchsz rate trainings)

:for e :in ⍳epochs
 ⎕←'Starting Training epoch ', e, ' at ',⍕⎕TS
 sgi0←0
 sgi1←k1
 sgi2←b1
 sgi3←k2
 sgi4←b2
 sgi5←fc
 sgi6←b
 k1←TrAll sgi0
 ⎕←'Ending   Training epoch ', e, ' at ',⍕⎕TS

 b1←sgo1
 k2←sgo2
 b2←sgo3
 fc←sgo4
 b←sgo5
:endfor

⍝ b1 k2 b2 fc b global into TestZhang. Read-only
 ⎕←'Starting TestZhang at ',⍕⎕TS
sgiTe1←b1
sgiTe2←k2
sgiTe3←b2
sgiTe4←fc
sgiTe5←b
 out←te_img TestZhang⍤2 99⊢k1
 ⎕←'Back from TestZhang at ',⍕⎕TS
 correct←+/,(MaxPos⍤1⊢,⍤5⊢te_lab)=MaxPos⍤1⊢,⍤5⊢out
 error←out MeanSquaredError te_lab
 ⎕←(⍕correct),' of ',(⍕tests),' correctly identified!'
 ⎕←'The mean error of ',(⍕tests),' is ',⍕error÷tests
 z←correct-1028 ⍝ Expected result
 ⎕←'Finished at ',⍕⎕TS

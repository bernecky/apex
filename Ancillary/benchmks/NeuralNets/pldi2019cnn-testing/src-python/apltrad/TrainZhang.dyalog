﻿ z←alpha TrainZhang omega;k1;b1;k2;b2;fc;b;s1;s2;out;d_out;d_s2;d_fc;d_b;d_c2;d_s1;d_k2;d_b2;d_c1;d_k1;d_b1;callsg0;callsg1;callsg2;callsg3;bl;sgo0;tr_img;tr_lab;img;lab;ri;bl1;bl2;c1;c2;err
⍝k1←sgi0
⍝b1←sgi1
⍝k2←sgi2
⍝b2←sgi3
⍝fc←sgi4
⍝b←sgi5
 (tr_img tr_lab k1 b1 k2 b2 fc b)←omega
 img←alpha⌷tr_img
 lab←alpha⌷tr_lab
 ri←⍴⍴img
 sgi1←b1
 c1←Sigmoid img MultiConv k1
 s1←2 2 AvgPool⍤2⊢c1

 ri←⍴⍴s1
 sgi1←b2
 c2←Sigmoid s1 MultiConv k2
 s2←2 2 AvgPool⍤2⊢c2

 ri←⍴⍴s2
 sgi1←b
 out←Sigmoid s2 MultiConv fc
 d_out←out-lab
 err←out MeanSquaredError lab

 bl←out BackLogistic d_out
⍝ d_out wts in bias

 callsg0←bl
 callsg1←fc
 callsg2←s2
 d_s2←BackMultiConv b
 d_fc←sgo0
 d_b←sgo1

 bl1←c2 BackLogistic BackAvgPool⍤2⊢d_s2
 callsg0←bl1
 callsg1←k2
 callsg2←s1
 callsg3←b2
 d_s1←BackMultiConv b2
 d_k2←sgo0
 d_b2←sgo1

 bl2←c1 BackLogistic BackAvgPool⍤2⊢d_s1
 callsg0←bl2
 callsg1←k1
 callsg2←img
 callsg3←b1
 z←BackMultiConv b1
 d_k1←sgo0
 d_b1←sgo1

 sgo1←d_b1
 sgo2←d_k2
 sgo3←d_b2
 sgo4←d_fc
 sgo5←d_b
 sgo6←err

 z←(d_k1 d_b1 d_k2 d_b2 d_fc d_b err)


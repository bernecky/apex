BackIn← {(d_out w in)←⍵ ⋄ ⊃+/,{(⍴in)↑(-⍵+⍴d_out)↑(⍵⌷w)×d_out}¨(⍳⍴w)}
⍝ For every index iv of `w`, compute w[iv]*d_out and shift it
⍝ by iv.  Then take shape(in) elements of that and sum all such
⍝ arrays with `+`.


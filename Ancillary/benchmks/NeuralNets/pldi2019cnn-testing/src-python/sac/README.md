SaC CNN
-------

The files are laid out as follows:

```
├── cnn.sac    # module that implements all NN operations
├── cnn_tc.sac # the same as above, but using set-notation
├── mnist.sac  # module to read MNIST data sets
└── zhang.sac  # the CNN
```

Compilation:

```
sac2c -t seq mnist.sac
sac2c -t seq cnn.sac
sac2c -t seq -o cnn zhang.cnn
```

Set flag `-t` to either `seq`, `mt_pth`, or `cuda` to change
which hardware configuration to use.

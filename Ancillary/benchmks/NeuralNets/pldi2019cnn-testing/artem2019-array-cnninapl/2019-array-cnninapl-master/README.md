This paper describes the implementation of the Zhang CNN in APL.


APL Fonts
=========

The utf-8 symbols in the APL listings are handled by XeLaTex.
In order to build the paper one has to have `DejaVu Sans Mono` font available on
the system.


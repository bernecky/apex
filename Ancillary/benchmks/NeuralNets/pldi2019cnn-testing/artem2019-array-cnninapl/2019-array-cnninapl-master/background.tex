\section{Background}%
\label{sec:background}


% \fixme{Note that this text about CNNs is a literal copy from the other paper.
% Therefore, we might want to adjust it at least at some point.}
%Due to space limit 
\subsection{CNN}
This section is an overview of machine learning algorithms, 
focusing on the computational aspects of CNNs.
For an in-depth review, refer to~\cite{schmidhuber2015learning,INDOLIA2018679}.

Machine learning algorithms
are based on the idea that we want to learn
(guess) a function $f$ that maps the input variables $X$ to the output variables
$Y$, \ie{} $Y = f\ X$, in an optimal way, according to some cost function.
After $f$ is found for the existing sample, we want to use $f$ to
make predictions for new inputs.

CNNs belong to the class of machine learning algorithms called neural networks.
They have a distinctive feature: the function $f:X\to Y$ that we
want to learn is a composition of functions $g_i$, 
that can be further decomposed into smaller functions.
Overall, such a composition forms a graph (or
\emph{network}) connecting inputs $X$ with outputs $Y$.

A typical function composition takes the form:
\(
    f\ x = A\ \left(\sum_i w_i\ (g_i\ x)\right)
\)
where $A$ is an activation function (usually chosen to be continuous and
differentiable, \eg{} sigmoid, hyperbolic tangent, \etc{}) and $w_i$ are
so-called weights.  These weights are parameters of the approximation that
we want to find, chosen so as to minimise our cost function.
%, similar to the role played by
%$\beta$ in linear regression\todo{explain this}

Usually, neural networks are designed to allow slicing of the
elementary functions $g_i$ into layers, so that all elements of a given
layer can be computed independently. Layering has the beneficial effect
of making that computation highly parallel.
A layer is an activation function of the weight\-ed sum of other 
layers, so most of the transitions in the network
can be expressed as matrix or tensor operations.

Very often, due to the network size and complexity, a closed
solution that finds optimal weights either 
does not exist or is very difficult to find.
Therefore, weight prediction is usually performed in an iterative manner.
In this case, the concept of backpropagation --- a method to calculate
the gradient of the objective function with respect to the weights, 
is of significant importance. It provides a working solution that
is straight-forward to compute:
\(
    w := w - \eta \nabla F(w)
\)
where $w$ are all the weights in the given network.  
For the cases in which our objective function can be 
written as: $F = \sum_i F_i$, the gradient descent
can be rewritten as: $w - \eta\nabla\sum_i F_i = w - \eta\sum_i\nabla F_i$.
Furthermore, the stochastic gradient descent~\cite{SGD} approximates the true
gradient as follows:
\(
    w := w - \eta\nabla F_i(w)
\)
which is typically more efficient.  Intuitively, if we process a batch of
items, we can update weights after processing one individual item.
Finally, with carefully chosen activation functions $A$,
the computation of backpropagation can be expressed as
a composition of linear-algebraic operations.

\subsection{APL}

The design of APL, a language with first-class arrays,
is driven by the desire to find an consistent, terse, 
and efficient notation to communicate mathematical ideas.
Although this might seem a purely syntactical consideration, 
it is taken seriously by advocates of array languages.
% In
%fact, Turing Award lecture of Kenneth Iverson, the author of APL, was
%titled \emph{Notation as a Tool of Thought}~\cite{KEIverson:tot}.
%Iverson's
The emphasis is that the user should express
what we want to compute, rather than how it should be computed,
using the smallest number of generic, easily composable primitives.
The shorter the program is, the quicker we, and others, 
can understand what it is doing. 
Furthermore, code that is not there can not break,
so shorter programs are inherently more bug-free than longer ones.
We offer a bit of APL knowledge here, to ease reading the paper.
For the full language reference manual, 
see~\cite{dyalog:ReferenceManual170}. 
We restrict ourselves to a purely functional subset of 
APL, and use no explicit array indexing, which simplifies
automatic or manual analysis of operation compositions.


% be it humans or compu
% These restrictions offer several distinct benefits. Purely
% functional operations are more amenable to analysis
% by humans and by compilers, which in turn increases 
% opportunities for program optimization by a compiler,
% such as \sac. Explicit array indexing is considered harmful,
% in the sense that it harks back to a pocket-calculator world.
% In addition, expressions written not using explicit array
% indexing are often much simpler to optimize for better performance.

% I don't think this helps at all.
%We use the Dyalog~\cite{} dialect of the APL, that can
%be run with Dyalog interpreters versions 16 and 17.  To be more specific,
%we will use a \verb|⎕ML←3| variant of the language, with array indexing
%that starts from 0: \verb|⎕IO←0|, and we only use \emph{dfns} to define
%functions.

\paragraph{Arrays} The principal data type in APL is a rectangular n-dimensional array ---
an object that can be indexed with $n$-element tuples of natural numbers.
Each array carries its shape --- an $n$-element tuple of natural numbers
that defines the valid set of indices into that array.  For example:
\begin{verbatim}
      w
0 1 2
3 4 5
6 7 8
\end{verbatim}
In this paper, an indented expression is the one
we wish to be evaluated, and the non-indented text
comprises the result of the computation.
In this example, \verb|w| is a variable
that evaluates to a $3 \times 3$ two-dimensional array with elements
$0, 1, 2,\dots$.  
We can select elements from such an array 
with two-element tuples, where both of
the elements are less than 3.

We can find the \emph{shape} of an array using the \verb|⍴| function:
\begin{verbatim}
      ⍴w
3 3
\end{verbatim}
The \emph{rank}, or dimension, of an 
array can be found from the shape of the shape:
\begin{verbatim}
      ⍴⍴w
2
\end{verbatim}

Numbers and characters are zero-dimensional arrays; their shape is
an empty vector, and their rank is 0:
\begin{verbatim}
      ⍴5

      ⍴⍴5
0
\end{verbatim}
We call 0-dimensional arrays \emph{scalars} and 1-dimensional ones
\emph{vectors}.

\paragraph{Arithmetic Functions}
Functions in APL are of order 1 and 2, and application is 
right-associative.  First-order functions can take either
one or two arguments\footnote{In APL parlance, 
one-argument functions are called \emph{monadic} and two-argument ones \emph{dyadic}. In this paper, we call them one- and two-argument.}.  
One-argument ones have a prefix form;
two-argument ones an infix one. For example:
\begin{verbatim}
      ⍴⍴w
2
      2×2+2
8
\end{verbatim}
Right associativity lets us avoid parentheses in the first expression;
we get 8, rather than 6, in the second one.  Typical arithmetic operations
include: \verb|+ - × ÷|, 
the \verb|*| stands for power, \ie{} \verb|2 * 3| evaluates
to 8.  A sequence of values is automatically merged into an array:
\begin{verbatim}
      2 3 4 5
2 3 4 5
\end{verbatim}
Arithmetic functions are automatically lifted to the element level, when
applying to arrays of the same shape:
\begin{verbatim}
      2 3 4 5 + 2 3 4 5
4 6 8 10
\end{verbatim}
When one of the arguments is an array and the other argument is a scalar,
the scalar is extended to be an array of the same shape as the other
argument: \verb|1 + 2 3 4 5| evaluates to \verb|3 4 5 6| as well as
\verb|2 3 4 5 + 1|.
%\begin{verbatim}
%      1 + 2 3 4 5
%3 4 5 6
%\end{verbatim}
%      2 3 4 5 + 1
%3 4 5 6
%\end{verbatim}
\paragraph{Nested Arrays}
APL supports nested arrays --- those with non-homogeneous elements.
This is achieved, primarily, by means of two 
functions: enclose \verb|⊂| and disclose \verb|⊃|.  
All elements of APL arrays are zero-dimensional scalars.
Enclose creates, from any argument array, a scalar,
which can then become an element of other arrays. 
The value of that argument array can be
be retrieved by applying disclose to that scalar. 
The overall concept is similar to the notion of pointers,
in which enclose takes a reference to an object, 
and disclose dereferences the reference.
% WTF, this is plainly wrong...
%The difference is that enclose and disclose are functional,
%whereas pointers are dysfunctional.
For example:
\begin{verbatim}
      ⊂w
┌─────┐
│0 1 2│
│3 4 5│
│6 7 8│
└─────┘
\end{verbatim}
The box, a display artifact of the APL session,
shows that the value is enclosed.  The shape of \verb|⊂w| is
the empty vector, hence is a scalar. 
When non-scalar values are written in sequence, the resulting
array contains enclosed elements of that sequence.  For example:
\begin{verbatim}
      (2 3) w
┌───┬─────┐
│2 3│0 1 2│
│   │3 4 5│
│   │6 7 8│
└───┴─────┘
\end{verbatim}
The shape of the above array is \verb|2|.  In order to access the element
\verb|4| in the above array we can select (using the \verb|⌷| operator)
the second vector element, disclose it, then select from it
 at index \verb|1 1|:
\begin{verbatim}
      1 1 ⌷ ⊃ 1 ⌷ (2 3) w
4
\end{verbatim}

\paragraph{Array Functions}
The one-argument \verb|,| (comma) function flattens any multi-dimensional array,
in row-major order.  The two-argument version of \verb|,| (comma)
concatenates arrays on the last axis, implying that
all the shape elements of both arguments, except the last one,
must be the same:
\begin{verbatim}
      (,w) (w,w)
┌─────────────────┬───────────┐
│0 1 2 3 4 5 6 7 8│0 1 2 0 1 2│
│                 │3 4 5 3 4 5│
│                 │6 7 8 6 7 8│
└─────────────────┴───────────┘
\end{verbatim}

The two-argument \verb|⍴| function flattens the right argument array,
and reshapes it to the shape of its left argument. If the right argument has 
more elements than is required by the shape, the remaining elements are
ignored. If there are fewer elements, array elements are replicated:
\begin{verbatim}
      (2 2⍴w) (3 3⍴1 2) (9⍴w)
┌───┬─────┬─────────────────┐
│0 1│1 2 1│0 1 2 3 4 5 6 7 8│
│2 3│2 1 2│                 │
│   │1 2 1│                 │
└───┴─────┴─────────────────┘
\end{verbatim}
A one-argument version of \verb|⍳| creates an array of the shape
provided by its argument, in which a given element contain the value of the
index of that element:
\begin{verbatim}
       (⍳5)(⍳2 2)(⍳2 1 1)
┌─────────┬─────────┬───────┐
│0 1 2 3 4│┌───┬───┐│┌─────┐│
│         ││0 0│0 1│││0 0 0││
│         │├───┼───┤│└─────┘│
│         ││1 0│1 1││┌─────┐│
│         │└───┴───┘││1 0 0││
│         │         │└─────┘│
└─────────┴─────────┴───────┘
\end{verbatim}


\paragraph{Higher-order Functions}
Second-order functions in APL accept first-order ones as their arguments.
A simple example is the function composition operator \verb|∘|.  It acts in the
usual way: \verb|(f∘g) e| is the same as \verb|f(g e)|.  Composition can be
also used for partial applications of two argument functions.  We compose
the argument with the function either on the right or on the left:
\begin{verbatim}
      ((÷∘4) 2) ((⍴∘1) 2 2)
┌───┬───┐
│0.5│1 1│
│   │1 1│
└───┴───┘
\end{verbatim}
Composition is often useful because it can save parentheses.  Consider a long
expression, \eg{} \verb|,w+3×2|, that we are want to divide by four. 
Instead of writing \verb|(,w+3×2)÷4| we can write \verb|÷∘4,w+3×2|.
Since element concatenation has a higher priority than the function application,
%\todo{We never defined element catenation nor priority.}
the \verb|+∘3 4| expression would not work as one might expect.  Instead of
doing \verb|(+∘3) 4| which evaluates to 7, APL does \verb|+∘(3 4)| which
produces a one-argument function that adds \verb|3 4| to its argument.
To avoid extra parentheses, it is convenient to use an identity function, such
as \verb|⊢|, to inhibit implicit concatenation:
\begin{verbatim}
      +∘3 ⊢ 4
7
\end{verbatim}
This has some similarity with the \verb|$| operator in Haskell or \verb|@@|
in Ocaml.

The reduce operator \verb|f/| (slash) inserts the two-argument function \verb|f|
among the elements of the argument array, on the last axis:
\begin{verbatim}
      (+/w) (×/1 2 3) (+/1 2 3)
┌───────┬─┬─┐
│3 12 21│6│6│
└───────┴─┴─┘
\end{verbatim}
If the argument array is a single element, then the result is that
element.  For empty vectors, \verb|+/| and \verb|×/| produce 0 and 1
correspondingly, but for an arbitrary function, reducing an empty array
is an error.

We use two \emph{element-wise}-like operators: each \verb|¨| and
rank \verb|⍤|~\cite{RBernecky:funrank,RBernecky:apex}.
The \verb|f¨| applies \verb|f| to each element of the argument array and
in case \verb|f| evaluates to a non-scalar result, a nested array is produced.
For example:
\begin{verbatim}
      (⍳¨2 3) (1∘+¨2 2⍴⍳4)
┌───────────┬───┐
│┌───┬─────┐│1 2│
││0 1│0 1 2││3 4│
│└───┴─────┘│   │
└───────────┴───┘
\end{verbatim}

The rank conjunction operator \verb|f⍤n| is a second-order function
that applies \verb|f| to the sub-arrays of the \verb|n| innermost
dimensions of the argument array. When results of \verb|f| applications
have the same shape \verb|s|, but are non-scalar, the shape of the result
is the concatenation of the argument array shape and \verb|s|.
For example:
\begin{verbatim}
      ⍴2 2∘⍴⍤0⊢2 3
2 2 2
\end{verbatim}
Here, we apply \verb|2 2∘⍴| to the elements of a two-element vector \verb|2 3|,
obtaining two $2 \times 2$ arrays, called \emph{cells}, 
that are combined (laminated in APL lingo) into an array of shape
\verb|2 2 2|. When cell result shapes of \verb|f| are non-uniform,
the largest shape is taken, and all the other arrays are padded with zeroes at
the end of each axis. In our examples, this situation does not arise.

Both operators allow \verb|f| to be a two-argument function, 
in which case the derived operator requires argument arrays 
on the right and on the left.
For the case of the rank operator, we also have to specify 
two ranks: for the left and right arguments.  For example:
\begin{verbatim}
      (2 3⍴¨3 4) ((2 2⍴2)⍴⍤(1 0)⊢2 3)
┌───────────┬───┐
│┌───┬─────┐│2 2│
││3 3│4 4 4││2 2│
│└───┴─────┘│   │
│           │3 3│
│           │3 3│
└───────────┴───┘
\end{verbatim}
The second expression is of shape \verb|2 2 2|, where the first \verb|2 2|
sub-array consists of 2s and the second one of 3s.

\paragraph{Defining functions}
A function is defined using the \verb|{}| block that surrounds the body
text. For example, a constant function that always produces 42 can
be defined as \verb|{42}|. 
Function arguments are bound to variables \verb|⍺| and
\verb|⍵| for the left and right arguments, respectively.
For example, \verb|{⍵ + 1} 5| evaluates to \verb|6|.  A function can have
local variable bindings using the assignment operator \verb|←|.  Multiple
statements can be separated either by the \verb|⋄| operator or a line break.
For example, the following function computes the number of elements in the
array:
\begin{verbatim}
      {s←⍴⍵ ⋄ p←×/s ⋄ p} w
9
\end{verbatim}
The result of a function is the first statement that is not
given a name. Often, as in the above, this is its last statement.

As we have mentioned before, we elided details about
operator semantics that are not relevant to understanding the
rest of the paper.  For precise specification, please refer
to~\cite{dyalog:ReferenceManual170}.

\paragraph{Variable names} In the rest of the paper, we use variable names with
unicode symbols.  For example: \verb|k¹|, \verb|C²|, \verb|δŷ|, \etc{}  Even
though this is not supported by current APL interpreters, 
it may help to relate the code with its mathematical formulation,
and thereby increase the overall readability.

%
% \input{literateprogramming}
%
% \subsection{\label{algorithmiccommunication}Algorithmic Communication}
%
% communication of algorithms to others --- education,
% pedantic, you bet your company applications
%
%
% absence of performance tuning measures,
% memory management, message passing, etc., that
% obscure algorithmic clarity.
%
% code volume --- line and character count
%
% number of functions
%
% near-total absence of cyclomatic complexity --- each
% function/verb appears to the user as a basic block.
%


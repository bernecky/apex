\section{\label{sec:performance}Performance}

Although our paper focuses on APL's productivity as a specification language,
it is enlightening to compare execution times of our APL-based CNN
implementation against ones that use state of the art frameworks.

\paragraph{Experimental Setup}%

We conducted timing tests on a 16GB Intel i7{-}6700HQ CPU, at
2.60GHz running Gentoo Linux, kernel version 5.0.0.
We used Dyalog APL versions 16.0.32742 and 17.0.35814,
\sac{} compiler version 1.3.3-Mi\-jas\-Costa-334-geedc7, and
\tf{} version 1.13.0.
We present arithmetic means of timing for system initialisation,
training 1000 images, updating weights at every image,
and recognising 10000 images.

\paragraph{Experimental Result Analysis}%
\label{sec:experimentalresults}

The elapsed times for our CNN performance measurements
are given in Figure~\ref{perfresults}.
\begin{figure}[h]
\begin{tabular}{lccc}
\toprule
Environment     & Init (\textit{s}) & Train $10^3$ (\textit{s}) & Test $10^4$ (\textit{s})  \\
\midrule
\sc{APL}        & 0.3  &  16.3 & 33.6 \\
\tf{}           & 6.0  &  0.85 &  6.3 \\
\sac{}          & 0.4  &  0.65 &  2.2 \\

\bottomrule
\end{tabular}
\caption{Elapsed times (seconds) for CNN benchmark}%
\label{perfresults}
\end{figure}

As can be seen, in comparison to \tf{}, the APL version is
about 20 times slower when training and about 5 times slower when
recognising the images.  APL's initialisation is fast,
as it reads four input files, the largest of which
is the 60000 training images, occupying about 47MB\@. \tf{},
by contrast, takes much longer to initialise, although
this rarely presents a problem when running real-world neural networks,
because long training times dominate execution time.

The execution time difference is unfortunate, and this may
be part of the price for concise framework-less specification.
Historically, the highly interactive nature of APL interpreters
has limited the scope of performance improvements, as syntax
classes can change underfoot during execution:
\eg{} verbs can become nouns, or vice versa.
Secondly, generation of large number of nested arrays drags APL
performance down by orders of magnitude, due to the load
placed on the array memory subsystem. % As opposed to hardware memory
Finally, APL interpreters typically do not perform simple optimizations,
such as code motion to lift loop-invariant expressions,
because of a chosen interpreter design decision to allow
interruption and continuation of evaluation, an interactive
session feature that few other systems support.
The absence of special treatment of the convolution operator
and lack of just in-time compilation also contribute
to poor performance.
% Who cares?
% We expect Dyalog to deal with the former shortcoming soon.

At this stage, we could say that APL is nothing but a prototyping
language, but the \sac{}~\cite{sac} runtime brings us hope.  The \sac{} runtime
reflects the performance of our hand-translated version of
the APL code into \sac{}, a functional array-oriented language for
high-performance. It turns out that a very similar specification
can bridge the performance gap between APL and \tf{}, with APL
potentially even outperforming the latter.
All that remains to do is to automate translation of APL into
a compiled language like \sac{}.

Our initial attempts to use the APEX compiler~\cite{RBernecky:apex}
were not successful, as APEX did not support
dfns and nested arrays. %, as well as lack of communication
%among the authors at the inception of this project.
The current version of APEX includes a simple Dfn-to-TradFn converter,
as well as support for stranded function arguments and results.
Work to replace remaining uses of nested arrays in
the CNN code is underway as of this writing, opening 
up immediate perspectives for future work.
As an added bonus, when generating \sac{}
code, we can immediately obtain parallel execution on multi-core
architectures, as well as GPUs, by simply providing a flag to a
compiler.


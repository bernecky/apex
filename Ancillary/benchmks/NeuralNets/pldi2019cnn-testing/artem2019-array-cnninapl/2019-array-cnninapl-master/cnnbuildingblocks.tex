\section{CNN Building Blocks}%
\label{sec:cnnbuildingblocks}

\begin{figure*}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{CNN.png}
    \caption{\label{fig:cnn}CNN for digit recognition. \textit{The picture is taken
        from~\cite{zhang2016derivation}}}
\end{figure*}

This paper demonstrates the use of a functional subset of
APL to solve a classical hand-written digit recognition problem, 
using Zhang's algorithm, as informally presented in 
Fig.~\ref{fig:cnn}, with input data from the widely used MNIST data 
set~\footnote{See 
\url{http://yann.lecun.com/exdb/mnist/}.}.
The Zhang paper contains a formal mathematical specification of
the algorithm; we present that specification as a small
set of defined APL functions, striving for a good
balance between conciseness and efficiency.
The full version of this code is freely available at
\url{https://github.com/ashinkarov/cnn-in-apl}.

\paragraph{Convolution}

The first layer $C_1$, computes six convolutions of the $28 \times 28$
input image $I$ with $5 \times 5$ matrices of weights $k^1_{1,i}$.  Each
convolution computes a weighted sum at every position of $I$ where $k^1_{1,i}$
could be placed atop without truncation.  This produces six $24 \times 24$
arrays.

Consider a single convolution over a 2d image $I$, with weights
$w$. In order to facilitate such computations, Dyalog APL recently
(Version~16) introduced a new operator, \emph{stencil}, denoted as \verb|⌺|,
whose semantics prescribe that the array is padded with zeroes, 
and then a user-defined convolution is applied to a sliding window
across the padded array, for example:

\begin{verbatim}
      {⊂⍵}⌺(3 3)⊢3 3⍴1
┌─────┬─────┬─────┐
│0 0 0│0 0 0│0 0 0│
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
├─────┼─────┼─────┤
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
├─────┼─────┼─────┤
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
│0 0 0│0 0 0│0 0 0│
└─────┴─────┴─────┘
\end{verbatim}
The algorithm applies stencil to the argument $3 \times 3$ array of 1-s
\verb|3 3⍴1|, with a sliding window of size \verb|3 3|,
computing the enclose \verb|{⊂⍵}| of each such subarray.
The zeroes at the non-central cells comprise the padding that will be
dropped, as it is not relevant to this algorithm, resulting 
in the following code:
\begin{verbatim}
    t ← ⌊÷∘2(⍴w)-~2|⍴w
    (-t)↓t↓({+/,w×⍵}⌺(⍴w)⊢I)
\end{verbatim}
The variable $t$ is the number of elements that have to be dropped.
Per \verb|⌺| semantics, one is subtracted from 
the even elements of the weight vector's shape,
and that result is halved: padding is added at the beginning and end
of each axis. The padding is dropped this way:
\verb|(-t)↓t↓| --- drop $t$ from the end and drop
$t$ from the beginning.  Finally, \verb|⌺(⍴w)⊢I| slides a window,
 of shape identical to $w$, over the array $I$ and,
at each such position, applies \verb|{+/,w×⍵}|, 
to compute the sum of the element-wise multiplication
of $w$ with the corresponding subarray of $I$.

%\fixme{This version might be a bit easier to explain:
%\texttt{⍝ conv←\{a←⍵ ⋄ s←1+\,(⍴a)-⍴⍺ ⋄ ⊃+/,⍺×\{s↑⍵↓a\}¨⍳⍴⍺\} }}

Dyalog's use of padding introduces significant overhead when $w$ 
is large, as it spends time computing result elements
that must immediately be discarded.  Also,
as \verb|⌺| is Dyalog-specific and not portable to
other APL implementations, we present an alternative formulation:
\begin{verbatim}
    s ← 1+(⍴I)-⍴w
    ⊃+/,w×(⍳⍴w){s↑⍺↓⍵}¨⊂I
\end{verbatim}
The shape of the resulting array, $s$, is
1 plus the element-wise difference of the shapes of $I$ and $w$. We
assume that dimensions of $I$ and $w$ are the same. 
If we are computing a three-element convolution over an
$n$-element vector $a$ with weights $b_0\ b_1\ b_2$,
the elements of $r$ would be:
$\mathop{\forall}\limits_{i=0}^{n-2}r[i] = a[i]\times b_0 + a[i+1] \times b_1 +
a[i+2]\times b_2$.  Note that $i$ goes over the indices of $r$, not $a$.
All array languages support element-wise operation on arrays, allowing
$\forall i\,.\,c[i] = a[i] + b[i]$ to be written as
$a = b + c$, if $a$, $b$, $c$ are of the same shape.  Hence, we can write
$r = a_0\times b_0 + a_1\times b_1 + a_2 \times b_2$ where
$a_0 = \mathop{\forall}\limits_{i=0}^{n-2}a[i]$,
$a_1 = \mathop{\forall}\limits_{i=0}^{n-2}a[i+1]$, and
$a_2 = \mathop{\forall}\limits_{i=0}^{n-2}a[i+2]$.
This simply means that $a_k = \texttt{(n-2)↑k↓a}$.
The indices of $b$ determine how many elements need to be dropped.
Generalizing this to an arbitrary number of dimensions produces
\verb|(⍳⍴w)|, which generates a nested array of the same shape as $w$, 
in which the elements are indices of $w$, along the lines
of what was done in \sac{}~\cite{SBScholz:sac} and MoA~\cite{LMRMullin:moa}.

For every such index, apply \verb|{s↑⍺↓⍵}|,
where the left argument is the index and the right argument is the enclose of
$I$, with the help of the \emph{each} operator, \verb|¨|. 
This produces an array of shape identical to the shape of $w$, 
in which each element is the shifted array $I$. 
Each such shifted array is multiplied with the corresponding weight,
and that result is summed, then disclosed, to form an element of
the final result.
When we substitute $w$ and $I$ by formal parameters, and abstract
the above expressions as a function, we get:
\begin{verbatim}
conv←{s←1+(⍴⍵)-⍴⍺⋄⊃+/,⍺×(⍳⍴⍺){s↑⍺↓⍵}¨⊂⍵}
\end{verbatim}

This formulation of convolution is rank and shape polymorphic, as long
as the ranks of $w$ and $I$ are the same.

\paragraph{Multiple Convolutions}%
\label{par:multiple_convolutions}

One way to apply \verb|conv| 6 times over $I$ would be to
introduce 6 $k^1_1$ variables, and apply \verb|conv| 6 times. 
However, a more elegant and generic way is to treat $k^1$ as a
3d array of shape $6\ 5\ 5$, in which case
we can use the rank operator \verb|⍤|, to apply \verb|conv|
to every subarray on $n$ innermost dimensions.  For example:
\verb|f⍤2⊢k¹| would apply \verb|f| to every $5\ 5$ subarray in $k^1$.  Given
that \verb|f| always produces a result of shape $s$, the shape of
\verb|f⍤2⊢k¹| is the catenation of $6$ and $s$.

$C_1$ prescribes adding a bias to the result of every
convolution, so we can use the same pattern: construct $b^1$
as a 6-element vector, and use the rank operator to add each bias to the
corresponding convolution. The semantics of the rank operator allows us to
combine these steps, as follows:
\begin{verbatim}
b¹ {⍺+⍵ conv I}⍤(0 2) ⊢k¹
\end{verbatim}
For two-argument functions on the left-hand side of the \verb|⍤| we may
specify two ranks, one for the left argument and for 
the right one. In the above example, we add each 
scalar from \verb|b¹| to every 2d subarray from \verb|k¹|.
If we generalize this by not hard-coding the number \verb|2|, which
happens to be the rank of $I$, we obtain a rank- and shape-polymorphic
function, which may be usable in other contexts:
\begin{verbatim}
multiconv←{(I k¹ b¹)←⍵⋄b¹{⍺+⍵ conv I}⍤(0,(⍴⍴I))⊢k¹}
\end{verbatim}
The three arguments \verb|I k¹ b¹| are passed into the function
in a nested array, as a right argument.

\paragraph{Activation Functions}%
\label{par:activation_functions}
The last step in $C_1$ applies the sigmoid (a.k.a standard logistic) activation
function to all values.  Sigmoid of $x$ is defined as $\frac{1}{1-e^x}$.
Since APL defines the unary cases of the
arithmetic functions \verb|÷| and \verb|*| as 
$\frac{1}{x}$ and $e^x$, respectively (where $e$ is the 
base of the natural logarithms), we define the activation function as:
\begin{verbatim}
logistic←{÷1+*-⍵}
\end{verbatim}
These arithmetic expressions are applicable both to scalars and arrays, so
\verb|logistic| can be immediately applied to arrays as well, resulting in a
shape-preserving element-wise application of the scalar version of
\verb|logistic|.
%\fixme{IMnotsoHO, viewing logistic as a "scalar version" is misguided,
%in an array language.  It's an array function with scalars being special}

\paragraph{Average Pooling}%
\label{par:average_pooling}

The average pooling layer $S_1$ computes the following function for every
image $C^1_i$:
\[
    \mathop{\forall}_{u\ v=0\ 0}^{12\ 12}S^1_i[u\ v]
    = \frac{1}{4}\sum_{m\ n=0\ 0}^{2\ 2}C^1_i[2u{+}m\ \ 2v{+}n]
\]
That is, split $C^1_i$ into $2 \times 2$ non-overlapping
blocks, and average the elements in each block.
A direct translation of the above formulation into APL would involve
array indexing, which is frowned upon in APL dialects, as it
is generally inefficient in an interpretive environment.
%is harder than array expressions to optimize in a compiled
%environment, and is almost certainly a far less elegant 
%expression of an algorithm. It almost certainly is far more
%verbose, too, which interferes with one's ability to comprehend
%the algorithm and its correctness.
% XXX(artem) the above statements are bogus!  The indexing version
%            is as short as the replicate-based one.  What the hell?
The result shape 
is the argument shape divided by \verb|2|.
For each index in this array, the average 
of the corresponding elements is:
\begin{verbatim}
{avg c[(⊂iv) (⊂iv+0 1) (⊂iv+1 0) (⊂iv+1 1)]}¨⍳(÷∘2⍴c)
\end{verbatim}
where \verb|avg| can be defined as the APL train:
\begin{verbatim}
avg ← {(+/÷≢),⍵}
\end{verbatim}
which is a shorthand, tacit way to express 
\verb|(+/,⍵)÷(≢,⍵)|, in which the left hand side of the
division sums all the elements of \verb|⍵| in the 
raveled (flattened) argument array, and the
right hand side is the number of elements in the argument array.

One way to simplify the generation of these 4 indices is:
\begin{verbatim}
{avg c[(⊂2×⍵)+⍳2 2]}¨⍳(÷∘2⍴c)
\end{verbatim}
This, unfortunately, still generates and uses index vectors,
so we shall explore index-free formulations.  First, we observe that
the split into a grid of $2 \times 2$ with further averaging is a stencil
operation, where the sliding window moves 2 elements along each axis.
This pattern can be expressed with the Dyalog APL stencil operator \verb|⌺|,
whereby non-unit movements of the sliding window can be expressed 
using two-row matrix, in which the first row is the shape of 
the sliding window, and the second row is the step. 
In our case, both rows in the matrix are $2\ 2$. According to the
padding formula \verb/⌊÷∘2(⍴w)-~2|⍴w/, the stencil of size $2 \times 2$ does not
create any padding, so we can formulate average pooling as:
\begin{verbatim}
avgpool ← {÷∘4{+/,⍵}⌺(2 2⍴2)⍤2⊢⍵}
\end{verbatim}
That is, for every array in the last two dimensions \verb|⍤2|, apply the
stencil operation with sliding window of shape $2 \times 2$, moving
the window 2 elements along every dimension \verb|⌺(2 2⍴2)|, 
with the stencil operation that sums all the 4 
elements \verb|{+/,⍵}|, dividing that sum by 4.
Despite its conciseness, this function uses the Dyalog-specific operator, and
it is specific to 4-element average pooling.

We could also do the splitting of a 2d array \verb|c| this way:
\begin{verbatim}
(x y) ← ⍴c
(x÷2) (y÷2) 2 2 ⍴ ⍉⍤2 ⊢(x÷2) 2 y ⍴ c
\end{verbatim}
That is, cut the matrix across the first axis in 2-row chunks
\verb|(x÷2) 2 y ⍴ c|. 
This takes a 2d array of shape $x\ y$, producing a 3d array 
of shape $\frac{x}{2}\ 2\ y$, then transposes the last two axes:
\verb|⍉⍤2|. This gives a result of shape $\frac{x}{2}\ y\ 2$, 
which can be cut along the second axis by a simple 
reshape \texttt{(x÷2)\ (y÷2)\ 2\ 2\ ⍴}. Then, we simply
apply \verb|avg| on the last two axes. Finally, this
operation itself has to be applied rank-2, as we want to pool the last
two dimensions:
\begin{verbatim}
avgpool ← {
    (x y) ← ⍴⍵
    avg⍤2 ⊢(x÷2)(y÷2)2 2 ⍴ ⍉⍤2 ⊢(x÷2)2y ⍴ ⍵
}⍤2
\end{verbatim}


\paragraph{$C_2$ and $FC$ Layers}%
\label{par:_c_2_layer}
Since we represented $s$ as a 3d
array of shape $[6,12,12]$, 
the rank-polymorphic specification of \texttt{multiconv}
is immediately applicable here.  Intuitively, if $s$ is a single entity,
then all the left hand sides 
of the arrows from $S^1$ to $C^2$ in Fig.~\ref{fig:cnn}
merge into a single point, akin to the first convolution.  The
argument to \texttt{conv} is of shape $6\ 12\ 12$, and
each weight $k^2_{i,*}$ is of shape
$6\ 5\ 5$, so the result is of shape $1\ 8\ 8$.
We have 12 $k^2_{i,*}$ and 12 biases, 
so the result shape of \texttt{multiconv} is of shape $12\ 1\ 8\ 8$.

A fully connected layer $FC$ is just a convolution
with a weight identical to the shape of the argument array, so
we can use \texttt{multiconv} again. 
Without additional reshapes, the shape of the
layer $S^2$ would be $12\ 1\ 4\ 4$. However, as we want to compute 10
weighted sums of all the elements,
$W$ becomes of shape $10\ 12\ 1\ 4\ 4$, yielding
a result of shape $10\ 1\ 1\ 1\ 1$ from \texttt{multiconv}.

\paragraph{Forward Pass}
At this point, we have enough building blocks to define a ``forward path''
of the network, \ie{} Fig.~\ref{fig:cnn}, which 
generates a 10-element vector of probabilities from an image $I$,
with fixed weights $k^i$ and $W$, and biases $b^i$ and $b$.
\begin{verbatim}
forward ← {
  (I k¹ b¹ k² b² W b) ← ⍵
  C¹ ← logistic multiconv I  k¹ b¹
  S¹ ← avgpool C¹
  C² ← logistic multiconv S¹ k² b²
  S² ← avgpool C²
  ŷ  ← logistic multiconv S² W  b
}
\end{verbatim}
Each layer is a single multi-dimensional array, and
that we compute the $FC$ layer directly from $S^2$.
The final answer is the index of $\hat{y}$ with the largest value:
\begin{verbatim}
{⍵⍳⌈/⍵},ŷ
\end{verbatim}
\Ie{} for the raveled \verb|ŷ|, find the maximum element \verb|⌈/⍵|,
and find the index where this element occurs for the first time \verb|⍵⍳|.
The \verb|⌈/⍵| produces the two-argument (dyadic) maximum function
\verb|⌈| over the array, and the two-argument version of \verb|⍳| with the array
on the left and the element on the right returns the first occurrence of this
element in that array, which we abstract into this function:
\begin{verbatim}
maxpos ← {(,⍵)⍳⌈/,⍵}
\end{verbatim}

\subsection{Gradient Descent}
Training of the network entails propagation of the
recognition error back into weights and biases, which we
do using \emph{stochastic gradient descent}.  The overall idea
is to consider $\hat{y}$ as a function over weights and biases.  The objective
function we want to minimise is: $o = \frac{1}{2}\sum_i{(\hat{y}_i - y_i)}^2$,
where $y$ is the correct answer, \ie{} a 10 element vector with value 1
at the position that corresponds to the digit depicted in $I$ and zeroes
at all the other positions.  Due to the linear nature of the network, and
carefully chosen activation functions, computation of partial derivatives
makes a great use of the chain rule.  For example, the derivative of our
objective function over some weight $w$ would be:
\[
    \frac{\partial o}{\partial w}
    = \sum_i\frac{\partial o}{\partial \hat{y}_i}
      \cdot\frac{\partial \hat{y}_j}{\partial w}
    = \sum_i(\hat{y}_i - y_i)\frac{\partial\hat{y}_i}{\partial w}
\]
The chain rule can then be applied again, allowing
us to ``reverse'' each layer and reconnect them by inverting the
arrows in Fig.~\ref{fig:cnn}. For precise mathematical details,
please refer to the Section 2 of~\cite{zhang2016derivation}. In the rest
of this section, we focus on the implementation of these back layers.

\paragraph{Mean Squared Error}
Our objective is the sum of squared differences divided by two.
This can written as a direct translation of the above sentence:
\begin{verbatim}
meansqerr ← {÷∘2+/,(⍺-⍵)*2}
\end{verbatim}
Note that we ravel (flatten) \verb|(⍺-⍵)*2| to 
produce a 10-element vector from \verb|ŷ|, obtaining a
scalar (zero-dimensional array) as a result of sum-reduction.

\paragraph{Back Sigmoid}
The derivative of the sigmoid function is:
\[
    \frac{\partial}{\partial x}\sigma(x) = \sigma(x)(1 - \sigma(x))
\]
Since our sigmoid functions are always ``between''
the layers, the derivative of sigmoid will be always in a larger
derivative chain.  Therefore, for convenience, we add a multiplier
for that part of the chain:
\begin{verbatim}
blog ← {⍺×⍵×1-⍵}
\end{verbatim}

\paragraph{Back Average Pooling}
If 2d array $s$ is the average pooling of $t$, then each $s_i$ is defined as
$\frac{1}{4}(t_i+t_j+t_k+t_l)$ for some indices $j,k,l$.  Any partial
derivative of $s_i$ over $t_i$ is simply $\frac{1}{4}$.  Therefore,
backpropagation through the average pooling layer can be computed as:
$\Delta t[i\ j] = \frac{1}{4}\Delta s[\lfloor i/2\rfloor\ \lfloor j/2\rfloor]$.
Higher-rank arguments apply this expression on the innermost 
2d planes, elegantly expressed in APL as:
\begin{verbatim}
backavgpool ← {2⌿2/⍵÷4}⍤2
\end{verbatim}
This reads: for a given argument array \verb|⍵|, divide every element
by 4, replicate every column twice (\verb|2/|), and then
replicate each row twice (\verb|2⌿|). Finally, apply this function rank-2.
\verb|backavgpool| hardcodes the size of the pooling, but
we can, in principle, provide a left argument of pool size to
give a generalized pooling, \eg{}
\begin{verbatim}
{(x y)←⍺ ⋄ {x⌿y/⍵÷x×y}⍤2 ⊢⍵}
\end{verbatim}


\paragraph{Back convolution}
The linear nature of the convolution implies that the derivatives are constants,
namely the input of the convolution itself.  Hence, we can approximate
the error in the weights as a convolution of the input with the error.
So, there is no need for a separate function.

The error of the bias is a sum of the error we are propagating from the
previous stages of the chain, since the derivative of the bias is constant 1.
\begin{verbatim}
backbias ← {+/,⍵}
\end{verbatim}
This function is so trivial that we consider it nugatory,
as its inline definition, at three characters, is shorter than
its name, and the meaning of the expression is immediately evident.
We keep the definition as it represents a logical step in the algorithm.

To compute the propagation of the error into the input of the
convolution, note that the derivatives of the convolution with respect to the input
are simply weights, with some care taken to describe the index relation
between the value we are propagating and the index of the input that it affects.
Consider the following example:
\begin{verbatim}
      in w d
┌───────────┬───┬─────┐
│0.5 1 1.5 2│1 2│1 2 3│
│2.5 3 3.5 4│3 4│4 5 6│
│4.5 5 5.5 6│   │7 8 9│
│6.5 7 7.5 8│   │     │
└───────────┴───┴─────┘
\end{verbatim}
This shows three arrays: \verb|in| is the argument. Its
values are not relevant, but they are useful in talking
about positions; \verb|w| are weights and \verb|d| is the
propagated error.  The convolution is performed in reverse, to
determine which weights contribute to which positions. 
It is clear that weight \verb|1| contributes to the positions 
of \verb|in| from \verb|0.5| to \verb|5.5|, 
as multiplication of any other element with the weight 1 implies out
of bound access.  By the same reasoning, weight 2 contributes to positions from
\verb|1| to \verb|6|; weight 3 from \verb|2.5| till \verb|7.5| and weight 4 from
\verb|3| till \verb|8|. This pattern can be expressed using the concept of
overtake: a take of more elements than there are in the array pads
the result with zeroes:
%\todo{We could define till this way: till←{⍺+⍳1÷|⍵-⍺}}
\begin{verbatim}
       5 ↑ 1 2 3
1 2 3 0 0
      ¯5 ↑ 1 2 3
0 0 1 2 3
\end{verbatim}
%\fixme{The explanation in the above paragraph could be clearer.
%I think it mixes algorithm with implementation.}
This means that we can overtake \verb|d| at every weight index in the following
way:
\begin{verbatim}
      ,w{(⍴in)↑(-⍵+⍴d)↑d}¨⍳⍴w
┌───────┬───────┬───────┬───────┐
│1 2 3 0│0 1 2 3│0 0 0 0│0 0 0 0│
│4 5 6 0│0 4 5 6│1 2 3 0│0 1 2 3│
│7 8 9 0│0 7 8 9│4 5 6 0│0 4 5 6│
│0 0 0 0│0 0 0 0│7 8 9 0│0 7 8 9│
└───────┴───────┴───────┴───────┘
\end{verbatim}
\Ie{}, pad with zeroes on the top/left with \verb|(-⍵+⍴d)↑|,
then trim the right/bottom with \verb|(⍴in)↑|, then
multiply shifts by the corresponding weight and sum that result:
\begin{verbatim}
backin ← {(d w in)←⍵ ⋄ ⊃+/,w{(⍴in)↑(-⍵+⍴d)↑⍺×d}¨⍳⍴w}
\end{verbatim}

Finally, the above three steps are combined to create a function
that propagates weight-, bias-, and input-errors:
\begin{verbatim}
bmconv ← {
  (δo ws in bs) ← ⍵
    d ← ⍴⍴in
  δin ← +⌿δo {backin ⍺ ⍵ in} ⍤(d, d) ⊢ ws
  δws ← {⍵ conv in} ⍤d ⊢ δo
  δbs ← backbias ⍤d ⊢ δo
  δin δws δbs
}
\end{verbatim}
As can be seen, this is mainly bookkeeping.  We apply \verb|backin|
rank-\verb|d|, then sum along the leading axis. This builds on the
assumption that convolution evaluates a vector of outputs. 
If rank increases, the enclose/disclose approach can be used to sum
the subarrays.  Back-weights and back biases are simply applications
of the corresponding functions using rank.
Putting all the basic blocks together, we have:
\begin{verbatim}
train ← {
  (I y k¹ b¹ k² b² W  b) ← ⍵
             C¹ ← logistic multiconv I  k¹ b¹
             S¹ ← avgpool C¹
             C² ← logistic multiconv S¹ k² b²
             S² ← avgpool C²
              ŷ ← logistic multiconv S² W  b
             δŷ ← ŷ - y
              e ← ŷ meansqerr y
    (δS² δW δb) ← bmconv (δŷ  blog  y) W  s² b
            δC² ← backavgpool δS²
  (δS¹ δk² δb²) ← bmconv (δC² blog C²) k² S¹ b²
            δC¹ ← backavgpool δS¹
    (_ δk¹ δb¹) ← bmconv (δC¹ blog C¹) k¹ I  b¹
  δk¹ δb¹ δk² δb² δW δb e
}
\end{verbatim}





% Reports of APL-based convolution on higher-rank arrays are, oddly enough,
% largely absent from the literature.\fixme{Is it really?}
% Given this dearth of information, we decided to write our own
% convolution in Dyalog~APL\@. We quickly discarded the two most
% likely candidates for kernel functions, \emph{n-wise reduction},
% and \emph{stencil}. Dyalog's definition of reduction is vector-based, so effectively
% useless for our matrix work. Their stencil conjunction is, in our
% opinion, overly complicated, and we felt that the work required
% to eliminate shards --- partial subarrays --- from its
% result elements would outweigh any
% potential benefit from use of that built-in
% operation.~\footnote{We are going to propose an extension to that primitive which would, hopefully, resolve this difficulty, by not generating
% shards.}
%
% \fixme{How important is it to make convolution rank-polymorphic?}





\section{\label{introduction}Introduction}


Array languages in general, and APL in particular, are often acclaimed
to be powerful tools of thought, as suggested in
Kenneth E. Iverson's 1979 Turing Award Lecture~\cite{KEIverson:tot}.
APL's design, based on fundamental concepts of natural language, where data
are \emph{nouns} and operators such as multiply and iterate are
\emph{verbs} and \emph{conjunctions}\footnote{
    See \url{https://www.jsoftware.com/papers/APLDictionary.htm} for more
details.}, in combination with a language nucleus of a few pre-defined
operators, makes APL an easily accessible tool for domain experts,
% from areas such as finance, insurance, the arts, chemistry, geology,
without requiring a strong background in programming.
%We assume that the Workshop audience will be familiar
%with terminology used in Functional Programming, so have adopted that
%for this paper.

% The value of APL as a tool is typified by comment made by
% Dr.\ Stephen D. Jaffe, a Distinguished Scientific Advisor for Exxon-Mobil,
% now retired:
%
% \begin{quote}
% We hire mathematicians and engineers, not computer programmers.
% (APL) was the key in getting a lot of people competent in analyzing
% their own data. (Armed with APL), we automated all of our
% pilot plants --- roughly 100 of them, all doing simulation
%experiments. \emph{Stephen D. Jaffe, in \cite{NMargolis:jaffeapl}}
% \end{quote}

Problems requiring handling
large amounts of data with some structural regularity
are often ideally suited for quick programmatic solutions in APL\@.
One such area that has recently gained significant interest
is machine learning and neural networks.
Neural networks can be viewed as nested linear approximations
of unknown functions that are poorly understood analytically.
Calibrating these functions from given input-output pairs, also referred to as
``training'', boils down to iterating
% typically requires massive compute power, by performing
mathematically simple operations over large amounts of data.

%While such training and execution of neural networks
While these operations should be ideally suited for
array languages such as APL, %it nevertheless turns out that domain experts
%from
machine learning experts tend to use several domain-specific
frameworks, such as \tf{} or \pt{}, instead.
% % Many work years of development have gone into the creation of these frameworks.
% % The development and optimisation of these frameworks is an ongoing activity
% % that attracts a lot of contributions from academia and industry.
% % Consequently, the aforementioned frameworks have become so complex that their installation
% % as well as their use require a steep learning curve by domain experts in
% % machine learning, as witnessed by the many online-resources that are freely
% % available.\fixme{Can we provide a few example links here?}
% The non-trivial learning curve and difficulty in understanding
% the underlying structure of the computations, due to multi-million line
% code bases, seems to contradict the
% %This willingness of domain experts to use these frameworks seems to contradict the
% APL as ``tool for thought'' idea for domain experts.
% In this paper, we investigate this apparent paradox. We ask ourselves
% whether APL is suitable to express neural networks, and if so, does
% the complexity of the specification justify existence of several
% frameworks that are as large as \tf{}.
While there is no doubt that these frameworks are convenient to use,
any non-trivial extensions beyond the provided functionality will require
understanding the underlying codebase which is difficult due to its enormous
size and complex design.  This makes domain experts dependant on framework
specialists.

Following the APL as ``tool for thought'' idea, we investigate
how difficult would it be to define a sufficient number of \tf{}-like operators,
with a clear mathematical structure, in native APL, to build
%We tackle these questions by investigating a framework-free,
%executable specification of Convolutional Neural Networks (CNNs)
%that mirrors the mathematical structure of the algorithm.
%We use APL~\cite{KEIverson:apl,dyalog:ReferenceManual170}, a language with
%first-class array support, to implement
a state of the art
hand-written image recognition CNN~\cite{PalmRB:prediction,zhang2016derivation}.
First of all, to our knowledge this has not been done before.
Secondly, if the operators were simple to define, we can
ask ourselves, whether APL is a reasonable language to host
a shallowly-embedded DSL for machine learning.

APL offers a number of purely functional built-in array
operations\footnote{Although APL includes a number of imperative constructs,
we restricted ourselves to a functional subset.}:
they have no state and operate on immutable data. New verbs and conjunctions
can be expressed as compositions of the extant ones, built-in or
user-defined, producing concise, purely functional
data-parallel specifications that are immediately executable.

Our study shows that, for our CNN, the requisite operations
can be expressed in ten lines of native APL code, using just 22 built-in
verbs and conjunctions:

%\fixme{ This has been done before: The code is so short that you can actually print it on your t-shirt:}
\begin{verbatim}
blog←{⍺×⍵×1-⍵}
backbias←{+/,⍵}
logistic←{÷1+*-⍵}
maxpos←{(,⍵)⍳⌈/,⍵}
backavgpool←{2⌿2/⍵÷4}⍤2
meansqerr←{÷∘2+/,(⍺-⍵)*2}
avgpool←{÷∘4{+/,⍵}⌺(2 2⍴2)⍤2⊢⍵}
conv←{s←1+(⍴⍵)-⍴⍺⋄⊃+/,⍺×(⍳⍴⍺){s↑⍺↓⍵}¨⊂⍵}
backin←{(d w in)←⍵⋄⊃+/,w{(⍴in)↑(-⍵+⍴d)↑⍺×d}¨⍳⍴w}
multiconv←{(a ws bs)←⍵⋄bs{⍺+⍵ conv a}⍤(0,(⍴⍴a))⊢ws}
\end{verbatim}

Such brevity offers several advantages. First, it captures the
computational essence of the underlying problem, and
facilitates communication of the algorithm to colleagues.
The operators are also rank- and shape-polymorphic, making them
reusable in various contexts. By polymorphic, we mean that
they will operate, unchanged, on arrays of any rank or shape.

Second, our specification can easily be ported to non-APL
contexts. None of the built-in operators are neural-network specific, and
all of them have precise, well-defined behaviour.

Finally, the functional nature of our specification, with
element-wise data-parallel operations, presents
many opportunities for compiler technology
to generate efficient, parallel code, for CPU or GPU target systems,
with no APL source code changes.

The individual contributions of the paper are:
\begin{itemize}
    \item Framework-free, concise, executable specification of a CNN in APL
    \item Design exploration of the CNN and its implementation
    \item Brief run-time performance evaluation
\end{itemize}

The rest of the paper is organised as follow. We briefly introduce
CNNs and APL in Section~\ref{sec:background}.  We explore design trade-offs
of the new operators and how to combine them in
Section~\ref{sec:cnnbuildingblocks}.
We talk about performance considerations
and compiler optimisations in Section~\ref{sec:performance}, discuss related
work in Section~\ref{sec:related} and conclude in
Section~\ref{sec:conclusions}.


% A recent paper by two of the authors demonstrates the use
% of SaC~\cite{sac}, a high-performance functional array language,
% to implement a
% Convolutional Neural Network (CNN)~\cite{PalmRB:prediction,zhang2016derivation}
% for hand-written digit recognition, and compares that
% implementation to equivalent implementations written
% in \tf{} and \pt{}.
% Although their benchmark was limited to
% serial and multi-core operation, the authors obtained speedups
% of about three times on multi-core operations, over the other
% implementations. Furthermore, the entire SaC benchmark comprised
% only about 300 lines of native SaC code, compared with about two
% million lines of code for TensorFlow.
%
% Given these promising results, we decided to investigate the
% use of APL~\cite{KEIverson:apl,dyalog:apl17} for the same
% application, primarily to see if APL's expressive power
% would result in terser code than SaC, thereby easing
% communication of the algorithm to others, as well as
% giving us a better sense of algorithmic correctness.
% As a secondary goal, we investigated the relative performance of the
% APL and SaC implementations.
%
% The rationale for the study was our understanding that
% array operations, such as convolution and stencil application, are common
% in physics, artificial intelligence, neural nets, \etc{}
% The Functional Array Language APL provides excellent built-in support
% for many array functions, facilitating its use as a generic
% Domain-Specific Language (DSL) by physicists, computational
% chemists, actuaries, and data scientists.

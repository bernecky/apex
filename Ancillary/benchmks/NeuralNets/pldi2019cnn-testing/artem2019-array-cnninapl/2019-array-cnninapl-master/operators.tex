\section{CNN building blocks}%
\obsolete{replaced by cnnbuildingblocks.tex}
\label{sec:operators}

\begin{figure*}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{CNN.png}
    \caption{\label{fig:cnn}CNN for digit recognition. \textit{The picture is taken
        from~\cite{zhang2016derivation}}}
\end{figure*}

In this paper we we will use a classical hand-written digit recognition problem,
and we will use Zhang's design~\cite{zhang2016derivation} to solve this problem.
We use the widely used MNIST data set\footnote{See
\url{http://yann.lecun.com/exdb/mnist/}.} as input.  We use Fig.~\ref{fig:cnn} as
an intuitive specification. For precise mathematical formulation of the parts
of that picture refer to Section~1 of~\cite{zhang2016derivation}. We try to
derive as generic as possible operators for each layer. At the same time,
we strive for a good balance between conciseness and efficiency.

\paragraph{Convolution}

In the first layer $C_1$, we compute six convolutions of the $28 \times 28$
input image $I$ with $5 \times 5$ matrices of weights $k^1_{1,i}$.  Each
convolution computes a weighted sum at every position of $I$ where $k^1_{1,i}$
could be placed atop without truncation.  This results in six $24 \times 24$
arrays.

Let us first focus on a single convolution over the 2d image $I$ with weights
$w$.  As convolution is a very common pattern, the recent version of Dyalog APL
introduced the new operator called stencil, denoted as \verb|⌺|. Its semantics
prescribes that the array is first padded with zeroes, and then then convolution
is applied.  Consider the following example:
\begin{verbatim}
      {⊂⍵}⌺(3 3)⊣3 3⍴1
┌─────┬─────┬─────┐
│0 0 0│0 0 0│0 0 0│
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
├─────┼─────┼─────┤
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
├─────┼─────┼─────┤
│0 1 1│1 1 1│1 1 0│
│0 1 1│1 1 1│1 1 0│
│0 0 0│0 0 0│0 0 0│
└─────┴─────┴─────┘
\end{verbatim}
We apply stencil to the argument $3 \times 3$ array of 1-s
\verb|3 3⍴1|, with sliding window of size \verb|3 3|, and for
each sub-array we evaluate its enclose \verb|{⊂⍵}|.  Note the zeroes
at the non-central cells.  These are the added paddings that we will
have to be delete, resulting in the following code:
\begin{verbatim}
    t ← ⌊÷∘2(⍴w)-~2|⍴w
    (-t)↓t↓({+/,w×⍵}⌺(⍴w)⊣I)
\end{verbatim}
The variable $t$ tells us how much elements we need to delete.  Per \verb|⌺|
semantics, if any of the components of the shape weight vector is even,
we subtract one from this component, and then we divide the result by two.
As the paddings are added at the beginning of each axis as well as at the end,
their removal is expressed as \verb|(-t)↓t↓| --- take $t$ from the end and take
$t$ from the beginning.  Finally, \verb|⌺(⍴w)⊣I| slides the window of the shape
identical to $w$ over the array $I$ and at each such a position we apply
\verb|{+/,w×⍵}| that computes the sum of the component-wise multiplication
of $w$ with the corresponding sub-array of $I$.


\fixme{This version might be a bit easier to explain:
\texttt{⍝ conv←\{a←⍵ ⋄ s←1+\,(⍴a)-⍴⍺ ⋄ ⊃+/,⍺×\{s↑⍵↓a\}¨⍳⍴⍺\} }}

This function introduces a significant overhead when $w$ is large, as we
spend time computing something that will be immediately discarded.  Also
as \verb|⌺| is Dyalog-specific, here is an alternative formulation:
\begin{verbatim}
    s ← 1+(⍴I)-⍴w
    ⊃+/,w×(⍳⍴w){s↑⍺↓⍵}¨⊂I
\end{verbatim}
First, we compute $s$ which is the shape of the resulting array. It is a
component-wise difference of shapes of $I$ and $w$ plus 1.  Note that here
we assume that dimensions of $I$ and $w$ are the same.  Next, let us consider
the 1d case first.  If we are computing a three-element convolution over an
$n$-element vector $a$ with weights $b_0\ b_1\ b_2$.  In this case, the elements
of $r$ would be:
$\mathop{\forall}\limits_{i=0}^{n-2}r[i] = a[i]\times b_0 + a[i+1] \times b_1 +
a[i+2]\times b_2$.  Note that $i$ goes over the indices of $r$, not $a$.
Now, we can turn scalar operation of the form $\forall c[i] = a[i] + b[i]$ into
$a = b + c$, if $a$, $b$, $c$ are of the same shape.  Hence, we can write
$r = a_0\times b_0 + a_1\times b_1 + a_2 \times b_2$ where
$a_0 = \mathop{\forall}\limits_{i=0}^{n-2}a[i]$,
$a_1 = \mathop{\forall}\limits_{i=0}^{n-2}a[i+1]$, and
$a_2 = \mathop{\forall}\limits_{i=0}^{n-2}a[i+2]$.
This simply means that $a_k = \texttt{(n-2)↑k↓a}$.
Observe that indices of $b$ determine how much one needs to drop.  This can
be generalised to arbitrary number of dimensions, and this is exactly what we
do: \verb|(⍳⍴w)| generates a nested array of the same shape as $w$, where all
the elements are indices of $w$.  For every such index, we apply \verb|{s↑⍺↓⍵}|
where the left argument is the index and the right argument is the enclose of
$I$, with the help of the each operator \verb|¨|.  This results in an array of
shape identical to the shape of $w$, in which
each element is the shifted array $I$.  After that, we multiply each such a
shifted array with the corresponding weight, sum all the elements and disclose
the result.  If we substitute $w$ and $I$ with formal parameters and abstract
the above expressions in a function we get:
\begin{verbatim}
conv←{s←1+(⍴⍵)-⍴⍺⋄⊃+/,⍺×(⍳⍴⍺){s↑⍺↓⍵}¨⊂⍵}
\end{verbatim}

Note that this formulation of convolution is rank and shape polymorphic, as long
as dimensionality of $w$ and $I$ are the same.

\paragraph{Multiple Convolutions}%
\label{par:multiple_convolutions}

How do we apply \verb|conv| 6 times over $I$.  We can surely introduce 6
$k^1_1$ variables, and apply the function 6 times.  However, a more generic
way is to treat $k^1$ as a 3d array of shape $6\ 5\ 5$, in which case
we can use the rank operator \verb|⍤|, to apply a function
to every sub-array at $n$ innermost dimensions.  For example:
\verb|f⍤2⊣k¹| would apply \verb|f| to every $5\ 5$ sub-array in $k^1$.  Given
that \verb|f| always evaluates the result of shape $s$, the shape of
\verb|f⍤2⊣k¹| would be concatenation of $6$ and $s$.

$C_1$ prescribes to add biases to the result of every
convolution.  This means that we can use exactly the same pattern: store $b^1$
as a 6-element vector, and use the rank operator to add each bias to the
corresponding convolution. The semantics of the rank operator allows us to
combine these steps as follows:
\begin{verbatim}
b¹ {⍺+⍵ conv I}⍤(0 2) ⊣k¹
\end{verbatim}
For two-argument functions on the left-hand side of the \verb|⍤| we specify
two ranks: for the left argument and for the right one.  So we take every
scalar from \verb|b¹| and every 2d sub-array from \verb|k¹|.  Finally, there is
no need to hard-code this number 2 in the above expression, as it is simply the
dimensionality of the $I$.  After abstracting this into a rank- and shape-polymorphic
function we get:
\begin{verbatim}
multiconv←{(I k¹ b¹)←⍵⋄b¹{⍺+⍵ conv I}⍤(0,(⍴⍴I))⊣k¹}
\end{verbatim}
As we have to pass three arguments into a function, we assume that
they come in the form of a nested array as a right argument.


\paragraph{Activation Functions}%
\label{par:activation_functions}
The last step in $C_1$ applies the sigmoid (a.k.a standard logistic) activation
function to all values.  Sigmoid of $x$ is defined as $\frac{1}{1-e^x}$.  In APL,
arithmetic functions \verb|÷| and \verb|*| have a special meaning when they are
applied to a single argument: \verb|÷x| is defined as $\frac{1}{x}$ and
\verb|*x| is defined as $e^x$ where $e$ is the base of the natural logarithm.
Therefore, the activation function can be defined as:
\begin{verbatim}
logistic←{÷1+*-⍵}
\end{verbatim}
As the involved arithmetic expressions are applicable to scalars and arrays,
\verb|logistic| can be immediately applied to arrays as well, resulting in a
shape-preserving element-wise application of the scalar version of
\verb|logistic|.

\paragraph{Average Pooling}%
\label{par:average_pooling}

The average pooling layer $S_1$ computes the following function for every
image $C^1_i$:
\[
    \mathop{\forall}_{u\ v=0\ 0}^{12\ 12}S^1_i[u\ v]
    = \frac{1}{4}\sum_{m\ n=0\ 0}^{2\ 2}C^1_i[2u{+}m\ \ 2v{+}n]
\]
That is, we split $C^1_i$ into $2 \times 2$ blocks and average each block.
The above formulation can be directly translated into APL but it would involve
indexing, which according to all APL mantras are considered harmful.
We can compute the shape of the result, which is just the shape of the
input divided by 2.  Then, for each index in this array we can compute
an average of the corresponding elements:
\begin{verbatim}
{avg c[(⊂iv) (⊂iv+0 1) (⊂iv+1 0) (⊂iv+1 1)]}¨⍳(÷∘2⍴c)
\end{verbatim}
where \verb|avg| can be famously defined as:
\begin{verbatim}
avg ← {(+/÷≢),⍵}
\end{verbatim}
which desugars into \verb|(+/,⍵)÷(≢,⍵)|, where the left hand side of the
division sums all the elements in the flattened argument array, and the
right hand side computes the number of elements in the argument array.

There are several ways we can simplify the generation of these 4 indices:
\begin{verbatim}
{avg c[(⊂2×⍵)+⍳2 2]}¨⍳(÷∘2⍴c)
\end{verbatim}
However, let us explore index-free formulations.  First, we observe that
the split into a grid of $2 \times 2$ with further averaging is a stencil
operation, where the sliding window moves 2 elements along each axis.
This pattern can be expressed with the stencil operator \verb|⌺|.  Non-unit
movements of the sliding window can be expressed by passing a matrix with two
rows, where the first row is the size of the sliding window, and the second one
is the step.  In our case both rows in the matrix are $2\ 2$.  According to the
padding formula \verb/⌊÷∘2(⍴w)-~2|⍴w/, the stencil of size $2 \times 2$ does not
create any paddings.  Therefore, we can formulate average pooling as:
\begin{verbatim}
avgpool ← {÷∘4{+/,⍵}⌺(2 2⍴2)⍤2⊣⍵}
\end{verbatim}
This reads: for every array at the last two dimensions \verb|⍤2|, apply the
stencil operation with sliding window of shape $2 \times 2$ that moves 2
elements along every dimension \verb|⌺(2 2⍴2)|, with the stencil operation
that sums all the 4 elements \verb|{+/,⍵}|.  Finally, divide the result by 4.
Despite its conciseness, this function uses the Dyalog-specific operator, and
it is specific to the 4-element average pooling.

We can do the splitting of a 2d array \verb|c| in the following way:
\begin{verbatim}
(x y) ← ⍴c
(x÷2) (y÷2) 2 2 ⍴ ⍉⍤2 ⊣(x÷2) 2 y ⍴ c
\end{verbatim}
That is: we cut the matrix across the first axis in 2-row chunks
\verb|(x÷2) 2 y ⍴ c|.  This does not change elements in memory, it
only changes 2d array of shape $x\ y$ into 3d array of shape $\frac{x}{2}\ 2\
y$.  Now, we can transpose the last two dimensions: \verb|⍉⍤2| ending
up with the shape $\frac{x}{2}\ y\ 2$, and we can cut along the second
axis by a simple reshape \texttt{(x÷2)\ (y÷2)\ 2\ 2\ ⍴}.  All we need to do
now is to apply \verb|avg| on the last two dimensions.  Finally, this
operation itself has to be applied rank-2, as we pool the last
two dimensions:
\begin{verbatim}
avgpool ← {
    (x y) ← ⍴⍵
    avg⍤2 ⊣(x÷2)(y÷2)2 2 ⍴ ⍉⍤2 ⊣(x÷2)2y ⍴ ⍵
}⍤2
\end{verbatim}


\paragraph{$C_2$ and $FC$ Layers}%
\label{par:_c_2_layer}
As we have chosen to represent $s$ as a 3d
array of shape $[6,12,12]$, the rank-polymorphic specification of \texttt{multiconv}
becomes immediately applicable here.  Intuitively, if $s$ is a single object,
then all the left hand sides of the arrows from $S^1$ to $C^2$ in Fig.~\ref{fig:cnn}
will merge into a single point, similarly to the first convolution.  The
input to \texttt{conv} is of shape $6\ 12\ 12$, each weight $k^2_{i,*}$ is of shape
$6\ 5\ 5$, producing a result of shape $1\ 8\ 8$.  As we have 12 $k^2_{i,*}$ and 12
biases, the application of the \texttt{multiconv} would be of shape $12\ 1\ 8\ 8$.

A fully connected layer $FC$ is just a convolution
with the weight that is identical to the shape of the input array, allowing us
to use \texttt{multiconv} again.  Without additional reshapes, the shape of the
layer $S^2$ would be $12\ 1\ 4\ 4$.  Therefore, as we intend to compute 10
weighted sums of all the elements,
$W$ becomes of shape $10\ 12\ 1\ 4\ 4$, yielding \texttt{multiconv} to
return the result of shape $10\ 1\ 1\ 1\ 1$.



\paragraph{Forward Pass}
At this point we have enough building blocks to define a ``forward path''
of the network, \ie{} Fig.~\ref{fig:cnn} which turns an image $I$
into a 10-element vector of probabilities, with fixed weights $k^i$ and $W$
and biases $b^i$ and $b$.
\begin{verbatim}
forward ← {
  (I k¹ b¹ k² b² W b) ← ⍵
  C¹ ← logistic multiconv I  k¹ b¹
  S¹ ← avgpool C¹
  C² ← logistic multiconv S¹ k² b²
  S² ← avgpool C²
  ŷ  ← logistic multiconv S² W  b
}
\end{verbatim}
Note that each layer is a single multi-dimensional array and
we compute the $FC$ layer directly from $S^2$.  To get the actual answer,
we have to chose the index of $\hat{y}$ with the largest value.  This
can be achieved with:
\begin{verbatim}
{⍵⍳⌈/⍵},ŷ
\end{verbatim}
which reads: for the flattened \verb|ŷ|, find the maximum element \verb|⌈/⍵|,
and find the index where this element occurs for the first time \verb|⍵⍳|.
Note, that \verb|⌈/⍵| reduces the two-argument (dyadic) maximum function
\verb|⌈| over the array, and the two-argument version of \verb|⍳| with the array
on the left and the element on the right returns the first occurrence of this
element in that array.  We can abstract this into a function:
\begin{verbatim}
maxpos ← {(,⍵)⍳⌈/,⍵}
\end{verbatim}

\subsection{Gradient Descent}
In order to implement the training of the network we have to propagate the
error that we got from the recognition back into the weights and biases.
In order to do this we use stochastic gradient descent.  The overall idea
is to consider $\hat{y}$ as a function over weights and biases.  The objective
function we want to minimise is: $o = \frac{1}{2}\sum_i{(\hat{y}_i - y_i)}^2$,
where $y$ is the correct answer, \ie{} a 10 element vector with value 1
at the position that corresponds to the digit depicted in $I$ and zeroes
at all the other positions.  Due to the linear nature of the network, and
carefully chosen activation functions, computation of partial derivatives
makes a great use of the chain rule.  For example, the derivative of our
objective function over some weight $w$ would be:
\[
    \frac{\partial o}{\partial w}
    = \sum_i\frac{\partial o}{\partial \hat{y}_i}
      \cdot\frac{\partial \hat{y}_j}{\partial w}
    = \sum_i(\hat{y}_i - y_i)\frac{\partial\hat{y}_i}{\partial w}
\]
Then the chain rule can be applied again.  This allows us to
``reverse'' each layer and reconnect them by inverting the
arrows in Fig.~\ref{fig:cnn}.  For precise mathematical details
please refer to the Section 2 of~\cite{zhang2016derivation}.  In the rest
of this section we focus on the implementation of these back layers.

\paragraph{Mean Squared Error}
We start with our objective, the sum of squared differences divided by two.
Thanks to the array versions of the arithmetic operations, we can write it
almost as a direct translation of the above sentence:
\begin{verbatim}
meansqerr ← {÷∘2+/,(⍺-⍵)*2}
\end{verbatim}
Note that we flatten \verb|(⍺-⍵)*2| to turn \verb|ŷ| into a 10-element vector,
so that we end-up with zero-dimensional array as a result of reduction.

\paragraph{Back Sigmoid}
The derivative of the sigmoid function is:
\[
    \frac{\partial}{\partial x}\sigma(x) = \sigma(x)(1 - \sigma(x))
\]
Since our sigmoid functions are always ``between''
the layers, the derivative of sigmoid will be always in a larger
derivative chain.  Therefore, for convenience, we add a multiplier
for that part of the chain:
\begin{verbatim}
blog ← {⍺×⍵×1-⍵}
\end{verbatim}

\paragraph{Back Average Pooling}
If 2d array $s$ is the average pooling of $t$ then each $s_i$ is defined as
$\frac{1}{4}(t_i+t_j+t_k+t_l)$ for some indices $j,k,l$.  Any partial
derivative of $s_i$ over $t_i$ is simply $\frac{1}{4}$.  Therefore,
backpropagation through the average pooling layer can be computed as:
$\Delta t[i\ j] = \frac{1}{4}\Delta s[\lfloor i/2\rfloor\ \lfloor j/2\rfloor]$.
For higher-dimensions we would need to apply this for the innermost
2d planes.  We can formulate this elegantly in APL as follows:
\begin{verbatim}
backavgpool  ← {2⌿2/⍵÷4}⍤2
\end{verbatim}
This reads: for a given argument array \verb|⍵|, divide every element
by 4, replicate every column twice \verb|2/|, and after that replicate
every row twice \verb|2⌿|.  Finally, apply this function rank-2.
This function hardcodes the size of the pooling, but
we can, in principle, generalise via the left argument, \eg{}
\begin{verbatim}
{(x y)←⍺ ⋄ {x⌿y/⍵÷x×y}⍤2 ⊣⍵}
\end{verbatim}


\paragraph{Back convolution}
The linear nature of the convolution implies that the derivatives are constants,
namely the input of the convolution itself.  Consequently, we can approximate
the error in the weights as a convolution of the input with the error.  So there
is no need for a separate function.

The error of the bias is a sum of the error we are propagating from the
previous stages of the chain, since the derivative of the bias is constant 1.
\begin{verbatim}
backbias ← {+/,⍵}
\end{verbatim}
Strictly speaking, we do not need this function either.

Now we have to compute the propagation of the error into the input of the
convolution.  The derivatives of the convolution with respect to the input
are simply weights.  We just have to carefully describe the index relation
between the value we are propagating and the index of the input that it affects.
Consider a trivial example:
\begin{verbatim}
      in w d
┌───────────┬───┬─────┐
│0.5 1 1.5 2│1 2│1 2 3│
│2.5 3 3.5 4│3 4│4 5 6│
│4.5 5 5.5 6│   │7 8 9│
│6.5 7 7.5 8│   │     │
└───────────┴───┴─────┘
\end{verbatim}
Here we have three arrays: \verb|in| is the input, the values are irrelevant, we
will use them to talk about positions; \verb|w| are weights and \verb|d| is the
propagated error.  Now, we have to make the convolution in reverse in order to
figure out which weights contribute to which positions.  It is clear that weight
\verb|1| contributes to the positions of \verb|in| from \verb|0.5| till
\verb|5.5|, as multiplication of any other element with the weight 1 implies out
of bound access.  By the same reasoning, weight 2 contributes to positions from
\verb|1| to \verb|6|; weight 3 from \verb|2.5| till \verb|7.5| and weight 4 from
\verb|3| till \verb|8|.  We can express this pattern by using the concept of
overtake: when we take more elements than there is in the array, the remainder
is padded with zeroes:
\begin{verbatim}
       5 ↑ 1 2 3
1 2 3 0 0
      ¯5 ↑ 1 2 3
0 0 1 2 3
\end{verbatim}
This means that we can overtake \verb|d| at every weight index in the following
way:
\begin{verbatim}
      ,w{(⍴in)↑(-⍵+⍴d)↑d}¨⍳⍴w
┌───────┬───────┬───────┬───────┐
│1 2 3 0│0 1 2 3│0 0 0 0│0 0 0 0│
│4 5 6 0│0 4 5 6│1 2 3 0│0 1 2 3│
│7 8 9 0│0 7 8 9│4 5 6 0│0 4 5 6│
│0 0 0 0│0 0 0 0│7 8 9 0│0 7 8 9│
└───────┴───────┴───────┴───────┘
\end{verbatim}
That is: we first pad with zeroes on the top/left with \verb|(-⍵+⍴d)↑|
and then we adjust it on the right/bottom with \verb|(⍴in)↑|.  Then if we
multiply our shifts by the corresponding weight and sum it all up we have:
\begin{verbatim}
backin ← {(d w in)←⍵ ⋄ ⊃+/,w{(⍴in)↑(-⍵+⍴d)↑⍺×d}¨⍳⍴w}
\end{verbatim}

Finally, we put together the three above steps in the function that
propagates weight- bias- and input errors:
\begin{verbatim}
bmconv ← {
  (δo ws in bs) ← ⍵
    d ← ⍴⍴in
  δin ← +⌿δo {backin ⍺ ⍵ in} ⍤(d, d) ⊣ ws
  δws ← {⍵ conv in} ⍤d ⊣ δo
  δbs ← backbias ⍤d ⊣ δo
  δin δws δbs
}
\end{verbatim}
As it can be seen, this is mainly bookkeeping.  We apply \verb|backin| rank-2
\verb|d| and sum it up along the leading dimension.  This builds on the
assumption that convolution evaluates a vector of outputs. If the
dimension goes higher, we can use the enclose/disclose approach to sum
the subarrays.  Back-weights and back biases are simply applications
of the corresponding functions using rank:

Putting all the basic blocks together we have:
\begin{verbatim}
train ← {
  (I y k¹ b¹ k² b² W  b) ← ⍵
             C¹ ← logistic multiconv I  k¹ b¹
             S¹ ← avgpool C¹
             C² ← logistic multiconv S¹ k² b²
             S² ← avgpool C²
              ŷ ← logistic multiconv S² W  b
             δŷ ← ŷ - y
              e ← ŷ meansqerr y
    (δS² δW δb) ← bmconv (δŷ  blog  y) W  s² b
            δC² ← backavgpool δS²
  (δS¹ δk² δb²) ← bmconv (δC² blog C²) k² S¹ b²
            δC¹ ← backavgpool δS¹
    (_ δk¹ δb¹) ← bmconv (δC¹ blog C¹) k¹ I  b¹
  δk¹ δb¹ δk² δb² δW δb e
}
\end{verbatim}





% Reports of APL-based convolution on higher-rank arrays are, oddly enough,
% largely absent from the literature.\fixme{Is it really?}
% Given this dearth of information, we decided to write our own
% convolution in Dyalog~APL\@. We quickly discarded the two most
% likely candidates for kernel functions, \emph{n-wise reduction},
% and \emph{stencil}. Dyalog's definition of reduction is vector-based, so effectively
% useless for our matrix work. Their stencil conjunction is, in our
% opinion, overly complicated, and we felt that the work required
% to eliminate shards --- partial subarrays --- from its
% result elements would outweigh any
% potential benefit from use of that built-in
% operation.~\footnote{We are going to propose an extension to that primitive which would, hopefully, resolve this difficulty, by not generating
% shards.}
%
% \fixme{How important is it to make convolution rank-polymorphic?}





ConvolveD←{
⍝ Convolution using Dyalog APL's Stencil conjunction
stencil←⍵  ⍝ Avoid name conflict in Stencil left operand
drp←¯1+⍴stencil ⍝ Drop shards
drp↓(-drp)↓{{+//,⍵}stencil×⍵}⌺(⍴stencil)⊢⍺}


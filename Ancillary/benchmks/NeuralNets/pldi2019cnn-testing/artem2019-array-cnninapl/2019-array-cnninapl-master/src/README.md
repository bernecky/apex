This directory contain multiple implementations of the CNN for handwritten
image recognition that follow Zhang's design.

The overall file structure is:

```
 apl
 ├── cnn.apl                 # APL implementation, basic operations only
 └── README.md
 input
 └── README.md               # A directory for MNIST inputs
 pt
 ├── main.py                 # PyTorch version of Zhang's CNN
 └── README.md
 sac
 ├── cnn.sac                 # Building blocks written using with-loops
 ├── cnn_tc.sac              # Building blocks using new notation as in the paper
 ├── mnist.sac               # Helper module to read in the data
 └── zhang.sac               # The actual CNN
 tf-cxx
 ├── cmake                   # Helper CMake modules
 ├── CMakeLists.txt          # CMake file to gather all the dependencies for
 │                           # the dependent libraries
 ├── README.md
 └── zhang.cc                # The C++ version of the CNN with Tensorflow
 tf-py
 └── zhang.py                # The python version of the CNN with Tensorflow
```

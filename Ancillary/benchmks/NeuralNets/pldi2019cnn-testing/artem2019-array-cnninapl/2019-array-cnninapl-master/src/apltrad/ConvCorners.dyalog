﻿z←alpha ConvCorners omega;cs
⍝ stencil alpha, image omega
cs←1+(⍴omega)-⍴alpha ⍝ Conv result shape
z←cs⍴(,(⍴omega)↑cs⍴1)/⍳×/⍴omega

﻿z←TrAll omega;i;k1;b1;k2;b2;fc;b;tr_img;tr_lab;batchsz;rate;trainings;error;blk;target;d_k1;d_b1;d_k2;d_b2;d_fc;d_b;i0;bo
i0←omega
k1←sgi1
b1←sgi2
k2←sgi3
b2←sgi4
fc←sgi5
b←sgi6
error←0
tr_img←sgi7
tr_lab←sgi8
batchsz←sgi10
rate←sgi11
trainings←sgi12

:for i :in ⍳trainings÷batchsz
⍝⍝ blk←batchsz↑(i×batchsz)↓tr_img
⍝⍝ target←batchsz↑(i×batchsz)↓tr_lab
 sgi0←k1
 sgi1←b1
 sgi2←k2
 sgi3←b2
 sgi4←fc
 sgi5←b
⍝  generate batchsized versions of k1 b1 k2 b2 fc b error
bo←((batchsz×i)+⍳batchsz) TrainZhang⍤0 1⊢ (tr_img tr_lab k1 b1 k2 b2 fc b)
⍝d_b1←sgo1
⍝d_k2←sgo2
⍝d_b2←sgo3
⍝d_fc←sgo4
⍝d_b←sgo5
⍝err←sgo6
 
 k1←k1-rate×+⌿⊃ bo[;0]
 b1←b1-rate×+⌿⊃ bo[;1]
 k2←k2-rate×+⌿⊃ bo[;2]
 b2←b2-rate×+⌿⊃ bo[;3]
 fc←fc-rate×+⌿⊃ bo[;4]
 b← b -rate×+⌿⊃ bo[;5]
 error←error++⌿⊃bo[;6]
 ⍝ slows us down...     ⎕←'Error at iteration ',(⍕i),' is ',⍕error ⍝⍝ ÷trainings
:endfor
sgo0←k1
sgo1←b1
sgo2←k2
sgo3←b2
sgo4←fc
sgo5←b
z←k1 
 
  

z← alpha AvgPool omega;rc
⍝ This is rank-2 only, and it only works for alpha≡2 2
rc←1⌽omega
z←omega+rc+(1⊖omega)+1⊖rc
z←((0⌷⍴omega)⍴(0⌷alpha)↑1)⌿z
z←((1⌷⍴omega)⍴(1⌷alpha)↑1)/z
z←z÷×/alpha

